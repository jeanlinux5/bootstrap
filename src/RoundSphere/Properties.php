<?php

namespace RoundSphere;

use Exception;

/**
 * Class Properties
 *
 * @package RoundSphere
 */
class Properties
{
    static $props = null;
    static $config_file = null;

    static function setConfigFile($config)
    {
        self::$config_file = $config;
        self::$props = null;
    }

    /**
     * Get property
     *
     * @param $key
     * @param string $default
     * @return mixed
     */
    static function getProperty($key, $default = '')
    {
        if (Properties::$props == null) {
            Properties::$props = Properties::loadProperties();
        }

        if (preg_match('#^(.*)\.(.*)$#', $key, $matches)) {
            $subkey1 = trim($matches[1]);
            $subkey2 = trim($matches[2]);
            return (isset(Properties::$props[$subkey1][$subkey2])) ? Properties::$props[$subkey1][$subkey2] : $default;
        }

        return (isset(Properties::$props[$key])) ? Properties::$props[$key] : $default;
    }

    static function setProperty($key, $value)
    {
        Properties::$props[$key] = $value;
    }

    static private function _parseValue($val)
    {
        if (preg_match("#\[(.*)\]#", $val, $matches)) {
            // [abc,def,ghi] syntax = return an array
            $parts = explode(',', $matches[1]);
            $values = array();
            foreach ($parts as $part) {
                $values[] = trim($part);
            }
            return $values;
        } else {
            return trim($val);
        }
    }

    static function loadProperties()
    {
        if (Properties::$props == null) {
            $props = array();

            $files = array(
                realpath(self::$config_file) => true,
                // To back to /websites/website.com/conf/project.properties
                realpath(dirname(self::$config_file) . "/../../conf/project.properties") => false,
                // To back to /websites/website.com/appname/etc/project.properties
                realpath(dirname(self::$config_file) . "/../etc/project.properties") => false,
            );

            foreach ($files as $filename => $required) {

                if ($required && (!file_exists($filename))) {
                    throw new Exception('Unable to load application configuration file');
                } elseif (file_exists($filename)) {
                    $file = fopen($filename, "r");
                    while (!feof($file)) {
                        $line = trim(fgets($file));
                        if (substr($line, 0, 1) != '#') {
                            $pos = strpos($line, "=");
                            if ($pos !== false) {
                                $key = trim(substr($line, 0, $pos));
                                $val = trim(substr($line, $pos + 1));

                                // check for 'core_db.name = value'   style lines
                                if (preg_match("#(.*?)\.(.*)\.(.*)#", $key, $matches)) {
                                    $props[$matches[1]][$matches[2]][$matches[3]] = Properties::_parseValue($val);
                                } elseif (preg_match("#(.*?)\.(.*)#", $key, $matches)) {
                                    $props[$matches[1]][$matches[2]] = Properties::_parseValue($val);
                                } else {
                                    $props[$key] = Properties::_parseValue($val);
                                }
                            }
                        }
                    }
                    fclose($file);
                }
            }

            Properties::$props = $props;
        }

        return Properties::$props;
    }
}


