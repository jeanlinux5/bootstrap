<?php

namespace RoundSphere;

class Isbn
{
    var $isbn13 = '';
    var $isbn10 = '';

    function __construct($isbn)
    {
        $isbn = isbn::clean($isbn);
        if ($this->valid($isbn)) {
            $this->isbn10 = $this->ten($isbn);
            $this->isbn13 = $this->thirteen($isbn);
        }
    }

    function ten($isbn = '', $asin_ok = false)
    {
        $isbn = $isbn ? isbn::clean($isbn, $asin_ok) : (isset($this) && isset($this->isbn10) ? $this->isbn10 : '');
        if (!($isbn)) {
            return false;
        } elseif (isbn::valid($isbn, false, $asin_ok)) {
            // Already 10 digits
            if (strlen($isbn) == 10) {
                return $isbn;
            } else {
// If ISBN starts with 978, drop 978 + check digit, recalculate check digit
// http://www.bisg.org/pi/appendix_1.html#Retrieving%20the%2010-digit%20ISBN%20from%20the%20Bookland%20EAN
                $retval = substr($isbn, 3, 9);
                $sum = 0;
                for ($i = 0; $i < 9; $i++) {
                    $sum += ((int)$retval{$i}) * ($i + 1);
                }
                $checksum = $sum % 11;
                if ($checksum == 10) {
                    $retval .= 'X';
                } else {
                    $retval .= $checksum;
                }
                return $retval;
            }
        } else {
            return false;
        }
    }

    function thirteen($isbn = '')
    {
        if ($isbn) {
            $isbn = isbn::clean($isbn);
        } elseif (isset($this) && !empty($this->isbn13)) {
            return $this->isbn13;
        } else {
            return '';
        }
        if (!$isbn) {
            return false;
        } elseif (isbn::valid($isbn)) {
            // Already 13 digits
            if (strlen($isbn) == 13) {
                return $isbn;
            } else {
                $retval = "978" . substr($isbn, 0, 9);
                $sum = 0;
                for ($i = 0; $i < 12; $i++) {
                    $sum += ((int)$retval{$i}) * (1 + (($i % 2) * 2)); // one or three, even/odd
                }
                $retval .= (10 - ($sum % 10)) % 10;
                return $retval;
            }
        } else {
            return false;
        }
    }

    function nineSevenEight($isbn)
    {
        // Strip non-numeric (or x)
        // $isbn = isbn10($isbn);

        // Strip non-numeric (or x)
        $isbn = preg_replace('/[^0-9x]/i', '', $isbn);
        if (strlen($isbn) == 13) {
            // Recalculate the ISBN
            $retval = substr($isbn, 3, 9);
            $sum = 0;
            for ($i = 0; $i < 9; $i++) {
                $sum += ((int)$retval{$i}) * ($i + 1);
            }
            $checksum = $sum % 11;
            if ($checksum == 10) {
                $retval .= 'X';
            } else {
                $retval .= $checksum;
            }
            $isbn = $retval;
        }

        $isbn = preg_replace('/[^0-9x]/i', '', $isbn);
        if (strlen($isbn) == 13) {
            return $isbn;
        } elseif (strlen($isbn) == 10) {
            // Build the 13 digit ISBN based on the 10
            $retval = "978" . substr($isbn, 0, 9);
            $sum = 0;
            for ($i = 0; $i < 12; $i++) {
                $sum += ((int)$retval{$i}) * (1 + (($i % 2) * 2)); // one or three, even/odd
            }
            $retval .= (10 - ($sum % 10)) % 10;
            return $retval;
        } else {
            return '';
        }
    }

    function clean($isbn, $asin_ok = false)
    {
        if ($asin_ok) {
            $digits = preg_replace('/[^0-9a-z]/i', '', $isbn);
            return $digits;
        }
        $digits = preg_replace('/[^0-9x]/i', '', $isbn);
        // If the resulting number is between 6 and 9 digits, try
        // left padding and see if we get a valid ISBN
        // This is written specifically for when Excel eliminates the leftmost zeros
        if ((strlen($digits) <= 9) && (strlen($digits) >= 6)) {
            $digits = str_pad($digits, 10, 0, STR_PAD_LEFT);
            if (!isbn::valid($digits)) {
                return '';
            }
        }
        return $digits;
    }

    function valid($isbn, $clean = false, $asin_ok = false)
    {
        if ($asin_ok && strlen($isbn) == 10 && preg_match('#^B#', $isbn)) return true;
        $isbn = ($clean) ? isbn::clean($isbn) : $isbn;
        $strlen = strlen($isbn);
        $isbn = strtoupper($isbn);
        if ($strlen == 10) {
            $sum = 0;
            for ($i = 0; $i < $strlen - 1; $i++) {
                $sum += ((int)$isbn{$i}) * ($i + 1);
            }
            $checksum = $sum % 11;
            return
                ($checksum == 10 && $isbn{strlen($isbn) - 1} == 'X') ||
                ($checksum == $isbn{strlen($isbn) - 1});
        } else if ($strlen == 13 && preg_match('/^97[89]/', $isbn)) {
            //http://www.bisg.org/pi/appendix_1.html#Algorithm%20for%20Checking%20the%20Bookland%20EAN
            $sum = 0;
            for ($i = 0; $i < $strlen; $i++) {
                $sum += ((int)$isbn{$i}) * (1 + (($i % 2) * 2)); // one or three, even/odd
            }
            return ($sum % 10) == 0;
        }
        return false;
    }

    // used to generate valid test ISBN data
    static function generateISBN($num)
    {
        $str = "$num";
        while (strlen($str) < 8) {
            $str = "0" . $str;
        }
        $str = "1" . $str;

        $sum = 0;
        for ($i = 0; $i < strlen($str); $i++) {
            $sum += (int)$str{$i} * ($i + 1);
        }

        // add checksum
        $checksum = $sum % 11;
        if ($checksum == 10) {
            $checksum = 'X';
        };
        $str .= $checksum;

        return $str;
    }
}

