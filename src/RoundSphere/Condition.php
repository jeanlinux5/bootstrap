<?php

namespace RoundSphere;

class Condition
{

    static private function cleaned($cond)
    {
        $clean = strtolower(preg_replace('#[^a-z0-9]*#i', '', $cond));
        // Some places UsedVeryGood is used instead of just verygood
        $clean = str_replace('used', '', $clean);
        return $clean;
    }

    static public function letters($cond)
    {
        switch (strtolower(self::words($cond))) {
            case 'new':
                return 'N';
            case 'likenew':
                return 'LN';
            case 'mint':
                return 'LN';
            case 'verygood':
                return 'VG';
            case 'good':
                return 'G';
            case 'acceptable':
                return 'A';
            case 'poor':
                return 'P';
            case 'parts':
                return 'P';

            case 11:
                return 'N';
            case 5:
                return 'A';
            case 4:
                return 'A';
            case 3:
                return 'G';
            case 2:
                return 'VG';
            case 1:
                return 'LN';

            default:
                return 'UNK';
        }
    }

    static public function words($cond)
    {
        switch (self::cleaned($cond)) {
            case 'new':
                return 'New';
            case 'likenew':
                return 'LikeNew';
            case 'mint':
                return 'LikeNew';
            case 'verygood':
                return 'VeryGood';
            case 'good':
                return 'Good';
            case 'acceptable':
                return 'Acceptable';
            case 'poor':
                return 'Poor';
            case 'parts':
                return 'Parts';

            case 11:
                return 'New';
            case 5:
                return 'Poor';
            case 4:
                return 'Acceptable';
            case 3:
                return 'Good';
            case 2:
                return 'VeryGood';
            case 1:
                return 'LikeNew';

            case 1000:
                return 'New';
            case 1500:
                return 'LikeNew';
            // case 3000:          return 'VeryGood';
            case 3000:
                return 'Good';
            case 3000:
                return 'Acceptable';
            case 7000:
                return 'Parts';

            default:
                return 'Unknown';
        }
    }

    static public function mwsxml($cond)
    {
        switch (strtolower(self::words($cond))) {
            case 'new':
                return 'New';
            case 'likenew':
                return 'UsedLikeNew';
            case 'mint':
                return 'UsedLikeNew';
            case 'verygood':
                return 'UsedVeryGood';
            case 'good':
                return 'UsedGood';
            case 'acceptable':
                return 'UsedAcceptable';
            case 'poor':
                return 'UsedAcceptable';
            case 'parts':
                return 'UsedAcceptable';
            default:
                return 'Unknown';
        }
    }

    static public function fba($cond)
    {
        switch (strtolower(self::words($cond))) {
            case 'new':
                return 'NewItem';
            case 'likenew':
                return 'UsedLikeNew';
            case 'mint':
                return 'UsedLikeNew';
            case 'verygood':
                return 'UsedVeryGood';
            case 'good':
                return 'UsedGood';
            case 'acceptable':
                return 'UsedAcceptable';
            case 'poor':
                return 'UsedAcceptable';
            case 'parts':
                return 'UsedAcceptable';

            /*
                        case 11:            return 'NewItem';
                        case 5:             return 'UsedAcceptable';
                        case 4:             return 'UsedAcceptable';
                        case 3:             return 'UsedGood';
                        case 2:             return 'UsedVeryGood';
                        case 1:             return 'UsedLikeNew';
            */

            default:
                return 'Unknown';
        }
    }

    static public function number($cond)
    {
        switch (strtolower(self::words($cond))) {
            case 'new':
                return 11;
            case 'likenew':
                return 1;
            case 'mint':
                return 1;
            case 'verygood':
                return 2;
            case 'good':
                return 3;
            case 'acceptable':
                return 4;
            case 'poor':
                return 5;
            case 'parts':
                return 5;

            /*
                        case 11:            return 11;
                        case 4:             return 1;
                        case 3:             return 1;
                        case 2:             return 2;
                        case 1:             return 3;
            */

            default:
                return 0;
        }
    }

    static public function customer($cond)
    {
        switch (strtolower(self::words($cond))) {
            case 'new':
                return 'New Item';
            case 'likenew':
                return 'Used; LikeNew';
            case 'mint':
                return 'Used; LikeNew';
            case 'verygood':
                return 'Used; VeryGood';
            case 'good':
                return 'Used; Good';
            case 'acceptable':
                return 'Used; Acceptable';
            case 'poor':
                return 'Used; Acceptable';
            case 'parts':
                return 'For Parts';

            /*
                        case 11:            return 'New; Item';
                        case 5:             return 'Used; Acceptable';
                        case 4:             return 'Used; Acceptable';
                        case 3:             return 'Used; Good';
                        case 2:             return 'Used; VeryGood';
                        case 1:             return 'Used; LikeNew';
            */

            default:
                return 'Unknown';
        }
    }

    static public function ebay($cond)
    {
        switch (strtolower(self::words($cond))) {
            case 'new':
                return 1000;
            case 'likenew':
                return 1500;
            case 'verygood':
                return 3000;
            case 'good':
                return 3000;
            case 'acceptable':
                return 3000;
            case 'parts':
                return 7000;
            default:
                return 0;
        }
    }

    static public function options()
    {
        return array(
            'Acceptable', 'Good', 'VeryGood', 'LikeNew', 'New'
        );
    }

    static public function isEqualOrBetter($left, $right)
    {
        $left = self::number($left);
        $right = self::number($right);
        // New offers are always equal or better
        if ($left == 11) return true;
        // Zero means unknown condition, ans is never equal or better
        if ($left == 0) return false;
        // If right is new, then left must be new also
        if ($right == 11) {
            return ($left == 11);
        }
        // Otherwise, lower numbers are equal or better
        return ($left <= $right);
    }

}

