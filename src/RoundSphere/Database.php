<?php
Namespace RoundSphere;

class Database
{
    protected $dbh;
    private $config;
    static private $db;
    static private $master;

    public function __construct($config = null)
    {
        if (empty($config)) {
            $config = Properties::getProperty('db');
        }
        $this->config = $config;
    }

    public function conn()
    {
        if (!isset($this->dbh)) {
            $this->_connect();
        }
        return $this->dbh;
    }

    public function prepare($sql)
    {
        return $this->conn()->prepare($sql);
    }

    public function getRow($sql, $args = array(), $recurse = true)
    {
        try {
            $sth = $this->conn()->prepare($sql);
            $sth->execute($args);
            $sth->setFetchMode(\PDO::FETCH_ASSOC);
            return $sth->fetch();
        } catch (\PDOException $e) {
            if ($recurse && 'HY000' == $e->getCode()) {
                // SQLSTATE[HY000]: General error: 2013 Lost connection to MySQL server during query
                unset($this->dbh);
                return $this->getRow($sql, $args, false);
            }
            throw $e;
        }
    }

    public function getOne($sql, $args = array(), $recurse = true)
    {
        try {
            $sth = $this->conn()->prepare($sql);
            $sth->execute($args);
            $sth->setFetchMode(\PDO::FETCH_NUM);
            $row =  $sth->fetch();
            return $row[0];
        } catch (\PDOException $e) {
            if ($recurse && 'HY000' == $e->getCode()) {
                // SQLSTATE[HY000]: General error: 2013 Lost connection to MySQL server during query
                unset($this->dbh);
                return $this->getOne($sql, $args, false);
            }
            throw $e;
        }
    }

    public function getCol($sql, $args = array(), $column = '', $recurse = true)
    {
        try {
            $sth = $this->conn()->prepare($sql);
            $sth->execute($args);
            if ($column) {
                $sth->setFetchMode(\PDO::FETCH_ASSOC);
            } else  {
                $sth->setFetchMode(\PDO::FETCH_COLUMN, 0);
            }

            $results = array();
            while ($row = $sth->fetch()) {
                if ($column) {
                    $results[] = $row[$column];
                } else {
                    $results[] = $row;
                }
            }
            return $results;
        } catch (\PDOException $e) {
            if ($recurse && 'HY000' == $e->getCode()) {
                // SQLSTATE[HY000]: General error: 2013 Lost connection to MySQL server during query
                unset($this->dbh);
                return $this->getCol($sql, $args, $column, false);
            }
            throw $e;
        }
    }


    public function getAll($sql, $args = array(), $index_col = '', $recurse = true)
    {
        try {
            $sth = $this->conn()->prepare($sql);
            $sth->execute($args);
            $sth->setFetchMode(\PDO::FETCH_ASSOC);
            $results = array();
            while ($row = $sth->fetch()) {
                if (isset($row[$index_col])) {
                    $results[$row[$index_col]] = $row;
                } else {
                    $results[] = $row;
                }
            }
            return $results;
        } catch (\PDOException $e) {
            if ($recurse && 'HY000' == $e->getCode()) {
                // SQLSTATE[HY000]: General error: 2013 Lost connection to MySQL server during query
                unset($this->dbh);
                return $this->getAll($sql, $args, $index_col, false);
            }
            throw $e;
        }
    }

    public function query($sql, $args = array(), $recurse = true)
    {
        try {
            $sth = $this->conn()->prepare($sql);
            return $sth->execute($args);
        } catch (\PDOException $e) {
            if ($recurse && 'HY000' == $e->getCode()) {
                // SQLSTATE[HY000]: General error: 2013 Lost connection to MySQL server during query
                unset($this->dbh);
                return $this->query($sql, $args, false);
            }
            throw $e;
        }
    }

    public function insertIdFromQuery($sql, $args = array(), $recurse = true )
    {
        try {
            $dbh = $this->conn();
            // $dbh->beginTransaction();
            $sth = $dbh->prepare($sql);
            $rc = $sth->execute($args);
            $insert_id = $dbh->lastInsertId();
            // $dbh->commit();
            // $this->last_affected = $sth->rowCount();
            return $insert_id;
        } catch (\PDOException $e) {
            if ($recurse && 'HY000' == $e->getCode()) {
                // SQLSTATE[HY000]: General error: 2013 Lost connection to MySQL server during query
                unset($this->dbh);
                return $this->insertIdFromQuery($sql, $args, false);
            }
            throw $e;
        }
    }

    public function quote($string)
    {
        return $this->conn()->quote($string);
    }

    private function _connect()
    {
        $conf = $this->config;
        $port = isset($conf['port']) ? $conf['port'] : 3306;
        if (empty($conf['host']) || empty($conf['name']) || empty($conf['user']) || empty($conf['pass'])) {
            throw new \Exception("RoundBase\Database::incomplete database config");
        }
        $this->dbh = new \PDO("mysql:host={$conf['host']};port={$port};dbname={$conf['name']};", $conf['user'], $conf['pass']);
        $this->dbh->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
    }

    static public function db()
    {
        if (!isset(self::$db)) {
            $config = Properties::getProperty('db');
            self::$db = new Database($config);
        }
        return self::$db;
    }

    static public function master()
    {
        if (!isset(self::$master)) {
            $config = Properties::getProperty('master_db');
            self::$master = new Database($config);
        }
        return self::$master;
    }
}
