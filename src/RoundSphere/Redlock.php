<?php

namespace RoundSphere;
use Predis;
use Predis\Connection\Aggregate\RedisCluster;
use Predis\Connection\Factory;
use Predis\Cluster\RedisStrategy;


class Redlock
{
    static $redis;
    protected $name;
    protected $randomValue;
    protected $locked = false;

    public function __construct($name = null)
    {
        $this->setName($name);
        $this->randomValue = rand(0, 1000000).'@'.time();
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function appendValue($string)
    {
        if ($this->locked) {
            throw new Exception("lock->appendValue() invalid after lock->acquire has been called");
        }
        $this->randomValue .= ":{$string}";
    }

    public function getLockValue()
    {
        return $this->randomValue;
    }

    static public function redis()
    {
        if (!isset(self::$redis)) {

            $redisConfig = Properties::getProperty('redis');
            if ($redisConfig) {
                self::$redis = new Predis\Client($redisConfig);
                return self::$redis;
            }

            $redisClusterConfig = Properties::getProperty('redisCluster');
            if ($redisClusterConfig) {

                $options = array(
                    'cluster' => function () {
                        $cluster = new Predis\Connection\Aggregate\RedisCluster(new Factory());
                        return $cluster;
                    },
                );

                $multipleServers = [
                    "{$redisClusterConfig['host']}:{$redisClusterConfig['port']}",
                ];

                self::$redis = new Predis\Client($multipleServers, $options);
                return self::$redis;
            }
            throw new \Exception("no redis config defined");
        }
        return self::$redis;
    }

    public function query()
    {
        return self::redis()->get("LOCK:{$this->name}");
    }

    public function acquire($expire = 60, $absoluteMax = 86400)
    {
        $this->randomValue = preg_replace('#\+.*#', '', $this->randomValue);
        $this->randomValue .= "+{$expire}";
        $this->locked = true;

        self::redis()->set("LOCKRENEW:{$this->name}", time());
        self::redis()->expire("LOCKRENEW:{$this->name}", $expire);

        $rv = self::redis()->setnx("LOCK:{$this->name}", $this->randomValue);
        if ($rv) {
            // Lock didn't already exists
            self::redis()->expire("LOCK:{$this->name}", $expire);

        } elseif ($absoluteMax) {
            // If lock already exists, parse the value and see if it is older than $absoluteMax
            $value = self::query();
            if (preg_match('#@([0-9]+)#', $value, $matches)) {
                $timestamp = (int)$matches[1];
                if ($timestamp < time() - $absoluteMax) {
                    // Parsed timestamp is older than $absolutMax, so set it
                    $rv = self::redis()->set("LOCK:{$this->name}", $this->randomValue);
                    self::redis()->expire("LOCK:{$this->name}", $expire);
                }
            }
        }
        return $rv;
    }

    public function renew($expire = 60)
    {
        $current = self::redis()->get("LOCK:{$this->name}");
        if ($current != $this->randomValue) {
            // Return false if the current value is not held by us
            return false;
        }
        self::redis()->set("LOCKRENEW:{$this->name}", time());
        self::redis()->expire("LOCKRENEW:{$this->name}", $expire);
        return self::redis()->expire("LOCK:{$this->name}", $expire);
    }

    public function release()
    {
        return self::redis()->del("LOCK:{$this->name}");
    }

    public function lastRenewed()
    {
        return self::redis()->get("LOCKRENEW:{$this->name}");
    }
}
