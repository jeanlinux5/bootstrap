<?php

namespace RoundSphere;
use GuzzleHttp\Cookie\FileCookieJar;

/*
    THIS IS A WORK IN PROGRESS. IT LOOKS LIKE IT MAINLY WORKS, BUT
    AUTHENTICATION THROUGH AMAZON SEEMS TO BE HAVING SOME PROBLEMS
*/


class HttpBotGuzzle
{
    protected $tmp_dir = '';
    protected $ua = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; YPC 3.2.0; .NET CLR 1.1.4322; InfoPath.1; yplus 5.6.04b)';
    protected $req_num = 0;
    protected $debug = true;
    protected $current_url = '';

    // Stick with the misspelling
    public $referer = '';

    protected $retrieved = array();
    protected $log = '';
    protected $short_log = '';
    protected $emulate_browser = true;

    protected $log_prefix = '';
    protected $timeout = 20;

    protected $skip_referer = false;
    protected $cleanTempDir = true;

    protected $guzzle;

    public function __construct($tmpDir = null)
    {
        if ($tmpDir) {
            if (!file_exists($tmpDir)) {
                throw new Exception("HttpBot: tmpDir specified by does not exist");
            }
            $this->cleanTempDir = false;
            $this->tmp_dir = $tmpDir;
        } else {
            $systemTempDir = \RoundSphere\Properties::getProperty('tmpDir', '/tmp');
            $this->tmp_dir = "{$systemTempDir}/http_bot." . time() . '-' . rand(0, 10000);
            mkdir($this->tmp_dir);
        }
        $this->short_log =& $GLOBALS['bot_log'];
    }

    public function __destruct()
    {
        if (!isset($_REQUEST['nodestruct'])) {
            $this->cleanTempDir = false;
            if (isset($_SERVER['TERM'])) {
                // Only output this if running in a terminal session
                echo "<h1>TEMPORARY FILES SAVED TO {$this->tmp_dir}</h1>\n";
            }
            bclog("DESTRUCTING IS LEAVING TEMPORARY FILES FOR DEBUGGING IN {$this->tmp_dir}");
        }

        if ($this->cleanTempDir) {
            // Clean up some temporary files that we created
            $dir = opendir($this->tmp_dir);
            while ($file = readdir($dir)) {
                if (!preg_match("#^\.#", $file)) {
                    unlink("{$this->tmp_dir}/$file");
                }
            }
            rmdir($this->tmp_dir);
        }
        return true;
    }

    public function setUserAgent($ua)
    {
        $this->ua = $ua;
    }

    public function getTempDir()
    {
        return $this->tmp_dir;
    }

    public function __get($name)
    {
        if (isset($this->$name)) {
            return $this->$name;
        }
        throw new \Exception("Undefined property: {$name}");
    }

    // Determine which file we are saving cookies in
    public function cookieFile()
    {
        // Just put everything in one cookie file. Curl sorts out which hosts gets which cookies
        return "{$this->tmp_dir}/cookies.json";
        // return "{$this->tmp_dir}/cookie.all.txt";
    }

    // The main function that create the CURL request and submits it
    public function post($url, $data = array(), $method = 'POST', $emulate = true, $output_file = null, $extraHeaders = [], $isJson = false)
    {
        $this->req_num++;
        $extra = ($emulate) ? '' : 'SUB'; // Add some extra spacing to make it easier to find the page requests
        $this->addLog("{$extra}Request [{$this->req_num}] $method $url");

// TODO:
        if ($output_file) {
            $file_out = fopen($output_file, 'wb');
            curl_setopt($curl, CURLOPT_FILE, $file_out);
        }

        // Convert $data from an array to a urlencoded string
        if (is_array($data)) {
            $encoded = '';
            // TODO: perhaps deal with multidimensional arrays here?
            foreach ($data as $name => $value) {
                $encoded .= urlencode($name) . '=' . urlencode($value) . '&';
            }
            // Remove the last ampersand
            $encoded = substr($encoded, 0, strlen($encoded) - 1);
        } else {
            $encoded = $data;
        }

        if (strtoupper($method) == 'POST') {
            $this->addLog("http_bot::post() posting with this \$data");
            $this->addLog($encoded);
            $this->addLog(print_r($data, true));
        } else {
            if ($data) {
                $this->addLog("received GET request with some \$data, so putting \$data in the query string");
                $this->addLog("url is now $url");
                $url .= (strpos($url, '?') === false) ? '?' : '&';
                $url .= $data;
                $data = null;
            }
        }
        $this->current_url = $url;

        $cookieFile = $this->cookieFile($url);
        $cookieJar = new FileCookieJar($cookieFile, TRUE);

        $this->guzzle = new \Guzzlehttp\Client([
              'cookies' => $cookieJar
        ]);

        if (empty($headers['User-Agent'])) {
            $headers = [
                'User-Agent' => $this->ua,
            ];
        }

        if (!$this->skip_referer && empty($headers['referer'])) {
            $headers['referer'] = preg_replace('#[^a-z0-9\. \-_:\/\?&=]#', '', $this->referer);
        }

        $headers['dnt'] = 1;
        $headers['pragma'] = 'no-cache';
        $headers['upgrade-insecure-requests'] =  1;
        $headers['cache-control'] = 'no-cache';
        $headers['scheme'] =  'https';
        $headers['accept'] =  'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8';
        $headers['accept-encoding'] =  'gzip, deflate, br';
        $headers['accept-language'] =  'en-US,en;q=0.9';
        $headers['scheme'] = 'https';

        file_put_contents("{$this->tmp_dir}/{$this->req_num}.request", print_r($headers, true));

        if ($isJson) {
            $dataNode = 'json';
        } else {
            $dataNode = 'form_params';
        }

        try {
            ob_start();
            $response = $this->guzzle->request(
                $method,
                $url,
                [
                    'headers'           => $headers,
                    'connect_timeout'   => $this->timeout,
                    $dataNode           => $data,
                    'debug'             => in_array('debug', $GLOBALS['argv']),
                ]
            );
            $output = ob_get_clean();
            file_put_contents("{$this->tmp_dir}/{$this->req_num}.request2", $output);
        } catch (\Exception $e) {
            $class = get_class($e);
            echo "EXCEPTION [{$class}]: ".$e->getMessage()."\n";
            $response = $e->getResponse();
            file_put_contents("{$this->tmp_dir}/{$this->req_num}.response", $response->getBody());
            return null;
        }

        $http_response = (string)$response->getBody();
        file_put_contents("{$this->tmp_dir}/{$this->req_num}.response", $http_response);

        // $http_response = curl_exec($curl); // Execute!
        if (empty($http_response)) {
            $this->addLog("Curl Error (" . curl_errno($curl) . ") " . curl_error($curl) . " for url=$url");
            echo "EMPTY HTTP RESPONSE\n";
            return false;
        }

        $topPart = "HTTP/1.1 {$response->getStatusCode()}\n";
        foreach ($response->getHeaders() as $name => $values) {
            $topPart .= "{$name}: " . implode(', ', $values) . "\n";
        }
        $topPart .= "\n";

        // Save our response for debugging
        // file_put_contents("{$this->tmp_dir}/{$this->req_num}.request", print_r(curl_getinfo($curl), true));
        file_put_contents("{$this->tmp_dir}/{$this->req_num}.response", $topPart.$http_response);
        // curl_close($curl);

        // Parse the page!
        $page = new HttpBotPage($this, $topPart.$http_response, $url);

/*
        // Check for a redirect since CURL won't do this (and we are probably better off with handling
        // the cookies ourself anyway)
        if (($page->http_code == 302 || $page->http_code == 301) && (!empty($page->headers['location']))) {
            // Found a legitimate redirect
            $redirect = $page->fullURL($page->headers['location']);
            $this->addLog("{$page->http_code} redirect to $redirect");
            //return $this->get($redirect);
            return $this->post($redirect, null, 'GET', $emulate);
        }
*/

        // If we have a text/html content type, then emulate a browser (fetch page elements, sleep, etc)
        if ($this->emulate_browser && $emulate && ($page->http_code == 200) && (!empty($page->headers['content-type'])) && preg_match('#text/html#i', $page->headers['content-type'])) {
            $page->emulateBrowser();
        }

        return $page;
    }

    // Post data as JSON
    public function get($url, $emulate = true, $use_cache = true)
    {
        // TODO - should technically do more here having to do with caching headers, but this should be good for now
        if ($use_cache && isset($this->retrieved[$url])) {
            // Return the cached data from disk
            if (file_exists("{$this->tmp_dir}/{$this->retrieved[$url]}.response")) {
                $cached_response = file_get_contents("{$this->tmp_dir}/{$this->retrieved[$url]}.response");
                return new HttpBotPage($this, $cached_response, $url);
            } else {
                // echo "{$this->tmp_dir}/{$this->retrieved[$url]}.response was supposed to exist but doesn't\n";
                return '';
            }
        }

        // Just call $http_bot::post() which does all of the real work
        $response = $this->post($url, null, 'GET', $emulate);
        $this->retrieved[$url] = $this->req_num;
        return $response;
    }

    // Function specifically for downloading a file.  Probably unnecessary, but keeping around just in case
    public function file($url, $output_file)
    {
        $this->req_num++;
        $this->addLog("Requesting FILE $url");
        $cookie_file = $this->cookieFile($url);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Get returned value as string (don't put to screen)
        curl_setopt($curl, CURLOPT_USERAGENT, $this->ua);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookie_file);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $cookie_file);
        curl_setopt($curl, CURLOPT_REFERER, $this->referer);
        // CURLOPT_FOLLOLOCATION is not allowed.. See http://br.php.net/manual/ro/function.curl-setopt.php#71313
        //curl_setopt($curl, CURLOPT_FOLLOWLOCATION,  true);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 20);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true); // for use with debugging below
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept-Charset: ISO-8859-1'));
        //curl_setopt($curl, CURLOPT_HEADER, true);
        $file_out = fopen($output_file, 'wb');
        curl_setopt($curl, CURLOPT_FILE, $file_out);
        $output = curl_exec($curl); // Execute!
        file_put_contents("{$this->tmp_dir}/{$this->req_num}.request", print_r(curl_getinfo($curl), true));
        file_put_contents("{$this->tmp_dir}/{$this->req_num}.response.file", $output);
        curl_close($curl);
        return $output;
    }

    // Some things pass three arguments, and others just pass onee
    public function log($message = '', $message2 = '', $message3 = '')
    {
        if (strtolower($message) == 'get' || strtolower($message) == 'post') {
            $message = $message3;
        }
        $message = ($this->log_prefix) ? "{$this->log_prefix} {$message}" : $message;
        $this->addLog($message);
    }

    public function addLog($message)
    {
        $fh = fopen("{$this->tmp_dir}/log.txt", 'a');
        fwrite($fh, date('Y-m-d H:i:s') . " - $message\n");
        fclose($fh);
    }
}
