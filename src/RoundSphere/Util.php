<?php

namespace RoundSphere;

use Exception;
use Mandrill;

// RoundSphere Utility Functions
class Util
{
    static function ValidEmail($email)
    {
        if ((!empty($email)) && is_string($email) && preg_match("/^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/", $email)) {
            return 1;
        } else {
            return 0;
        }
    }

    static function randomString($size, $charset = null)
    {
        if ($charset == null) {
            $charset = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        }
        $string = '';
        for ($i = 0; $i < $size; $i++) {
            $string .= $charset[(mt_rand(0, (strlen($charset) - 1)))];
        }
        return $string;
    }

    static function hashByField($things, $field)
    {
        $retval = null;
        for ($i = 0; $i < count($things); $i++) {
            if (isset($things[$i][$field])) {
                $retval[$things[$i][$field]] = $things[$i];
            }
        }
        return $retval;
    }

    static function sortObjectByField(&$array, $field, $direction = SORT_ASC, $second_field = '', $second_direction = SORT_ASC)
    {
        $sortcol = array();
        $second_col = array();
        foreach ($array as $key => $row) {
            $sortcol[$key] = $row->$field;
            if ($second_field) {
                $second_col[$key] = $row->$second_field;
            }
        }
        if ($second_field) {
            return array_multisort($sortcol, $direction, $second_col, $second_direction, $array);
        } else {
            return array_multisort($sortcol, $direction, $array);
        }
    }

    static function sortHashByField(&$hash, $field, $direction = SORT_ASC, $second_field = '', $second_direction = SORT_ASC)
    {
        $sortcol = array();
        $second_col = array();
        if (is_array($hash)) {
            foreach ($hash as $key => $row) {
                $sortcol[$key] = isset($row[$field]) ? $row[$field] : '';
                if ($second_field) {
                    $second_col[$key] = $row[$second_field];
                }
            }
            if ($second_field) {
                return array_multisort($sortcol, $direction, $second_col, $second_direction, $hash);
            } else {
                return array_multisort($sortcol, $direction, $hash);
            }
        } else {
            return array();
        }
    }

    static function orderBy(&$data, $field, $sortdir = 'ASC')
    {
        $code = "return strnatcmp(\$a->$field, \$b->$field);";
        switch ($sortdir) {
            case 'DESC':
                usort($data, create_function('$b,$a', $code));
                break;
            default:
                usort($data, create_function('$a,$b', $code));
                break;
        }
    }

    static function orderByArray(&$data, $field, $sortdir = 'ASC')
    {
        $code = "return strnatcasecmp(\$a['$field'], \$b['$field']);";
        switch ($sortdir) {
            case 'DESC':
                usort($data, create_function('$b,$a', $code));
                break;
            default:
                usort($data, create_function('$a,$b', $code));
                break;
        }
    }

    static function in_iarray($str, $a)
    {
        foreach ($a as $v) {
            if (strcasecmp($str, $v) == 0) {
                return true;
            }
        }
        return false;
    }

    static function array_iunique($a)
    {
        $n = array();
        foreach ($a as $k => $v) {
            if (!in_iarray($v, $n)) {
                $n[$k] = $v;
            }
        }
        return $n;
    }

    // looks for $needle in array and returns
    // index into array
    static function array_index($arr, $needle)
    {
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i] == $needle)
                return $i;
        }
        // not found
        return -1;
    }

    static function sortHashOfObjectsByField(&$hash, $field, $direction = SORT_ASC, $second_field = '', $second_direction = SORT_ASC)
    {
        $sortcol = array();
        $second_col = array();
        if (is_array($hash)) {
            foreach ($hash as $key => $row) {
                $sortcol[$key] = $row->$field;
                if ($second_field) {
                    $second_col[$key] = $row->$second_field;
                }
            }
            if ($second_field) {
                return array_multisort($sortcol, $direction, $second_col, $second_direction, $hash);
            } else {
                return array_multisort($sortcol, $direction, $hash);
            }
        } else {
            return array();
        }
    }

    static function remoteIP()
    {
        //return (getenv('HTTP_X_FORWARDED_FOR')) ? getenv('HTTP_X_FORWARDED_FOR') : getenv('REMOTE_ADDR');
        $ip = null;
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($ips as $thisIp) {
                $thisIp = trim($thisIp);
                if ($ip) continue;
                if (!preg_match('#([0-9\.]+)#', $thisIp, $matches)) {
                    // Initial regex check
                    continue;
                }
                // don't match Private IP Addreses
                if (preg_match('#(^127\.0\.0\.1)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)#', $thisIp)) {
                    continue;
                }
                // Make sure that the forwarded-for is a valid format
                if (preg_match('#^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$#', $thisIp)) {
                    $ip = $thisIp;
                }
            }
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = null;
        }
        return $ip;
    }

    static function timed_file($filename, $timeout = 1, $lines = false)
    {
        $start_time = time();
        $file = fopen($filename, 'r');
        if (!$file) {
            return false;
        }
        stream_set_blocking($file, FALSE);
        stream_set_timeout($file, $timeout);
        $contents = '';
        $status = socket_get_status($file);
        while (!feof($file) && !$status['timed_out']) {
            $contents .= fread($file, 8192);
            $status = socket_get_status($file);
            if (time() - $start_time > $timeout) $status["timed_out"] = true;
        }

        if ($status['timed_out']) return false;

        // Split on newlines if $lines (similar in functionality to php's file() function)
        if ($lines) {
            $contents = explode("\n", $contents);
            $last = count($contents);
            // Drop the last element if it is blank
            if ($last > 1 && empty($contents[$last - 1])) {
                unset($contents[$last - 1]);
            }
        }
        return $contents;
    }

    static public function shortId($int)
    {
        // We don't wan't to use 2 confusing letters (i or o's), so we first convert to base34 (missing Y and Z)
        $base34 = strtoupper(str_pad(base_convert($int, 10, 34), 3, '0', STR_PAD_LEFT));
        // Then replace the I's and O's, with Y's and Z's
        $short_id = str_replace(
            array('I', 'O'),
            array('Y', 'Z'),
            $base34);
        return $short_id;
    }

    static public function fromShortId($short_id)
    {
        // Convert the Y's and Z's back into I's and O's
        $base34 = str_replace(
            array('Y', 'Z'),
            array('I', 'O'),
            $short_id);
        // Then convert it back from base34 to base10
        $int = base_convert($base34, 34, 10);
        return $int;
    }

    static public function removeOutliers($array, $width = 1)
    {
        $stddev = stats_standard_deviation($array);
        $avg = array_sum($array) / count($array);

        $cleaned = array();
        $low = $avg - ($stddev * $width);
        $high = $avg + ($stddev * $width);

        foreach ($array as $value) {
            if (($value >= $low) && $value <= $high) {
                $cleaned[] = $value;
            }
        }
        return $cleaned;
    }

    static public function lowestInArray($array)
    {
        sort($array, SORT_NUMERIC);
        // Don't want a negative threshold
        while (isset($array[0]) && $array[0] < 0) {
            array_shift($array);
        }
        return (isset($array[0])) ? $array[0] : null;
    }

    static public function array_combine_expand($a, $b, $pad = TRUE)
    {
        $acount = count($a);
        $bcount = count($b);
        // more elements in $a than $b but we don't want to pad either
        if (!$pad) {
            $size = ($acount > $bcount) ? $bcount : $acount;
            $a = array_slice($a, 0, $size);
            $b = array_slice($b, 0, $size);
        } else {
            // more headers than row fields
            if ($acount > $bcount) {
                $more = $acount - $bcount;
                // how many fields are we missing at the end of the second array?
                // Add empty strings to ensure arrays $a and $b have same number of elements
                $more = $acount - $bcount;
                for ($i = 0; $i < $more; $i++) {
                    $b[] = "";
                }
                // more fields than headers
            } else if ($acount < $bcount) {
                $more = $bcount - $acount;
                // fewer elements in the first array, add extra keys
                for ($i = 0; $i < $more; $i++) {
                    $key = 'extra_field_0' . $i;
                    $a[] = $key;
                }
            }
        }
        return array_combine($a, $b);
    }

    // Exponential Moving Average
    // From http://next.randomfoo.net/blog/id/3903
    static public function expma($in)
    {
        $out[0] = $in[0];
        for ($i = 1; $i < sizeof($in); $i++) {
            $out[$i] = $out[$i - 1] + 0.1 * ($in[$i] - $out[$i - 1]);
        }
        return $out;
    }

    static public function avgTopFromArray($array, $number)
    {
        $relevant = array_slice($array, 0, $number);
        return (array_sum($relevant) / count($relevant));
    }

    static public function roundToQuarter($amount)
    {
        $four_times = $amount * 4;
        $rounded_times_four = round($four_times);
        $return = round($rounded_times_four / 4, 2);
        return $return;
    }

    static public function addMonth($date_str, $months = 1)
    {
        $date = new \DateTime($date_str);
        $start_day = $date->format('j');

        $date->modify("+{$months} month");
        $end_day = $date->format('j');

        if ($start_day != $end_day) {
            $date->modify('last day of last month');
        }
        return $date->format('Y-m-d');
    }

    static public function mwsTimestamp($timestamp)
    {
        return date('Y-m-d', $timestamp).'T'.date('H:i:s', $timestamp).'Z';
    }

    static public function timezoneList($usOnly = false)
    {
        $usZones = array(
            'UTC',
            'America/Anchorage',
            'America/Denver',
            'America/Phoenix',
            'America/Chicago',
            'America/Los_Angeles',
            'America/New_York',
        );
        $zones_array = array();
        $timestamp = time();
        $prev = date_default_timezone_get();
        foreach(timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            if ((!$usOnly) || ($usOnly && in_array($zone, $usZones))) {
                $zones_array[$key]['zone'] = $zone;
                $zones_array[$key]['diff'] = 'UTC '.date('P', $timestamp);
                $zones_array[$key]['display'] = $zone . ' (UTC '.date('P', $timestamp).')';
            }
        }
        date_default_timezone_set($prev);
        return $zones_array;
    }

    static public function strtotimeUtc($string)
    {
        $string .= ' UTC';
        return strtotime($string);
    }

    static public function md5_base64($data)
    {
        return base64_encode(pack('H*', md5($data)));
    }

    static public function ago($tstamp)
    {
        //$tstamp = strtotime($tstamp);
        $tstamp = is_numeric($tstamp) ? $tstamp : strtotime($tstamp);
        if ($tstamp <= 0) return 'Never';
        $seconds = time() - $tstamp;

        $minutes = intval($seconds / 60);
        $seconds = $seconds % 60;

        $hours = intval($minutes / 60);
        $minutes = $minutes % 60;

        $days = intval($hours / 24);
        $hours = $hours % 24;

        $weeks = intval($days / 7);
        $days = $days % 7;

        $months = intval($weeks / 4);
        $weeks = $weeks % 4;

        $years = intval($months / 12);
        $months = $months % 12;
        // echo "years={$years} months={$months} weeks={$weeks} days={$days} hours={$hours} mins={$minutes} secs={$seconds}<br />\n";

        if ($diff = Util::formatSince($years, 'year', $months, 'month')) {
        } elseif ($diff = Util::formatSince($months, 'month', $days, 'day')) {
        } elseif ($diff = Util::formatSince($weeks, 'week', $days, 'day')) {
        } elseif ($diff = Util::formatSince($days, 'day', $hours, 'hour')) {
        } elseif ($diff = Util::formatSince($hours, 'hour', $minutes, 'minute')) {
        } elseif ($diff = Util::formatSince($minutes, 'minute', $seconds, 'second')) {
        } else {
            $diff = "seconds";
        }
        return $diff . " ago";
    }

    static public function formatSince($sum1, $desc1, $sum2, $desc2)
    {
        if ($sum1 == 1 && $sum2 == 0) {
            $diff = "1 $desc1";
        } elseif ($sum1 == 1 && $sum2 == 1) {
            $diff = "1 $desc1";
        } elseif ($sum1 == 1 && $sum2 > 1) {
            $diff = "1 $desc1";
        } elseif ($sum1 > 1 && $sum2 == 1) {
            $diff = "$sum1 {$desc1}s";
            //} elseif($sum1 > 1 && $sum2 > 1) {
        } elseif ($sum1 > 1) {
            $diff = "$sum1 {$desc1}s";
        } else {
            return false;
        }
        return $diff;
    }

    static public function array_to_xml($data, &$xmlobj)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    // $key = "i{$key}";   // XML doesn't allow integer tags?
                    $subnode = $xmlobj->addChild($key);
                    self::array_to_xml($value, $subnode);
                } else {
                    self::array_to_xml($value, $xmlobj);
                }
            } else {
                /*
                                if (is_numeric($key)) {
                                    $key = "i{$key}";
                                }
                */
                $xmlobj->addChild($key, $value);
            }
        }
    }

    static public function mb_unserialize($string)
    {
        $string = preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $string);
        return unserialize($string);
    }

    function uniqueId()
    {
        return base_convert(time(), 10, 36) . Util::randomString(4);
    }

    static public function obfuscatedEmailAddress($email)
    {
        $alwaysEncode = array('.', ':', '@');

        $result = '';

        // Encode string using oct and hex character codes
        for ($i = 0; $i < strlen($email); $i++) {
            // Encode 25% of characters including several that always should be encoded
            if (in_array($email[$i], $alwaysEncode) || mt_rand(1, 100) < 25) {
                if (mt_rand(0, 1)) {
                    $result .= '&#' . ord($email[$i]) . ';';
                } else {
                    $result .= '&#x' . dechex(ord($email[$i])) . ';';
                }
            } else {
                $result .= $email[$i];
            }
        }
        return $result;
    }

    static public function obfuscatedEmailLink($email, $params = array())
    {
        if (!is_array($params)) {
            $params = array();
        }

        // Tell search engines to ignore obfuscated uri
        if (!isset($params['rel'])) {
            $params['rel'] = 'nofollow';
        }

        $neverEncode = array('.', '@', '+'); // Don't encode those as not fully supported by IE & Chrome

        $urlEncodedEmail = '';
        for ($i = 0; $i < strlen($email); $i++) {
            // Encode 25% of characters
            if (!in_array($email[$i], $neverEncode) && mt_rand(1, 100) < 25) {
                $charCode = ord($email[$i]);
                $urlEncodedEmail .= '%';
                $urlEncodedEmail .= dechex(($charCode >> 4) & 0xF);
                $urlEncodedEmail .= dechex($charCode & 0xF);
            } else {
                $urlEncodedEmail .= $email[$i];
            }
        }

        $obfuscatedEmail = self::obfuscatedEmailAddress($email);
        $obfuscatedEmailUrl = self::obfuscatedEmailAddress('mailto:' . $urlEncodedEmail);

        $link = '<a href="' . $obfuscatedEmailUrl . '"';
        foreach ($params as $param => $value) {
            $link .= ' ' . $param . '="' . htmlspecialchars($value) . '"';
        }
        $link .= '>' . $obfuscatedEmail . '</a>';

        return $link;
    }

    static public function mandrillMail($recipient, $subject, $message, $from = null)
    {
        $mandrillApiKey = Properties::getProperty('mandrillApiKey');
        if (!$mandrillApiKey) {
            $mandrillApiKey = Properties::getProperty('mandrill_api_key');
        }
        if (!$mandrillApiKey) {
            return false;
        }
        $mandrill = new Mandrill($mandrillApiKey);
        try {
            $mandrill->messages->send(array(
                'text' => $message,
                'subject' => $subject,

                'from_email' => ($from ? $from : 'automation@roundsphere.com'),
                'to' => array(array(
                    'email' => $recipient
                )),
                'tags' => array('utilclass'),
                'metadata' => array('website' => Properties::getProperty('website')),
            ));
            return true;
        } catch (Exception $e) {
            // mail('brandon@bookscouter.com', 'Mandrill Error on searchrank-localDeploy', print_r($e, true));
            return false;
        }
    }

    static public function blankPng()
    {
        return base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');
    }
    static public function sendBlankPng()
    {
        header('Content-Type: image/png');
        echo self::blankPng();
    }

    static public function blankGif()
    {
        return base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAALAAAAAABAAEAAAICBAEAOw==');
    }
    static public function sendBlankGif()
    {
        header('Content-Type: image/gif');
        return self::blankGif();
    }

    static public function signPaapiUrl($url, $secret_key)
    {
        $original_url = $url;
        if (strpos($original_url, "Signature") > 0) {
            return $url;
        } else {
            // Decode anything already encoded
            $url = urldecode($url);

            // Parse the URL into $urlparts
            $urlparts = parse_url($url);

            // Build $params with each name/value pair
            foreach (explode('&', $urlparts['query']) as $part) {
                if (strpos($part, '=')) {
                    list($name, $value) = explode('=', $part, 2);
                } else {
                    $name = $part;
                    $value = '';
                }
                $params[$name] = $value;
            }

            // Include a timestamp if none was provided
            if (empty($params['Timestamp'])) {
                $params['Timestamp'] = gmdate('Y-m-d\TH:i:s\Z');
            }

            // Sort the array by key
            ksort($params);

            // Build the canonical query string
            $canonical = '';
            foreach ($params as $key => $val) {
                $canonical .= "$key=" . rawurlencode($val) . "&";
            }
            // Remove the trailing ampersand
            $canonical = preg_replace("/&$/", '', $canonical);
            // Some common replacements and ones that Amazon specifically mentions
            $canonical = str_replace(array(' ', '+', ',', ';'), array('%20', '%20', urlencode(','), urlencode(':')), $canonical);
            //bclog("\$canonical = $canonical");

            // Build the si
            $string_to_sign = "GET\n{$urlparts['host']}\n{$urlparts['path']}\n$canonical";
            // Calculate our actual signature and base64 encode it
            $signature = base64_encode(hash_hmac('sha256', $string_to_sign, $secret_key, true));

            // Finally re-build the URL with the proper string and includ the Signature
            $url = "{$urlparts['scheme']}://{$urlparts['host']}{$urlparts['path']}?$canonical&Signature=" . rawurlencode($signature);
            // bclog("Signed\n$original_url \nas\n$url");
            return $url;
        }
    }
}

