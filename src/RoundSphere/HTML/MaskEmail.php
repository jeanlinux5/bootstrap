<?php

namespace RoundSphere\HTML;

class MaskEmail {
    public static function render($params)
    {
        $address = isset($params['address']) ? $params['address'] : '';
        $method  = isset($params['method'])  ? $params['method']  : 'hex';

        switch ($method) {
            case 'javascript':
                $js_encode = '';
                for ($x=0; $x < strlen($address); $x++) {
                    $js_encode .= '%' . bin2hex($address[$x]);
                }

                return '<script type="text/javascript">eval(unescape(\''.$js_encode.'\'))</script>';
                break;

            case 'javascript_charcode':
                for($x = 0, $y = strlen($address); $x < $y; $x++ ) {
                    $ord[] = ord($address[$x]);
                }
                $_ret = "<script type=\"text/javascript\" language=\"javascript\">\n";
                $_ret .= "<!--\n";
                $_ret .= "{document.write(String.fromCharCode(";
                $_ret .= implode(',',$ord);
                $_ret .= "))";
                $_ret .= "}\n";
                $_ret .= "//-->\n";
                $_ret .= "</script>\n";
                return $_ret;
                break;

            case 'hex':
            default:
                /*
                            $address_encode = '';
                            for ($x = 0; $x < strlen($address); $x++) {
                                if (preg_match('!\w!',$address[$x])) {
                                    $address_encode .= '%' . bin2hex($address[$x]);
                                } else {
                                    $address_encode .= $address[$x];
                                }
                            }
                */
                $text_encode = '';
                for ($x = 0; $x < strlen($address); $x++) {
                    $text_encode .= '&#x' . bin2hex($address[$x]).';';
                }
                return $text_encode;

                break;
        }
    }
}
