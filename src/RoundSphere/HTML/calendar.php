<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class Calendar
{
    public static function render($params = array())
    {
        $title = isset($params['title']) ? $params['title'] : "";
        $name = isset($params['name']) ? $params['name'] : "textbox";
        $id = isset($params['id']) ? $params['id'] : $name;
        $layout = isset($params['layout']) ? $params['layout'] : "plain";
        $extra = isset($params['extra']) ? $params['extra'] : "";
        $previous = isset($params['previous']) ? $params['previous'] : "";
        $format = isset($params['format']) ? $params['format'] : "YYYY-MM-DD";
        $required = isset($params['required']) ? $params['required'] : "false";
        $default = isset($params['default']) ? $params['default'] : "today";

        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        ## if $previous wasn't obtained from $params, then look in the $_REQUEST
        ## if it's not in previous, then use the $default parameter
        if (!$previous) {
            switch ($default) {
                case 'today':
                    $default = date('Y-m-d');
                    break;
                case 'NULL':
                case NULL:
                default:
                    $default = NULL;
                    break;
            }
            $previous = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, $default);
        }
        $previous = preg_replace('#( [0-9]{2}:[0-9]{2}:[0-9]{2})#', '', $previous);

        // TODO: Make sure that $previous is in the proper format here (ie, remove the 'time' portion from a mysql formated dattime field)

        $html_result = '';
        static $calendar_javascript_included = false;
        if (!$calendar_javascript_included) {
            $html_result .= '<script language="javascript" type="text/javascript" src="/js/calendarDateInput.js"></script>';
            $calendar_javascript_included = true;
        }

        ## Create the textbox, based on the selected layout
        switch ($layout) {
            case "row":
            case "2cols":
                $content = "<td valign=\"top\">$title";
                $content .= "</td><td valign=\"top\">\n";
                $content .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $content .= "<script language=\"javascript\">DateInput('$name', $required, '$format'";
                $content .= $previous ? ", '$previous'" : ' ';
                $content .= ");</script>\n";
                $content .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $content .= "</td>\n";
                ## Add the enclosing <tr> tags if layout = "row"
                $html_result .= ($layout == 'row') ? "<tr>$content</tr>" : $content;
                break;

            case "plain":
            default:
                $html_result .= $title;
                $html_result .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $html_result .= "<script language=\"javascript\">DateInput('$name', $required, '$format'";
                $html_result .= $previous ? ", '$previous'" : ' ';
                $html_result .= ");</script>\n";
                $content = (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;

        }
        return $html_result;
    }
}



