<?php

namespace RoundSphere\HTML;

class SortUrl {
    public static function render($params)
    {

        //$link =  isset($params['SCRIPT_NAME']) ? $params['SCRIPT_NAME'] : $smarty->get_template_vars("SCRIPT_NAME");
        $link = '';
        $link .= "?";

        $sortfield      = isset($params['sortfield'])    ? $params['sortfield']     : 'order';
        $orderfield     = isset($params['orderfield'])   ? $params['orderfield']    : 'dir';
        $default        = isset($params['default'])      ? $params['default']       : '';
        $thisfield      = isset($params['thisfield'])    ? $params['thisfield']     : '';
        $otherparams    = isset($params['otherparams'])  ? $params['otherparams']   : array();   //Array containing other params for the url.
        $otherparams  = isset($params['otherparams'])    ? $params['otherparams']  : ''; //Array containing other params for the url.

        $current_sort   = isset($_GET[$sortfield]) ? $_GET[$sortfield] : '';

        // Loop through all of the current parameters
        foreach($_GET as $key => $value) {
            if($key == $sortfield) {
                // Do nothing
            } elseif($key == $orderfield) {
                $sort_value = $value;
            } else {
                if (is_array($value)) {
                    foreach ($value as $subKey=> $subValue) {
                        $link .= "{$key}[{$subKey}]=".urlencode($subValue)."&";
                    }
                } else {
                    $link .= "{$key}=".urlencode($value)."&";
                }
            }
        }

        $link .= "{$sortfield}=".urlencode($thisfield) ."&";
        $sort_value = !empty($_GET[$orderfield]) ? $_GET[$orderfield] : 'asc';

        $arrowdir = false;
        if($current_sort == $thisfield) {
            switch($sort_value) {
                case 'in':
                    $value      = 'out';
                    $arrowdir   = 'down';
                    break;
                case 'out':
                    $value      = 'in';
                    $arrowdir   = 'up';
                    break;
                case 'asc':
                    $value      = 'desc';
                    $arrowdir   = 'up';
                    break;
                case 'desc':
                    $value      = 'asc';
                    $arrowdir   = 'down';
                    break;
                case 'up':
                    $value      = 'down';
                    $arrowdir   = 'down';
                    break;
                case 'down':
                    $value      = 'up';
                    $arrowdir   = 'up';
                    break;
                default:
                    $value = $sort_value;
                    $arrowdir   = 'up';
                    break;
            }
            $link .= "{$orderfield}=".urlencode($value)."&";
        } else {
            $link .= "$orderfield=".urlencode($default)."&";
        }

        $img = "/images/icons/$arrowdir.png";

        if (is_array($otherparams) && $otherparams) {
            foreach ($otherparams as $param_name => $param_value) {
                if ($param_name != 'anchor') {
                    $link .= "$param_name=".urlencode($param_value)."&";
                } //Leave the anchor till the end of the link
                elseif ($param_name == 'anchor') { $anchor = $param_value; };
            }
        }

        // Remove the last &
        $link = preg_replace("/&$/", '', $link);
        if (!empty($anchor)) {
            $link .= "#$anchor";
        }

        if(isset($params['showlink']) && $params['showlink']) {
            $return = "<a href=\"$link\">".$params['showlink'];
            $return .= $arrowdir ? "<img src=\"$img\" alt=\"$arrowdir\" border=\"0\" class='arrowimage'/>" : '';
            $return .= "</a>\n";
            return $return;
        } else {
            return $link;
        }
    }
}
