<?php

namespace RoundSphere\HTML;

class SameUrl
{
    public static function render($params)
    {
        $link = isset($params['SCRIPT_NAME']) ? $params['SCRIPT_NAME'] : $GLOBALS['t']->vars['SCRIPT_NAME'];
        $link .= "?";

        $results = array_merge($_GET, $_POST, $params);

        $showlink = false;

        foreach ($results as $key => $value) {
            switch ($key) {
                case "showlink":
                case "SCRIPT_NAME":
                    // do nothing here
                    break;
                default:
                    $value = isset($params[$key]) ? $params[$key] : $value;
                    $link .= isset($value) ? "$key=" . urlencode($value) . "&" : '';
                    break;
            }
        }
        $link = preg_replace('/&$/', '', $link);
        if (isset($params['showlink']) && $params['showlink']) {
            if ($link == $_SERVER['REQUEST_URI']) {
                return "<b>" . $params['showlink'] . "</b>\n";
            } else {
                return "<a href=\"$link\">" . $params['showlink'] . "</a>\n";
            }
        } else {
            return $link;
        }
    }
}
