<?php

namespace RoundSphere\HTML;

use Exception;

/**
 * Class Html
 * @package RoundSphere\HTML
 */
class Html {
    public static function display($type, $params) {
        switch($type) {
            case "calendar":
                return Calendar::render($params);
            case "checkbox":
                return Checkbox::render($params);
            case "cycle":
                return Cycle::render($params);
            case "date":
                return Date::render($params);
            case "input":
                return Input::render($params);
            case "hidden":
                return Input::renderHidden($params);
            case "password":
                return Input::renderPassword($params);
            case "mask_email":
                return MaskEmail::render($params);
            case "openxad":
                return Openxad::render($params);
            case "radio":
                return Radio::render($params);
            case "same_url":
            case "sameurl":
                return SameUrl::render($params);
            case "select":
                return Select::render($params);
            case "select_day":
                return Select::renderDay($params);
            case "select_month":
                return Select::renderMonth($params);
            case "select_year":
                return Select::renderYear($params);
            case "select_state":
                return Select::renderState($params);
            case "select_multiple":
            case "selectmultiple":
                return SelectMultiple::render($params);
            case "show_only":
                return ShowOnly::render($params);
            case "sort_url":
            case "sorturl":
                return SortUrl::render($params);
            case "textarea":
                return Textarea::render($params);
            default:
                throw new Exception('Unknown element type');
        }
    }
}
