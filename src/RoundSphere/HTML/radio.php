<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class Radio
{
    public static function render($params)
    {

        $display_col = isset($params['display_col']) ? $params['display_col'] : '';
        $value_col = isset($params['value_col']) ? $params['value_col'] : '';
        $title = isset($params['title']) ? $params['title'] : '';
        $name = isset($params['name']) ? $params['name'] : '';
        $layout = isset($params['layout']) ? $params['layout'] : '';
        $previous = isset($params['selected']) ? $params['selected'] : null;
        $extra = isset($params['extra']) ? $params['extra'] : '';
        $default = isset($params['default']) ? $params['default'] : '';
        $split = isset($params['split']) ? $params['split'] : '';
        $class = isset($params['class']) ? $params['class'] : '';


        if (isset($params['values'])) {
            $values = $params['values'];
        } else {
            bclog('bcradio requires a "values" paramater');
            return null;
        }

        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        /*
            if (!empty($params['tooltip'])) {
                $title .= template::tooltip($params['tooltip']);
            }
        */


        /*
            if ($previous === null) {
                $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
                $previous = ($previous_var === null) ? $default : $previous_var;
            }
        */
        if ($previous === null) {
            if (preg_match('#(.*)\[(.*)\]#', $name, $matches)) {
//echo "sub";
                $previous_var = isset($GLOBALS['t']->vars['previous'][$matches[1]][$matches[2]]) ? $GLOBALS['t']->vars['previous'][$matches[1]][$matches[2]] : $default;
            } else {
                $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            }
            $previous = ($previous_var === null) ? $default : $previous_var;
        }

//echo "previous = $previous\n";

        $poss_values = array();

        $i = 0;
        if ($value_col && $display_col && is_array($values)) {
            foreach ($values as $key => $value) {
                $poss_values[$i]['value'] = ($value_col == '_key') ? $key : $value[$value_col];
                $poss_values[$i]['display'] = $value[$display_col];
                $i++;
            }
        } elseif ($value_col && is_array($values)) {
            foreach ($values as $key => $value) {
                $poss_values[$i]['value'] = ($value_col == '_key') ? $key : $value[$value_col];
                $poss_values[$i]['display'] = $value;
                $i++;
            }
        } elseif (is_array($values)) {
            foreach ($values as $value) {
                if (is_array($value)) {
                    $poss_values[$i] = $value;
                } else {
                    $poss_values[$i]['value'] = $value;
                    $poss_values[$i]['display'] = $value;
                }
                $i++;
            }
        } else {
            foreach (explode(',', $values) as $value) {
                $this_value = '';
                $this_display = '';
                if (preg_match('/:/', $value)) {
                    list($this_value, $this_display) = explode(':', $value, 2);
                    if (!$this_display) {
                        $this_display = $my_value;
                    }
                } else {
                    $this_display = $value;
                    $this_value = $value;
                }
                $poss_values[$i]['value'] = $this_value;
                $poss_values[$i]['display'] = $this_display;
                $i++;
            }
        }

        if (isset($params['addblank'])) {
            array_unshift($poss_values, array('value' => '', 'display' => $params['addblank']));
        }

        $html_result = '';
        $selected = 0;

        switch ($layout) {
            case 'row':
            case '2cols':
                $html_result .= "<td>$title";
                $html_result .= "</td><td>\n";
                if ($error) {
                    $html_result .= "<span class=\"input_error\">$error</span><br />";
                }
                foreach ($poss_values as $this_value) {
                    $html_result .= "<input type=\"radio\" name=\"$name\" class=\"{$class}\"  value=\"" . $this_value['value'] . "\"";
                    if ($previous == $this_value['value'] && (!$selected)) {
                        $selected = 1;
                        $html_result .= " checked=\"checked\"";
                    }
                    $html_result .= " $extra />" . $this_value['display'] . "{$split}\n";
                }
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $html_result .= "</td>\n";
                if ($layout == 'row') {
                    $html_result = "<tr>\n$html_result\n</tr>\n";
                }
                break;

            case 'plain':
            default:
                $html_result .= "$title ";
                if ($error) {
                    $html_result .= "<span class=\"input_error\">$error</span><br />";
                }
                foreach ($poss_values as $this_value) {
                    $html_result .= "<input type=\"radio\" name=\"$name\"  class=\"{$class}\" value=\"" . $this_value['value'] . "\"";
                    if ($previous == $this_value['value'] && (!$selected)) {
                        $selected = 1;
                        $html_result .= " checked=\"checked\"";
                    }
                    $html_result .= " $extra />" . $this_value['display'] . "{$split}\n";
                }
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;

        }
        return $html_result;
    }
}
