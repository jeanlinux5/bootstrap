<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class Date {
    public static function render($params)
    {
        static $included_javascript = false;
        ## Retrieve various settings from passed-in arguments
        $title          = isset($params['title'])       ? $params['title']        : '';
        $name           = isset($params['name'])        ? $params['name']         : 'date';
        $id             = isset($params['id'])          ? $params['id']           : $name;
        $layout         = isset($params['layout'])      ? $params['layout']       : 'plain';
        $extra          = isset($params['extra'])       ? $params['extra']        : '';
        $previous       = isset($params['previous'])    ? $params['previous']     : null;
        $noselect       = isset($params['noselect'])    ? $params['noselect']     : '';
        $format         = isset($params['format'])      ? $params['format']       : 'YYYY-MM-DD';
        $required       = isset($params['required'])    ? $params['required']     : 'false';
        $default        = isset($params['default'])     ? $params['default']      : 'today';

        ## Add a space to $extra to make formatting consistent
        $extra = "$extra ";

        if (!$included_javascript) {
            echo '<script src="/js/calendarDateInput.js" type="text/javascript" language="javascript"></script>';
            $included_javascript = true;
        }

        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        // if $previous wasn't obtained from $params, then look in $_REQUEST
        // if it's not in previous, then use the $default parameter
        if ($previous === null) {
            $default = ($default == 'today') ? date('Y-m-d') : $default;
            switch($default) {
                case 'today':
                    $default = date('Y-m-d');
                    break;
                case 'NULL':
                    $default = NULL;
                    break;
            }
            $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            $previous = ($previous_var === null) ? $default : $previous_var;//? $previous_var : $default;
        }
        // Match date in the format '2008-06-02 06:07:08'
        if (preg_match('/^(\d{4}-\d{2}-\d{2}) \d{2}:\d{2}:\d{2}$/', $previous, $matches)) {
            // Shorten mysql timestamps down to just the date
            $previous = $matches[1];
        }

        // TODO: Make sure that $previous is in the proper format here (ie, remove the 'time' portion from a mysql formated dattime field)


        $html_result = '';

        ## Create the textbox, based on the selected layout
        switch($layout) {
            case "row":
            case "2cols":
                $content = "<td valign=\"top\">$title";
                $content .= "</td><td valign=\"top\">\n";
                $content .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $content .= "<script language=\"javascript\">DateInput('$name', $required, '$format'";
                $content .= $previous ? ", '$previous'" : ' ';
                $content .= ");</script>\n";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $content .= "</td>";
                ## Add the enclosing <tr> tags if layout = "row"
                $html_result .= ($layout == "row") ? "<tr>$content</tr>" : $content;
                break;

            case "plain":
            default:
                $html_result .= $title;
                $html_result .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $html_result .= "<script language=\"javascript\">DateInput('$name', $required, '$format'";
                $html_result .= $previous ? ", '$previous'" : ' ';
                $html_result .= ");</script>";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;
        }

        return $html_result;
    }
}
