<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class SelectMultiple
{
    public static function render($params)
    {
        // Retrieve all of the values passed in
        $display_col = isset($params['display_col']) ? $params['display_col'] : '';
        $value_col = isset($params['value_col']) ? $params['value_col'] : '';
        $title = isset($params['title']) ? $params['title'] : '';
        $name = isset($params['name']) ? $params['name'] : '';
        $layout = isset($params['layout']) ? $params['layout'] : '';
        $previous = isset($params['selected']) ? $params['selected'] : null;
        $extra = isset($params['extra']) ? $params['extra'] : '';
        $noselect = isset($params['noselect']) ? $params['noselect'] : '';
        $default = isset($params['default']) ? $params['default'] : array();
        $values = isset($params['values']) ? $params['values'] : '';
        $class = isset($params['class']) ? $params['class'] : '';
        $size = isset($params['size']) ? $params['size'] : 5;

        if (!$values) {
            bclog("bcdropdown requires a \"values\" paramater");
            return null;
        }

        // Check for any errors
        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        // Populate $previous from the template $previous array
        if ($previous === null) {

            $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            $previous = ($previous_var === null) ? $default : $previous_var;
        }

        // Create the $poss_values array with correct value/display format
        // $values can be passed in a variety of formats
        $poss_values = array();
        $i = 0;
        if ($value_col && $display_col && is_array($values)) {
            foreach ($values as $key => $value) {
                $poss_values[$i]['value'] = ($value_col == '_key') ? $key : $value[$value_col];
                $poss_values[$i]['display'] = $value[$display_col];
                $i++;
            }
        } elseif (is_array($values)) {
            foreach ($values as $key => $value) {
                if (is_array($value)) {
                    $poss_values[$i] = $value;
                } else {
                    // $poss_values[$i]['value']   = $value;
                    $poss_values[$i]['value'] = ($value_col == '_key') ? $key : $value[$value_col];
                    $poss_values[$i]['display'] = $value;
                }
                $i++;
            }
        } else {
            // Sample: "m:Male,f:Female" or just "Male:Female"
            foreach (explode(',', $values) as $value) {
                $this_value = "";
                $this_display = "";
                if (preg_match("/:/", $value)) {
                    list($this_value, $this_display) = explode(":", $value);
                    if (!$this_display) {
                        $this_display = $my_value;
                    }
                } else {
                    $this_display = $value;
                    $this_value = $value;
                }
                $poss_values[$i]['value'] = $this_value;
                $poss_values[$i]['display'] = $this_display;
                $i++;
            }
        }

        // Add a blank parameter to the front of the list
        if (isset($params['addblank'])) {
            array_unshift($poss_values, '');
            $poss_values[0]['value'] = '';
            $poss_values[0]['display'] = $params['addblank'];
        }

        $html_result = '';

        $select_open = $noselect ? '' : "<select class=\"$class\" id=\"{$name}[]\" name=\"{$name}[]\" multiple=\"multiple\" size=\"$size\" $extra>\n";
        $select_close = $noselect ? '' : "</select>\n";

        switch ($layout) {
            case '2cols':
            case 'row':
                $html_result .= "<td>$title";
                $html_result .= "</td><td>";
                if ($error) {
                    $html_result .= "<span class=\"input_error\">$error</span><br />";
                }
                $html_result .= "$select_open\n";
                foreach ($poss_values as $this_value) {
                    $html_result .= "<option value=\"" . $this_value['value'] . "\"";
                    if (in_array($this_value['value'], $previous)) {
                        $html_result .= ' selected="selected"';
                    }
                    $html_result .= " >" . $this_value['display'] . "</option>\n";
                }
                $html_result .= "$select_close";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $html_result .= "</td>\n";

                if ($layout == 'row') {
                    $html_result = "<tr>$html_result</tr>";
                }
                break;

            case 'plain':
                $html_result .= "$title ";
                if ($error) {
                    $html_result .= "<span class=\"input_error\">$error</span><br />";
                }
                $html_result .= $select_open;
                foreach ($poss_values as $this_value) {
                    $html_result .= "<option value=\"" . $this_value['value'] . "\"";
                    if (in_array($this_value['value'], $previous)) {
                        $html_result .= ' selected="selected"';
                    }
                    $html_result .= " >" . $this_value['display'] . "</option>\n";
                }
                $html_result .= $select_close;
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;

            default:
                $html_result .= "$title ";
                if ($error) {
                    $html_result .= "<br /><span class=\"input_error\">$error</span><br />";
                }
                $html_result .= $select_open;
                foreach ($poss_values as $this_value) {
                    $html_result .= "<option value=\"" . $this_value['value'] . "\"";
                    if (in_array($this_value['value'], $previous)) {
                        $html_result .= " selected=\"selected\"";
                    }
                    $html_result .= " >" . $this_value['display'] . "</option>\n";
                }
                $html_result .= $select_close;
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;
        }
        return $html_result;
    }
}
