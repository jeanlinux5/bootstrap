<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class Select
{
    public static function render($params)
    {
        // Retrieve all of the values passed in
        $display_col = isset($params['display_col']) ? $params['display_col'] : '';
        $value_col = isset($params['value_col']) ? $params['value_col'] : '';
        $title = isset($params['title']) ? $params['title'] : '';
        $name = isset($params['name']) ? $params['name'] : '';
        $layout = isset($params['layout']) ? $params['layout'] : '';
        $previous = isset($params['selected']) ? $params['selected'] : null;
        $extra = isset($params['extra']) ? $params['extra'] : '';
        $noselect = isset($params['noselect']) ? $params['noselect'] : '';
        $default = isset($params['default']) ? $params['default'] : '';
        $values = isset($params['values']) ? $params['values'] : '';
        $class = isset($params['class']) ? $params['class'] : '';

        if (!$values) {
            bclog("select <$name> requires a 'values' paramater");
            return null;
        }

        // Check for any errors
        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        // Populate $previous from the template $previous array
        if ($previous === null) {
            $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            $previous = ($previous_var === null) ? $default : $previous_var;
        }

        // Create the $poss_values array with correct value/display format
        // $values can be passed in a variety of formats
        $poss_values = array();
        $i = 0;
        if ($value_col && $display_col && is_array($values)) {
            foreach ($values as $key => $value) {
                $poss_values[$i]['value'] = ($value_col == '_key') ? $key : $value[$value_col];
                $poss_values[$i]['display'] = ($display_col == '_value') ? $value : $value[$display_col];
                $i++;
            }
        } elseif (is_array($values)) {
            foreach ($values as $key => $value) {
                if (is_array($value)) {
                    $poss_values[$i] = $value;
                } else {
                    $poss_values[$i]['value'] = ($value_col == '_key') ? $key : (is_array($value) ? $value[$value_col] : $value);
                    $poss_values[$i]['display'] = $value;
                }
                $i++;
            }
        } else {
            // Sample: "m:Male,f:Female" or just "Male,Female"
            foreach (explode(',', $values) as $value) {
                $this_value = '';
                $this_display = '';
                if (preg_match('/:/', $value)) {
                    list($this_value, $this_display) = explode(':', $value);
                    if (!$this_display) {
                        $this_display = $my_value;
                    }
                } else {
                    $this_display = $value;
                    $this_value = $value;
                }
                $poss_values[$i]['value'] = $this_value;
                $poss_values[$i]['display'] = $this_display;
                $i++;
            }
        }

        // Add a blank parameter to the front of the list
        if (isset($params['addblank'])) {
            array_unshift($poss_values, '');
            $poss_values[0]['value'] = '';
            $poss_values[0]['display'] = $params['addblank'];
        }

        $html_result = '';
        $selected = false;

        $select_open = $noselect ? "" : "<select class=\"$class\" id=\"$name\" name=\"$name\" $extra>\n";
        $select_close = $noselect ? "" : "</select>\n";

        switch ($layout) {
            case '2cols':
            case 'row':
                $html_result .= "<td>$title";
                $html_result .= '</td><td>';
                if ($error) {
                    $html_result .= "<span class=\"input_error\">$error</span><br />";
                }
                $html_result .= "$select_open\n";
                foreach ($poss_values as $this_value) {
                    $html_result .= "<option value=\"" . $this_value['value'] . "\"";
                    if ($previous == $this_value['value'] && (!$selected)) {
                        $selected = true;
                        $html_result .= ' selected="selected"';
                    }
                    $html_result .= " >" . $this_value['display'] . "</option>\n";
                }
                $html_result .= "$select_close";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $html_result .= "</td>\n";
                if ($layout == 'row') {
                    $html_result = "<tr>$html_result</tr>";
                }
                break;

            case 'plain':
                $html_result .= "$title ";
                if ($error) {
                    $html_result .= "<span class=\"input_error\">$error</span><br />";
                }
                $html_result .= $select_open;
                foreach ($poss_values as $this_value) {
                    $html_result .= "<option value=\"" . $this_value['value'] . "\"";
                    if ($previous == $this_value['value'] && (!$selected)) {
                        $selected = true;
                        $html_result .= ' selected="selected"';
                    }
                    $html_result .= " >" . $this_value['display'] . "</option>\n";
                }
                $html_result .= $select_close;
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;

            default:
                $html_result .= "$title ";
                if ($error) {
                    $html_result .= "<br /><span class=\"input_error\">$error</span><br />";
                }
                $html_result .= $select_open;
                foreach ($poss_values as $this_value) {
                    $html_result .= "<option value=\"" . $this_value['value'] . "\"";
                    if ($previous == $this_value['value'] && (!$selected)) {
                        $selected = true;
                        $html_result .= ' selected="selected"';
                    }
                    $html_result .= " >" . $this_value['display'] . "</option>\n";
                }
                $html_result .= $select_close;
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;
        }
        return $html_result;
    }

    public static function renderDay($params)
    {
        $name = isset($params['name']) ? $params['name'] : "day";
        $params['values'] = array();
        for ($i = 1; $i <= 31; $i++) {
            $day = ($i < 10) ? "0$i" : $i;
            array_push($params['values'], array(
                'value' => $day,
                'display' => $day
            ));
        }
        return self::render($params);
    }

    public static function renderMonth($params)
    {
        if (is_string($params)) {
            $params = array('name' => $params);
        }
        $params['name'] = isset($params['name']) ? $params['name'] : 'month';
        $params['display_col'] = isset($params['display_col']) ? $params['display_col'] : 'long';
        $params['value_col'] = isset($params['value_col']) ? $params['value_col'] : 'num';
        $params['addblank'] = isset($params['addblank']) ? $params['addblank'] : '';

        $params['values'] = array();
        $params['values'][] = array('num' => '01', 'short' => 'Jan', 'long' => 'January');
        $params['values'][] = array('num' => '02', 'short' => 'Feb', 'long' => 'February');
        $params['values'][] = array('num' => '03', 'short' => 'Mar', 'long' => 'March');
        $params['values'][] = array('num' => '04', 'short' => 'Apr', 'long' => 'April');
        $params['values'][] = array('num' => '05', 'short' => 'May', 'long' => 'May');
        $params['values'][] = array('num' => '06', 'short' => 'Jun', 'long' => 'June');
        $params['values'][] = array('num' => '07', 'short' => 'Jul', 'long' => 'July');
        $params['values'][] = array('num' => '08', 'short' => 'Aug', 'long' => 'August');
        $params['values'][] = array('num' => '09', 'short' => 'Sep', 'long' => 'September');
        $params['values'][] = array('num' => '10', 'short' => 'Oct', 'long' => 'October');
        $params['values'][] = array('num' => '11', 'short' => 'Nov', 'long' => 'November');
        $params['values'][] = array('num' => '12', 'short' => 'Dec', 'long' => 'December');

        $i = 0;
        while (isset($params['values'][$i])) {
            //$params['values'][$i]['longest'] = "{$params['values'][$i]['short']} ({$params['values'][$i]['num']})";
            $params['values'][$i]['longest'] = "{$params['values'][$i]['num']} - {$params['values'][$i]['short']}";
            $i++;
        }

        return self::render($params);
    }

    public static function renderState($params)
    {
        $name = isset($params['name']) ? $params['name'] : 'state';
        $params['values'] = getStates();

        return self::render($params);
    }

    public static function renderYear($params)
    {
        if (is_string($params)) {
            $params = array('name' => $params);
        }

        $name = isset($params['name']) ? $params['name'] : 'year';
        $backward = isset($params['backward']) ? $params['backward'] : 0;
        $forward = isset($params['forward']) ? $params['forward'] : 10;
        $direction = isset($params['direction']) ? $params['direction'] : 'asc';

        $poss_values = array();
        $current_year = date('Y');

        // Backward should be negative
        $i = ($backward < 0) ? $backward : 0 - $backward;

        for (; $i <= $forward; $i++) {
            $this_year = $current_year + $i;
            $this_value['value'] = $this_year;
            $this_value['display'] = $this_year;
            array_push($poss_values, $this_value);
        }

        if ($direction != 'asc') {
            $poss_values = array_reverse($poss_values);
        }

        $params['values'] = $poss_values;

        return self::render($params);
    }
}
