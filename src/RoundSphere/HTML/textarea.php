<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class Textarea
{
    public static function render($params)
    {
        ## Retrieve various settings from passed-in arguments
        $title = isset($params['title']) ? $params['title'] : '';
        $name = isset($params['name']) ? $params['name'] : 'textbox';
        $id = isset($params['id']) ? $params['id'] : 'name';
        $type = isset($params['type']) ? $params['type'] : 'text';
        $rows = isset($params['rows']) ? $params['rows'] : 6;
        $cols = isset($params['cols']) ? $params['cols'] : 60;
        $layout = isset($params['layout']) ? $params['layout'] : 'plain';
        $extra = isset($params['extra']) ? $params['extra'] : '';
        $previous = isset($params['previous']) ? $params['previous'] : null;
        $explain = isset($params['explain']) ? $params['explain'] : '';
        $default = isset($params['default']) ? $params['default'] : '';
        $class = isset($params['class']) ? $params['class'] : 'clean';

        ## Add a space to $extra to make formatting consistent
        $extra = "$extra ";

        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        // If $previous wasn't obtained from $params, then look in the input variables for it
        if ($previous === null) {
            $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            $previous = ($previous_var === null) ? $default : $previous_var;
        }
        //$previous = htmlspecialchars($previous);

        $html_result = '';

        // Create the textbox, based on the selected layout

        switch ($layout) {
            case 'row':
            case '2cols':
                $html_result .= "<td valign=\"top\">$title";
                $html_result .= "</td><td valign=\"top\">\n";
                $html_result .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $html_result .= "<textarea class=\"$class\" type=\"$type\" name=\"$name\" id=\"$name\" rows=\"$rows\" cols=\"$cols\" $extra/>" . htmlentities($previous, ENT_QUOTES, 'UTF-8') . "</textarea>\n";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $html_result .= "</td>\n";
                if ($layout == 'row') {
                    $html_result = "<tr>$html_result</tr>";
                }
                break;

            case 'plain':
            default:
                $html_result .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $html_result .= "<textarea class=\"$class\" name=\"$name\" id=\"$name\" rows=\"$rows\" cols=\"$cols\" $extra/>" . htmlentities($previous, ENT_QUOTES) . "</textarea>\n";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;
        }

        return $html_result;
    }
}

