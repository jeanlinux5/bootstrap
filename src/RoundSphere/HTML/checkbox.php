<?php

namespace RoundSphere\HTML;

use RoundSphere\Template;

class Checkbox
{
    public static function render($params)
    {
        // Retrieve various settings from passed-in arguments
        $title = isset($params['title']) ? $params['title'] : '';
        $name = isset($params['name']) ? $params['name'] : 'textbox';
        $id = isset($params['id']) ? $params['id'] : $name;
        $type = isset($params['type']) ? $params['type'] : 'text';
        $size = isset($params['size']) ? $params['size'] : '40';
        $layout = isset($params['layout']) ? $params['layout'] : 'plain';
        $class = isset($params['class']) ? $params['class'] : 'inputarea';
        $extra = isset($params['extra']) ? $params['extra'] : '';
        $previous = isset($params['previous']) ? $params['previous'] : null;
        $explain = isset($params['explain']) ? $params['explain'] : '';
        $default = isset($params['default']) ? $params['default'] : '';

        // Add a space to $extra to make formatting consistent
        $extra = "$extra ";

        global $errors;
        if (isset($errors) && is_object($errors)) {
            $error = $errors->fetch($name);
        } elseif (is_array($errors)) {
            $error = isset($errors[$name]) ? $errors[$name] : '';
        } else {
            $error = '';
        }

        // If $previous wasn't obtained from $params, then look in the input variables for it
        if ($previous === null) {
            if (preg_match('#(.*)\[(.*)\]#', $name, $matches)) {
                $previous_var = isset($GLOBALS['t']->vars['previous'][$matches[1]][$matches[2]]) ? $GLOBALS['t']->vars['previous'][$matches[1]][$matches[2]] : $default;
            } else {
                $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            }
            $previous = ($previous_var === null) ? $default : $previous_var;
        }

        $html_result = '';
        // Create the checkbox, based on the selected layout
        switch ($layout) {
            case 'row':
            case '2cols':
                $html_result .= "<td valign=\"top\">$title";
                $html_result .= "</td><td valign=\"top\">\n";
                $html_result .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $html_result .= "<input class=\"$class\" type=\"checkbox\" name=\"$name\" id=\"$name\" size=\"$size\" ";
                $html_result .= $previous ? "checked=\"checked\"" : '';
                $html_result .= "$extra/> &nbsp; $explain\n";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                $html_result .= "</td>\n";
                if ($layout == "row") {
                    $html_result = "<tr>$html_result</tr>";
                }
                break;

            case 'plain':
            default:
                $html_result .= $title;
                $html_result .= $error ? "<span class=\"input_error\">$error</span><br />" : "";
                $html_result .= "<input class=\"$class\" type=\"checkbox\" name=\"$name\" id=\"$name\"";
                $html_result .= $previous ? "checked=\"checked\"" : '';
                $html_result .= " $extra/> &nbsp; $explain\n";
                $html_result .= (!empty($params['tooltip'])) ? Template::tooltip($params['tooltip']) : '';
                break;

        }

        return $html_result;
    }
}
