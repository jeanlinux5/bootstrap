<?php

namespace RoundSphere\HTML;

class ShowValue
{
    public static function render($params)
    {
        ## Retrieve various settings from passed-in arguments
        $title = isset($params['title']) ? $params['title'] : '';
        $name = isset($params['name']) ? $params['name'] : 'textbox';
        $id = isset($params['id']) ? $params['id'] : $name;
        $type = isset($params['type']) ? $params['type'] : 'hidden';
        $layout = isset($params['layout']) ? $params['layout'] : 'plain';
        $extra = isset($params['extra']) ? $params['extra'] : '';
        $previous = isset($params['previous']) ? $params['previous'] : null;
        $modifier = isset($params['modifier']) ? $params['modifier'] : '';
        $default = isset($params['default']) ? $params['default'] : '';

        ## Add a space to $extra to make formatting consistent
        $extra = "$extra ";

        global $errors;
        $error = (isset($errors) && is_object($errors)) ? $errors->fetch($name) : '';

        // If $previous wasn't obtained from $params, then look in the input variables for it
        if ($previous === null) {
            $previous_var = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
            $previous = ($previous_var === null) ? $default : $previous_var;
        }

        switch ($modifier) {
            case 'nl2br':
                $show = nl2br($previous);
                break;
            default:
                $show = $previous;
                break;
        }

        // Create the textbox, based on the selected layout
        $html_result = '';
        switch ($layout) {
            case "row":
            case "2cols":
                $html_result .= "<td>$title";
                $html_result .= $error ? "<br /><span>error</span>" : "";
                $html_result .= "</td><td>\n";
                $html_result .= "<input type=\"$type\" name=\"$name\" id=\"$name\" value=\"$previous\" $extra/>$show\n";
                $html_result .= "</td>\n";
                $html_result = ($layout == 'row') ? "<tr>$html_result</tr>" : $html_result;
                break;

            case "plain":
            default:
                $html_result .= $error ? "<br /><span error</span>" : "";
                $html_result .= "<input type=\"$type\" name=\"$name\" id=\"$name\" value=\"$previous\" $extra/>$show\n";
                break;

        }

        return $html_result;
    }
}
