<?php

namespace RoundSphere;

class lock
{
    static $memcache;
    protected $name;
    protected $randomValue;
    protected $locked = false;

    public function __construct($name = null)
    {
        $this->setName($name);
        $this->randomValue = rand(0, 1000000).'@'.time();
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function appendValue($string)
    {
        if ($this->locked) {
            throw new Exception("lock->appendValue() invalid after lock->acquire has been called");
        }
        $this->randomValue .= ":{$string}";
    }

    public function getLockValue()
    {
        return $this->randomValue;
    }

    static public function memcache()
    {
        if (!isset(self::$memcache)) {
            self::$memcache = new \Memcached();

            $servers = isset($params['servers']) ? $params['servers'] : properties::getProperty('memcache');

            foreach ($servers as $server) {
                list($host, $port) = explode(':', $server);
                $persist = true;
                self::$memcache->addServer($host, $port, $persist);
            }
        }
        return self::$memcache;
    }

    public function query()
    {
        return self::memcache()->get("LOCK:{$this->name}");
    }

    public function acquire($expire = 60)
    {
        $this->randomValue .= "+{$expire}";
        $this->locked = true;
        self::memcache()->set("LOCKRENEW:{$this->name}", time(), $expire);
        return self::memcache()->add("LOCK:{$this->name}", $this->randomValue, $expire);
    }

    public function renew($expire = 60)
    {
        $current = self::memcache()->get("LOCK:{$this->name}");
        if ($current != $this->randomValue) {
            // Return false if the current value is not held by us
            return false;
        }
        self::memcache()->set("LOCKRENEW:{$this->name}", time(), $expire);
        return self::memcache()->replace("LOCK:{$this->name}", $this->randomValue, $expire);
    }

    public function release()
    {
        return self::memcache()->delete("LOCK:{$this->name}");
    }

    public function lastRenewed()
    {
        return self::memcache()->get("LOCKRENEW:{$this->name}");
    }
}
