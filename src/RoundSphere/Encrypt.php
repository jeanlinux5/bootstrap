<?php

namespace RoundSphere;

class Encrypt
{
    private $gpg;
    static private $config;

    public function __construct()
    {
        $config = self::config();
        $this->gpg = new \gnupg();
        putenv("GNUPGHOME={$config['gnupg_home']}");
        $this->gpg->seterrormode(GNUPG_ERROR_WARNING);
    }

    static private function config()
    {
        if (!isset(self::$config)) {
            self::$config = Properties::getProperty('encrypt');
            if (!self::$config) {
                throw new \Exception('Encryption configuration missing');
            }
        }
        return self::$config;
    }

    public function encrypt($data)
    {
        $config = self::config();
        $this->gpg->addencryptkey($config['gnupg_fingerprint']);
        return $this->gpg->encrypt($data);
    }

    public function decrypt($data, $pass_phrase = '')
    {
        $config = self::config();
        $this->gpg->adddecryptkey($config['gnupg_fingerprint'], $pass_phrase);
        return $this->gpg->decrypt($data);
    }


    static public function encode($string, $db = null)
    {
        $config = self::config();
        $db  = isset($db)  ? $db  : \db::app();

        $enc = $db->getOne("
            SELECT ENCODE( ? , ?)
        ", array ($string, $config['encryption_key']));
        // Strip any trailing equals signs, which are just padding
        return preg_replace('/=+$/', '', base64_encode($enc));
    }

    static public function decode($string, $db = null)
    {
        $config = self::config();
        $db  = isset($db)  ? $db  : \db::app();

        // Add trailing equal signs to make it the right number of chars
        while (strlen($string) % 4 != 0) {
            $string .= '=';
        }
        return $db->getOne("
            SELECT DECODE( ? , ?)
        ", array (base64_decode($string), $config['encryption_key']));
    }

    static public function aes_encode($string, $db = null, $key = null)
    {
        $config = self::config();
        $db  = isset($db)  ? $db  : \db::app();
        $key = isset($key) ? $key : $config['encryption_key'];

        $enc = $db->getOne("
            SELECT AES_ENCRYPT( ? , ?)
        ", array ($string, $key));
        // Strip any trailing equals signs, which are just padding
        return preg_replace('/=+$/', '', base64_encode($enc));
    }

    static public function aes_decode($string, $db = null, $key = null)
    {
        $config = self::config();
        $db  = isset($db)  ? $db  : \db::app();
        $key = isset($key) ? $key : $config['encryption_key'];

        // Add trailing equal signs to make it the right number of chars
        while (strlen($string) % 4 != 0) {
            $string .= '=';
        }
        return $db->getOne("
            SELECT AES_DECRYPT( ? , ?)
        ", array (base64_decode($string), $key));
    }

    /*
        The aes_encode returns base64_encoded info, but the + and / characters
        are not suitable for URLs. This function converts those to _ and - characters
        which will work within a URL
    */
    static public function aes_encode_for_url($string, $db = null, $key = null)
    {
        $encoded = self::aes_encode($string, $db, $key);
        return str_replace(['/', '+'], ['_', '-'], $encoded);
    }

    static public function aes_decode_for_url($string, $db = null, $key = null)
    {
        $encoded = str_replace(['_', '-'], ['/', '+'], $string);
        return self::aes_decode($encoded, $db, $key);
    }
}
