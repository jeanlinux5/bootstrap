<?php

namespace RoundSphere;

class DBMigrate extends Base
{
    private $db_command_line_template;
    protected $sql_file_dir;
    private $verbose;

    private $db_name;
    private $root_db_pass;

    protected $properties;
    private $migrations_run;

    private $dbh;
    private $dbCommand;

    public function __construct($verbose = true)
    {
        $this->verbose = $verbose;
    }

    public function setDbCommandLine($commandLine)
    {
        $this->db_command_line_template = $commandLine;
    }

/*
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        }
    }
    public function __get($name)
    {
        return property_exists($this, $name) ? $this->$name : null;
    }
*/

    public function migrate()
    {
        if (empty($this->properties['db_name'])) {
            throw new Exception ('db_name property is not defined');
        }
        $this->output("Starting migration");
        $this->initDbCommand();
        $this->getMigrationsRun();
        $this->applyMigrations();
        $this->output("Migration complete");
    }

    private function initDbCommand()
    {
        $cmd = $this->db_command_line_template;
        preg_match_all("/{(\w+)}/", $cmd, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $key = $match[1];
            $cmd = str_replace($match[0], $this->properties[$key], $cmd);
        }
        $this->dbCommand = $cmd;
        $this->output("Got dbCommand = {$this->dbCommand}");
    }

    public function databaseSize()
    {
        $rows = $this->dbh()->getAll("
            SHOW TABLE STATUS
        ");
        $dataSize   = 0;
        $indexSize  = 0;
        foreach ($rows as $row) {
            $dataSize   += $row['Data_length'];
            $indexSize  += $row['Index_length'];
        }
        return $indexSize + $dataSize;
    }

    private function getMigrationsRun()
    {
        // check if our "db_migration" table exists
        $tables = $this->dbh()->getCol("SHOW TABLES IN `{$this->properties['db_name']}`");
        if (!in_array("db_migration", $tables)) {
            $this->output("Creating db_migration table");
            $this->dbh()->query("
                CREATE TABLE {$this->properties['db_name']}.db_migration (
                    filename VARCHAR(128) PRIMARY KEY,
                    date_run DATETIME NOT NULL
                )");
        }

        // load migration files we've already run
        $this->migrations_run = $this->dbh()->getCol("
            SELECT  filename
            FROM    {$this->properties['db_name']}.db_migration
        ");
    }

    public function migrationsToRun()
    {
        $this->getMigrationsRun();
        $totalSqlFiles      = 0;
        $migrationsToRun    = array();
        $dir = opendir($this->sql_file_dir);
        while ($file = readdir($dir)) {
            if (substr($file, -4) === ".sql") {
                $totalSqlFiles++;
                if (!in_array($file, $this->migrations_run)) {
                    $migrationsToRun[] = $file;
                }
            }
        }
        closedir($dir);

        // $this->output("  Total SQL files: $totalSqlFiles");
        // $this->output("  SQL files to run: " . count($migrationsToRun));

        sort($migrationsToRun);
        return $migrationsToRun;
    }

    private function applyMigrations()
    {
        $migrationsToRun = $this->migrationsToRun();

        chdir($this->sql_file_dir);
        foreach ($migrationsToRun as $file) {
            $this->applyMigration($file);
        }
    }

    private function applyMigration($sqlFile)
    {
        $this->output("    Applying SQL file: $sqlFile");

        $command = $this->dbCommand . " < " . $sqlFile;
        $output  = array();
        exec($command, $output, $retval);
        if ($retval != 0) {
            print "Failed to execute migration: $command\n";
            print "Return code: $retval\n";
            if (count($output) > 0) {
                print "Output from database command:\n";
                foreach ($output as $line) {
                    print "    $line";
                }
            }
            exit;
        }

        // add file to db
        $this->dbh()->query("
            INSERT INTO {$this->properties['db_name']}.db_migration
            SET     filename    = ?,
                    date_run    = NOW()
        ", array($sqlFile));
    }

    private function output($str)
    {
        if ($this->verbose) {
            echo "{$str}\n";
        }
    }

    private function dbh()
    {
        if (!isset($this->dbh)) {
            $dbConfig = array(
                'host'      => $this->properties['db_host'],
                'port'      => $this->properties['db_port'],
                'user'      => $this->properties['db_user'],
                'pass'      => $this->properties['db_pass'],
                'name'      => $this->properties['db_name'],
            );
            $this->dbh = new Database($dbConfig);
            if (!$this->dbh) {
                throw new Exception("Unable to connect to db");
            }
        }
        return $this->dbh;
    }
}
