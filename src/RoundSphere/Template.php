<?php

namespace RoundSphere;

use RoundSphere\HTML\Html;

class Template
{
    var $headers = array();
    var $onload = array();
    var $breadcrumbs = array();
    var $wrap = 'layout.html';
    var $template_name = '';
    var $requested_page = '';
    var $debugging = false;
    var $page_title = '';
    var $vars = array();
    var $meta = array();
    var $config = array();
    var $canonical = '';
    protected $javascript = '';

    // Class constructor
    function __construct($config = array())
    {
        $this->config = $config;
        // using singular 'message' instead of 'messages' for compatability with old system
        if (!isset($_SESSION['message'])) {
            $_SESSION['message'] = array();
        }
        if (!isset($_SESSION['errors'])) {
            $_SESSION['errors'] = array();
        }
        $this->addBreadcrumb("/", "Home");

        $displayed_config = array(
            'website' => $this->config['website'] . $this->config['relative_url'],
            'secure_site' => isset($this->config['secure_site']) ? $this->config['secure_site'] : '',
            'relative_url' => isset($this->config['relative_url']) ? $this->config['relative_url'] : '',
        );
        $this->requested_page = isset($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : '';
        if (!$this->requested_page) {
            $this->requested_page = isset($_SERVER['REDIRECT_SCRIPT_URL']) ? $_SERVER['REDIRECT_SCRIPT_URL'] : $_SERVER['SCRIPT_NAME'];
        }
        $relative = preg_replace('/\//', '\/', $this->config['relative_url']);
        $this->requested_page = preg_replace("/^$relative/", '', $this->requested_page);

        if (!empty($GLOBALS['pl']->location)) {
            $this->template_name = $GLOBALS['pl']->location;
        }

        $this->assign(array(
            'config' => $displayed_config,
            // If this was called from the .htaccess rewrite rules, set SCRIPT_NAME to be the name of the actual URL
            // that was requested, and not bcframework.php
            'SCRIPT_NAME' => $this->config['relative_url'] . $this->requested_page
        ));

    }

    function assign($arg1, $arg2 = '')
    {
        if (is_array($arg1)) {
            foreach ($arg1 as $name => $value) {
                $this->vars[$name] = $value;
            }
        } elseif ($arg1 && $arg2) {
            $this->vars[$arg1] = $arg2;
        }
    }

    function value($name)
    {
        return isset($this->vars[$name]) ? $this->vars[$name] : '';
    }

    function setTemplate($template)
    {
        $this->template_name = $template;
    }

    function setCanonical($url)
    {
        $this->canonical = $url;
    }

    public function appendJavascript($string)
    {
        $this->javascript .= "{$string}\n\n";
    }

    function wrap($template)
    {
        //global $hooks;
        //$hooks->call('wrap_start');
        // If the template includes a directory, and that directory contains
        // a file named "layout.html", then assign sub_content_file to $template
        // and use the <DIR>/layout.html as $template
        if (preg_match("/(^[a-zA-Z0-9_\/]+)\/(.*)/", $template, $matches)) {
            $dir = $matches[1];
            $file = $matches[0];
            if (file_exists($this->config['template_dir'] . "/" . $this->template_name . "/$dir/layout.html")) {
                $dir_template = "$dir/layout.html";
            } elseif (file_exists($this->config['template_dir'] . "/default/$dir/layout.html")) {
                $dir_template = "$dir/layout.html";
            }
            // Fix for a really ugly recursive case when our wrap is the same as $dir/layout.html
            if (isset($dir_template) && $dir_template != $this->wrap) {
                $template = $dir_template;
                $this->assign("sub_content_file", $file);
            }
            // Otherwise proceed as normal
        }
        $this->assign(array(
            'content_file' => $template,
        ));
        //$hooks->call('wrap_display');
        $this->pageInfo();
        $this->display($this->wrap);
    }


    function pageInfo()
    {
        global $includeDir, $loggedin, $private_label, $hooks;

        if ($this->debugging) {
            $this->assign(array(
                '_SESSION' => $_SESSION
            ));
        }
        $global_title = isset($GLOBALS['title']) ? $GLOBALS['title'] : $this->config['website_name'];
        $this->page_title = ($this->page_title) ? $this->page_title : $global_title;

        // Assign all of this stuff to the template object
        $this->assign(array(
            'todays_date' => date('U'),
            'timestamp' => time(),
            'breadcrumbs' => $this->breadcrumbs,
            'pageTitle' => $this->page_title,
            'onloads' => $this->onload,
            'global_messages' => $_SESSION['message'],
            'global_errors' => $_SESSION['errors'],
            'pl' => isset($private_label) ? $private_label->flatten() : array(),
            'admin' => isset($_SESSION['admin']) ? $_SESSION['admin'] : false,
            'loggedin' => $loggedin ? $loggedin : '',
            'jsdebug' => isset($_SESSION['jsdebug']) ? $_SESSION['jsdebug'] : false,
            'template_name' => $this->template_name,
            'session' => array('session_name' => session_name(), 'session_id' => session_id()),
            'is_secure' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? true : false
        ));
        // Now empty the messages and errors arrays since they have been displayed
        $_SESSION['message'] = array();
        $_SESSION['errors'] = array();
        // Finally, display the page
    }

    function fourohfour($message = '')
    {
        if (!headers_sent()) {
            header("HTTP/1.0 404 File Not Found");
        }

        $this->title = '404 -- File Not Found';
        $this->assign('addl_message', $message);
        $this->wrap('404.html');
        exit;
    }

    function fourohthree($message = '')
    {
        if (!headers_sent()) {
            header('HTTP/1.1 403 Forbidden');
        }

        $this->title = '403 -- Access Denied';
        $this->assign('addl_message', $message);
        $this->wrap('403.html');
        exit;
    }

    // Used to add something  between the <head></head> tags
    function addHtmlHeader($header)
    {
        array_push($this->headers, $header);
    }

    // Add a javascript function to the <body onload=""> parameter
    function addOnload($function)
    {
        array_push($this->onload, $function);
    }

    // Add a message to be displayed to the user at the top of the page
    function addMessage($message)
    {
        foreach ($_SESSION['message'] as $msg) {
            if ($msg == $message) return true;
        }

        array_push($_SESSION['message'], $message);

        return false;
    }

    function addError($error)
    {
        foreach ($_SESSION['errors'] as $err) {
            if ($err == $error) return true;
        }

        array_push($_SESSION['errors'], $error);

        return false;
    }


    function setTitle($title, $prepend_website = false)
    {
        $this->page_title = ($prepend_website) ? $this->config['website_name'] . " - " : '';
        $this->page_title .= $title;
    }

    // Add a breadcrumb
    function addBreadcrumb($link, $display)
    {
        $thiscrumb['link'] = $link;
        // Clean up display a little bit
        $display = preg_replace('/<.*?>/', '', $display);
        $thiscrumb['display'] = $display;
        array_push($this->breadcrumbs, $thiscrumb);
    }

    public function enforceCanonical($desired)
    {
        if ($_SERVER['REQUEST_URI'] != $desired) {
            $this->redirectPerm($desired);
        }
    }

    // Redirect a user to a new location
    function redirect($location = '', $exit = false, $permanent = false)
    {
        if (!empty($_SERVER['HTTPS'])) {
            $this_website = preg_replace("/\/+$/", '', $this->config['secure_website']);
        } else {
            $this_website = preg_replace("/\/+$/", '', $this->config['website']);
        }
        // $this_website = 'http';
        // $this_website .= isset($_SERVER['HTTPS']) ? 's' : '';
        // $this_website .= "://".$_SERVER['HTTP_HOST'];
        // Removing trailing slash from $this->config['relative_url']
        $rel_url = preg_replace("/\/+$/", '', $this->config['relative_url']);
        // bclog("redirect called with location = $location");
        if (!preg_match("/^http/", $location)) {
            if (preg_match("/^\?/", $location)) {
                $location = $this_website . $rel_url . "/" . $this->requested_page . $location;
                // If $location begins with a slash, then don't insert the relative url
//            } elseif( preg_match("/^\//", $location) ) {
//                $location = $this_website.$location;
            } elseif ($location) {
                $location = preg_match("/^\//", $location) ? $location : "/$location";
                $location = $this_website . $rel_url . $location;
            } else {
                $location = $this_website . $rel_url . "/" . $this->requested_page;
            }
        }
        // bclog("redirecting to $location");
        if ($permanent) {
            header('HTTP/1.1 301 Moved Permanently');
        }
        header("Location: $location");
        if ($exit) {
            exit;
        }
    }

    function redirectPerm($location, $exit = false)
    {
        $this->redirect($location, $exit, true);
    }

    // Display an error message using "error.html"
    function displayError($message, $exit = false)
    {
        $this->assign('error_message', $message);
        $this->wrap('error.html');
        if ($exit) {
            exit;
        }
    }

    // Display a message and the login prompt
    function promptLogin($message = "You must be logged in", $return_url = '', $exit = false)
    {
        $this->addMessage($message);
        $ref = $return_url ? $return_url : $_SERVER['REQUEST_URI'];
        // urlencode the $ref so that the parameters get passed correctly
        $this->redirect("login.php?ref=" . urlencode($ref));
        if ($exit) {
            exit;
        }
    }

    function realTemplate($template)
    {
        // $start =$template;
        if (empty($this->config['skinable'])) {
            return $template;
        }
        // $template that begin with static/ or dynamic/ don't need looked up
        if (!preg_match("#^(static|dynamic)/#", $template)) {
            if (file_exists($this->config['template_dir'] . "/" . $this->template_name . "/$template")) {
                $template = $this->template_name . "/$template";
            } else {
                $template = "default/$template";
            }
        }
        return $template;
    }

    function fetch($template)
    {
        global $CONFIG, $t, $errors, $loggedin, $private_label, $pl, $seo, $cbbookbag, $pldata;
        // Assign all of the vars so that they can be used easily
        foreach ($this->vars as $name => $value) {
            // Don't override any currently set vars (ie: $this->config)
            if (!isset($$name)) {
                $$name = $value;
            } else {
                // bclog("No setting $name to $value because $name already exists");
            }
        }
        $real_template = $this->realTemplate($template);
        if (file_exists("{$this->config['template_dir']}/$real_template")) {
            ob_start();
            include("{$this->config['template_dir']}/$real_template");
            $output = ob_get_contents();
            ob_clean();
            return $output;
        } else {
            bclog("Template file {$this->config['template_dir']}/$real_template doesn't exist");
        }

    }


    function display($template)
    {
        echo $this->fetch($template);
    }

    function populateSeo()
    {
        global $seo;
        if (!empty($seo->phrases['title'])) {
            $this->page_title = $seo->phrases['title'];
        }
        if (empty($this->page_title)) {
            $this->page_title = $GLOBALS['pldata']['default_title'];
        }
        if (!empty($seo->phrases['meta_description'])) {
            $this->meta['description'] = $seo->phrases['meta_description'];
        }
        if (!empty($seo->phrases['meta_keywords'])) {
            $this->meta['keywords'] = $seo->phrases['meta_keywords'];
        }

    }


    /*
        HTML Elements
    */

    static public function tooltip($tip)
    {
        return '<a href="#" class="tooltip" title="' . htmlentities($tip) . '"><img src="/images/help-icon.png" /></a>';
    }

    function element($name, $params = array())
    {
        if (is_string($params)) {
            $params = array('name' => $params);
        }
        return Html::display($name, $params);
    }

    // Magical function to call anything undefined as an element
    /* This isn't passing the arguments properly yet
        function __call($function, $args)
        {
            return $this->element($function, $args);
        }
    */


    // TODO: I know there is some way to implement a generic catch-all type function (it is used in the soap classes)
    function input($params = array())
    {
        return $this->element('input', $params);
    }

    function password($params = array())
    {
        return $this->element('password', $params);
    }

    function select($params = array())
    {
        return $this->element('select', $params);
    }

    function select_month($params = array())
    {
        return $this->element('select_month', $params);
    }

    function select_year($params = array())
    {
        return $this->element('select_year', $params);
    }

    function select_day($params = array())
    {
        return $this->element('select_day', $params);
    }

    function selectmultiple($params = array())
    {
        return $this->element('selectmultiple', $params);
    }

    function radio($params = array())
    {
        return $this->element('radio', $params);
    }

    function textarea($params = array())
    {
        return $this->element('textarea', $params);
    }

    function checkbox($params = array())
    {
        return $this->element('checkbox', $params);
    }

    function sameurl($params = array())
    {
        return $this->element('sameurl', $params);
    }

    function sorturl($params = array())
    {
        return $this->element('sorturl', $params);
    }

    function showvalue($params = array())
    {
        return $this->element('showvalue', $params);
    }

    function showonly($params = array())
    {
        return $this->element('showonly', $params);
    }

    function date($params = array())
    {
        return $this->element('date', $params);
    }

    function cycle($params = array())
    {
        return $this->element('cycle', $params);
    }

    function tinymce($params = array())
    {
        return $this->element('tinymce', $params);
    }

    function calendar($params = array())
    {
        return $this->element('calendar', $params);
    }

    function openxad($params = array())
    {
        return $this->element('openxad', $params);
    }

    function hidden($params = array())
    {
        if (is_string($params)) {
            $params = array('name' => $params);
        }
        $params['type'] = 'hidden';
        return $this->element('input', $params);
    }

    function hidden_repost()
    {
        $html = '';
        foreach (array_merge($_POST, $_GET) as $name => $value) {
            if (is_string($value) || is_numeric($value)) {
                $html .= "<input type='hidden' name='" . htmlentities($name, ENT_QUOTES) . "' value='" . htmlentities($value, ENT_QUOTES) . "' />\n";
            } elseif (is_array($value)) {
                foreach ($value as $subname => $subvalue) {
                    $html .= "<input type='hidden' name='" . htmlentities($name, ENT_QUOTES) . "[" . htmlentities($subname, ENT_QUOTES) . "]' value='" . htmlentities($subvalue, ENT_QUOTES) . "' />\n";

                }
            }
        }
        return $html;
    }

    function pagination($page, $pages, $method = 'GET')
    {
        static $pagination_counter = 1;
        //$this->element('pagination', array($page, $pages));
        $html = '';
        $html .= "<form name=\"pagination_{$pagination_counter}\" id=\"pagination_{$pagination_counter}\" action=\"{$_SERVER['REQUEST_URI']}\" method=\"{$method}\">\n";
        $html .= $this->hidden_repost();

        if ($page > 1) {
            $html .= $this->sameUrl(array(
                'page'      => ($page - 1),
                'showlink'  => '&lt;&lt; Previous'
            ));
        } else {
            $html .= " &lt;&lt; Previous ";
        }

        $html .= ' | Page ';
        $html .= $this->select(array(
            'name'      => 'page',
            'id'        => "pagination-select-{$pagination_counter}",
            'values'    => range(1, $pages),
            'default'   => $page,
            'extra'     => "class=\"pagination\" onchange=\"document.getElementById('pagination_{$pagination_counter}').submit()\"",
        ));
        $html .= " of {$pages} | ";

        if ($page < $pages) {
            $html .= $this->sameUrl(array(
                'page'      => ($page + 1),
                'showlink'  => 'Next &gt;&gt;'
            ));
        } else {
            $html .= " Next &gt;&gt;";
        }
        $html .= '</form>';
        $pagination_counter++;
        return $html;
    }

    //
    // used to generate a masked input with a shadow hidden field
    // added for Booksupply 2.1 to show cc numbers on the settings page
    //
    function input_obfuscate($params)
    {
        $value = isset($params['previous']) ? $params['previous'] : null;
        if (!$value) {
            $name = $params['name'];
            $value = isset($GLOBALS['t']->vars['previous'][$name]) ? $GLOBALS['t']->vars['previous'][$name] : requestValue($name, null);
        }

        if ($value) {
            $obfuscate_char = isset($params['obfuscate_char']) ? $params['obfuscate_char'] : '*';
            $chars_to_show = isset($params['chars_to_show']) ? $params['chars_to_show'] : 4;
            $len = strlen($value) - $chars_to_show;
            $newval = '';
            $i = 0;
            for (; $i < $len; $i++) {
                $newval .= $obfuscate_char;
            }
            for (; $i < $chars_to_show + $len; $i++) {
                $newval .= $value[$i];
            }

            $params['previous'] = $newval;
        }

        $retval = $this->input($params);
        $params['name'] = $params['name'] . "_hidden";
        $retval .= $this->hidden($params);
        return $retval;
    }

}



