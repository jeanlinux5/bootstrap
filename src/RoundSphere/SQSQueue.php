<?php

namespace RoundSphere;

use Aws;
use Predis;
use Predis\Connection\Aggregate\RedisCluster;
use Predis\Connection\Factory;
use Predis\Cluster\RedisStrategy;

class SQSQueue
{
    static protected $client;
    static protected $redis;

    static protected function aws()
    {
        if (!isset(self::$client)) {
            $awsConfig = Properties::getProperty('aws');
            self::$client = \Aws\Sqs\SqsClient::factory($awsConfig);
        }
        return self::$client;

    }

    static public function redis()
    {
        if (!isset(self::$redis)) {

            $redisConfig = Properties::getProperty('redis');
            if ($redisConfig) {
                self::$redis = new Predis\Client($redisConfig);
                return self::$redis;
            }

            $redisClusterConfig = Properties::getProperty('redisCluster');
            if ($redisClusterConfig) {

                $options = array(
                    'cluster' => function () {
                        $cluster = new Predis\Connection\Aggregate\RedisCluster(new Factory());
                        return $cluster;
                    },
                );

                $multipleServers = [
                    "{$redisClusterConfig['host']}:{$redisClusterConfig['port']}",
                ];

                self::$redis = new Predis\Client($multipleServers, $options);
                return self::$redis;
            }
            throw new \Exception("no redis config defined");
        }
        return self::$redis;
    }

    static public function queueUrl($queueName, $attributes = array() )
    {
        $env = Properties::getProperty('environment');
        if ($result = self::redis()->get("SQSQueue:{$queueName}:{$env}")) {
            return $result;
        }

        $queueUrl = Database::master()->getOne("
            SELECT  queueUrl
            FROM    SQSQueues
            WHERE   queueName = ?
                AND environment = ?
        ", array($queueName, $env));
        if ($queueUrl) {
            self::redis()->set("SQSQueue:{$queueName}:{$env}", $queueUrl);
            self::redis()->expire("SQSQueue:{$queueName}:{$env}", 3600);
            return $queueUrl;
        }

        // SQS Queue doesn't exist in our database
        $queuePrefix = Properties::getProperty('sqsQueuePrefix', 'RS');
        $SQSQueueName = "{$queuePrefix}_{$env}_{$queueName}";
        $result = self::aws()->createQueue(array(
            'QueueName'     => $SQSQueueName,
            'Attributes'    => array(),
        ));
        $queueUrl = $result->get('QueueUrl');
        Database::db()->query("
            INSERT IGNORE INTO SQSQueues
            SET     queueName   = ?,
                    environment = ?,
                    queueURl    = ?,
                    created     = NOW()
        ", array($queueName, $env, $queueUrl));
        return $queueUrl;
    }

    static public function sendMessage($queueName, $params, $uniqueId = null, $expire = 10800)
    {
        $queueUrl = self::queueUrl($queueName);
        if (!$queueUrl) {
            throw new Exception("Unable to determine QueueURL for queueName {$queueName}");
        }

        if (!isset($params['started'])) {
            $params['started'] = time();
        }
        $params['_created'] = time();
        if ($uniqueId) {
            $env = Properties::getProperty('environment');
            $lock = new Redlock("SQSJob:{$env}:{$uniqueId}");

            if (!$lock->acquire($expire)) {
                return true;
            }

            $params['_uniqueId'] = $uniqueId;
        }
        $messageBody = utf8_encode(json_encode($params));
        if (!$messageBody) {
            return false;
        }

        $message = self::aws()->sendMessage(array(
            'QueueUrl'      => $queueUrl,
            'MessageBody'   => $messageBody,
        ));

        return $message;
    }

    static public function sendMessageBatch($queueName, $messages, $expire = 10800)
    {
        $queueUrl = self::queueUrl($queueName);
        if (!$queueUrl) {
            throw new Exception("Unable to determine queueURL for queueName {$queueName}");
        }

        $batch = [];

        foreach ($messages as $i => $params) {

            $includeThisMessage = true;

            if (!isset($params['started'])) {
                $params['started'] = time();
            }
            $params['_created'] = time();
            if (!empty($params['_uniqueId'])) {
                // If a _uniqueId is provided, make sure that it isn't locked in redis
                $env = Properties::getProperty('environment');
                $lock = new Redlock("SQSJob:{$env}:{$params['_uniqueId']}");

                if (!$lock->acquire($expire)) {
                    // Message is already in queue, so don't add again
                    $includeThisMessage = false;
                }
            }

            if ($includeThisMessage) {
                $batch[] = [
                    'Id'            => $i,
                    'MessageBody'   => utf8_encode(json_encode($params)),
                ];
            }
        }

        if (!$batch) {
            return true;
        }

        $message = self::aws()->sendMessageBatch(array(
            'QueueUrl'  => $queueUrl,
            'Entries'   => $batch,
        ));

        return $message;
    }

    static public function startJob($uniqueId)
    {
        // Do Nothing
    }

    static public function completeJob($uniqueId)
    {
        $env = Properties::getProperty('environment');
        $lock = new Redlock("SQSJob:{$env}:{$uniqueId}");
        $lock->release();
    }

    static public function receiveMessage($queue, $longPollTime = 0)
    {
        $queueUrl = self::queueUrl($queue);
        return self::aws()->receiveMessage(array(
            'QueueUrl'          => $queueUrl,
            'WaitTimeSeconds'   => $longPollTime,
        ));
    }

    static public function delayMessage($queue, $receiptHandle, $delay)
    {
        return self::changeMessageVisibility($queue, $receiptHandle, $delay);
    }

    static public function deleteMessage($queue, $receiptHandle)
    {
        $queueUrl = self::queueUrl($queue);
        $rv =  self::aws()->deleteMessage(array(
            'QueueUrl'      => self::queueUrl($queue),
            'ReceiptHandle' => $receiptHandle,
        ));
    }

    static public function changeMessageVisibility($queue, $receiptHandle, $seconds)
    {
        $queueUrl = self::queueUrl($queue);
        $rv =  self::aws()->changeMessageVisibility(array(
            'QueueUrl'          => self::queueUrl($queue),
            'ReceiptHandle'     => $receiptHandle,
            'VisibilityTimeout' => $seconds,
        ));
    }
}
