<?php

namespace RoundSphere;

class Timer
{
    private $start;
    private $times;
    private $logfile;
    private $pad    = '';

    public function __construct($msg)
    {
        $this->start = microtime(true);
        $msg = date('Y-m-d H:i:s')." {$msg}";
        $this->tick($msg);
        $this->pad = '  ';
    }

    public function __destruct()
    {
        if ($this->logfile) {
            $fh = @fopen($this->logfile, 'a');
            if ($fh) {
                fwrite($fh, $this->asString());
                fclose($fh);
            }
        }
    }

    public function asString()
    {
        $str = '';
        foreach ($this->times as $time) {
            $str .= sprintf("[%2.2f] %s\n", $time['elapsed'], $time['message']);
        }
        return $str;
    }

    public function logTo($filename)
    {
        $this->logfile = $filename;
    }

    public function tick($message)
    {
        $elapsed = microtime(true) - $this->start;
        $this->times[] = array(
            'elapsed'   => $elapsed,
            'message'   => $this->pad.$message,
        );
    }
}

