<?php
/**
 * Created by PhpStorm.
 * User: delta6
 * Date: 7/11/14
 * Time: 6:09 PM
 */

namespace RoundSphere;


class HttpBotForm
{
    var $method = 'POST';
    var $action = '';
    var $fields = array();
    var $html = '';
    var $dom_obj = '';
    var $page = '';
    var $url = '';
    var $form = null;

    function __construct(&$page, $name)
    {
        require_once __DIR__.'/../../3rdparty/simple_html_dom.php';

        if (is_object($page) && get_class($page) == 'RoundSphere\HttpBotPage') {
            $this->page =& $page;
            $this->url = $page->url;
            $content = $page->content;
        } elseif (is_string($page) && preg_match('#https?://[^ ]+$#i', $page)) {
            $this->url = $page;
            $content = file_get_contents($page);
        } else {
            $content = $page;
        }

        // Use the cool html_dom class
        $html = str_get_html($content);

        $forms = $html->find('form');
        $theform = false;
        if (is_numeric($name)) {
            $theform = isset($forms[$name]) ? $forms[$name] : null;
        } else {
            // Must be a string. Look for a matching form name, id or action
            $theform = null;
            foreach ($forms as $num => $form) {
                if (isset($form->name) && $form->name == $name) {
                    $theform = $form;
                    break;
                } elseif (isset($form->id) && $form->id == $name) {
                    $theform = $form;
                    break;
                } elseif (isset($form->action) && $form->action == $name) {
                    $theform = $form;
                    break;
                }
            }
        }
        if (!$theform) {
            // $this->page->bot->addLog("unable to find a form with name = $name");
            return false;
        }

        $this->method = !empty($theform->method) ? $theform->method : 'GET';
        $this->action = !empty($theform->action) ? html_entity_decode($theform->action) : $this->url;

        // If we have a $page object available, make the action the full url
        if ($this->page) {
            $this->action = $this->page->fullUrl($this->action);
        }

        $this->dom_obj = (string)$theform;

        foreach ($theform->find('input') as $input) {
            switch (strtolower($input->type)) {
                // Radio buttons should only have the 'checked' value value set
                case 'radio':
                    if ((!empty($input->name)) && $input->checked) {
                        $this->fields[$input->name] = $input->value;
                    } elseif ((!empty($input->name)) && (!isset($this->fields[$input->name]))) {
                        $this->fields[$input->name] = '';
                    }
                    break;

                case 'image':
                    //do nothing because when there are many of this in form, when try to submit the form does not work
                    break;

                default:
                    if (!empty($input->name)) {
                        if (preg_match('#ape:(.*)#', $input->value, $matches)) {
                            // This ape: stuff is something that Amazon does. Not sure if it
                            // is propriatry to them or not
                            $this->fields[$input->name] = base64_decode(trim($matches[1]));
                        } else {
                            $this->fields[$input->name] = html_entity_decode($input->value);
                        }
                    }
                    break;
            }
        }
        foreach ($theform->find('textarea') as $input) {
            if (!empty($input->name)) {
                $this->fields[$input->name] = $input->value;
            }
        }
        foreach ($theform->find('select') as $select) {
            if (!empty($select->name)) {
                $this->fields[$select->name] = '';
                foreach ($select->find('option') as $option) {
                    if ($option->selected) {
                        $this->fields[$select->name] = $option->value;
                        break;
                    }

                }
            }
        }

        $this->form = $theform;

        // Todo: add <select> and <radio> and other types here as needed

    }

    function setField($name, $value)
    {
        $this->fields[$name] = $value;
    }


    function submit(&$bot)
    {
        $bot->addLog("Submitting form via {$this->method} to {$this->action}");
        $bot->addLog(print_r($this->fields, true));
        return $bot->post($this->action, $this->fields, $this->method);
    }

    function submitAndDownload(&$bot, $output_file)
    {
        $bot->addLog("Submitting form via {$this->method} to {$this->action}");
        $bot->addLog(print_r($this->fields, true));
        return $bot->post($this->action, $this->fields, $this->method, false, $output_file);
    }

    function DebugVar()
    {
        $copy = $this;
        // I don't want the whole $this->page object
        $copy->page = '';
        DebugVar($copy);
        unset($copy);
    }

    function removeField($name)
    {
        unset($this->fields[$name]);
    }

    function addSubmitImage($name = null)
    {
        if ($this->form) {
            if (is_null($name) == false) {
                // If a name was specified, find all of the input fields
                foreach ($this->form->find('input') as $input) {
                    // where the type is image and the name matches the specified name
                    if (strtolower($input->type) == 'image' && (isset($input->name)) && ($input->name == $name)) {
                        $name_x = 'x';
                        $name_y = 'y';
                        if (!empty($input->name)) {
                            $name_x = "$input->name.x";
                            $name_y = "$input->name.y";
                            if (!empty($input->value)) {
                                $this->fields[$input->name] = $input->value;
                            }
                        }
                        $width = !empty($input->width) ? $input->width : 15;
                        $height = !empty($input->height) ? $input->height : 15;
                        $this->fields[$name_x] = rand(1, $width);
                        $this->fields[$name_y] = rand(1, $height);
                        break;
                    }
                }
            } else {
                // Else if no name was specified, just use 'x' and 'y' as the input variables
                foreach ($this->form->find('input') as $input) {
                    if (strtolower($input->type) == 'image') {
                        $width = !empty($input->width) ? $input->width : 15;
                        $height = !empty($input->height) ? $input->height : 15;
                        $this->fields['x'] = rand(1, $width);
                        $this->fields['y'] = rand(1, $height);
                        break;
                    }
                }
                if (empty($this->fields['x'])) {
                    $this->fields['x'] = rand(1, 15);
                }
                if (empty($this->fields['y'])) {
                    $this->fields['y'] = rand(1, 15);
                }
            }
        }
    }
} 
