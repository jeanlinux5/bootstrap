CREATE TABLE `SQSQueues` (
  `queueName` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `environment` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `queueUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`queueName`,`environment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
