<?php

namespace RoundSphere;

class Lock
{
    static $memcache;
    protected $name;
    protected $randomValue;

    public function __construct($name = null)
    {
        $this->setName($name);
        $this->randomValue = rand(0, 1000000);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    static public function memcache()
    {
        if (!isset(self::$memcache)) {
            self::$memcache = new \Memcached();

            $servers = Properties::getProperty('memcache');
            if (isset($params['servers'])) {
                $servers = $params['servers'];
            }

            foreach ($servers as $server) {
                list($host, $port) = explode(':', $server);
                $persist = true;
                self::$memcache->addServer($host, $port, $persist);
            }
        }
        return self::$memcache;
    }

    public function acquire($expire = 60)
    {
        return self::memcache()->add("LOCK:{$this->name}", $this->randomValue, $expire);
    }

    public function renew($expire = 60)
    {
        $current = self::memcache()->get("LOCK:{$this->name}");
        if ($current != $this->randomValue) {
            // Return false if the current value is not held by us
            return false;
        }
        return self::memcache()->replace("LOCK:{$this->name}", $this->randomValue, $expire);
    }

    public function release()
    {
        return self::memcache()->delete("LOCK:{$this->name}");
    }
}

