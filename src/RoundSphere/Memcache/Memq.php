<?php

namespace RoundSphere\Memcache;

use RoundSphere\Properties;

define('MEMQ_TTL', 3600);

/*
 *See http://abhinavsingh.com/blog/2010/02/memq-fast-queue-implementation-using-memcached-and-php-only/
 */
class memq
{
    private static $mem = NULL;

    private function __construct() {}

    private function __clone() {}

    private static function getInstance()
    {
        if (!self::$mem) self::init();
        return self::$mem;
    }

    private static function init()
    {
        $mem = new \Memcached;
        if (Properties::getProperty('memcache_dynamic_client')) {
            // Intended for use with Amazon Elasticache Dynamic Client mode
            // http://docs.aws.amazon.com/AmazonElastiCache/latest/UserGuide/AutoDiscovery.html
            $mem->setOption(\Memcached::OPT_CLIENT_MODE, \Memcached::DYNAMIC_CLIENT_MODE);
        }
        foreach (Properties::getProperty('memcache') as $server) {
            list($host, $port) = explode(":", $server);
            $mem->addServer($host, $port);
        }
        self::$mem = $mem;
    }

    static public function data($queue)
    {
        $mem = self::getInstance();
        return array(
            'head'  => $mem->get($queue."_head"),
            'tail'  => $mem->get($queue."_tail"),
        );
    }

    public static function is_empty($queue)
    {
        $mem  = self::getInstance();
        $head = $mem->get($queue."_head");
        $tail = $mem->get($queue."_tail");

        if ($head >= $tail || $head === FALSE || $tail === FALSE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function size($queue)
    {
        $mem  = self::getInstance();
        $head = $mem->get($queue."_head");
        $tail = $mem->get($queue."_tail");
        return $tail - $head;
    }

    public static function dequeue($queue, $after_id = FALSE, $till_id = FALSE)
    {
        $mem = self::getInstance();

        if ($after_id === FALSE && $till_id === FALSE) {
            $tail = $mem->get($queue."_tail");
            if (($id = $mem->increment($queue."_head")) === FALSE)
                return FALSE;

            if ($id <= $tail) {
                return $mem->get($queue."_".($id));
            } else {
                $mem->decrement($queue."_head");
                return FALSE;
            }
        } else if($after_id !== FALSE && $till_id === FALSE) {
            $till_id = $mem->get($queue."_tail");
        }

        $item_keys = array();
        for($i=$after_id+1; $i<=$till_id; $i++)
            $item_keys[] = $queue."_".$i;
        $null = NULL;

        return $mem->getMulti($item_keys, $null, \Memcached::GET_PRESERVE_ORDER);
    }

    public static function enqueue($queue, $item)
    {
        $mem = self::getInstance();

        // Increment the tail by one
        $id = $mem->increment($queue."_tail");

        // we want the _head and _tail items to never expire
        $add_ttl = 0;

        if ($id === FALSE) {
            // tail did not exist.  Create it first.
            if ($mem->add($queue."_tail", 1, $add_ttl) === FALSE) {
                // Adding it failed (Somebody created it first?). Now try to incremtn
                $id = $mem->increment($queue."_tail");
                if ($id === FALSE) {
                    // wow, this shouldn't happen
                    return FALSE;
                }
            } else {
                $id = 1;
                $mem->add($queue."_head", $id, $add_ttl);
            }
        }

        // echo "Setting {$queue}_{$id} to {$item}<br />\n";
        // BC - Changed this from add() to set()
        if($mem->set($queue."_".$id, $item, MEMQ_TTL) === FALSE)
            return FALSE;

        return $id;
    }
}
