<?php
/**
 * Created by PhpStorm.
 * User: delta6
 * Date: 7/11/14
 * Time: 6:07 PM
 */

namespace RoundSphere;

class HttpBotPage
{
    var $headers = array();
    var $content = '';
    var $http_code = '';
    var $url = '';
    var $bot = '';

    var $urlparts = array();

    function __construct($bot, $http_response, $url)
    {
        $this->bot =& $bot;
        $this->url = $url;
        $this->setUrlParts();

        if (preg_match('#(.*?)\n\r?\n(.*)#s', $http_response, $matches)) {
            $lines = explode("\n", $matches[1]);
            $this->content = $matches[2];
        } else {
            $lines = explode("\n", $http_response);
        }

        $end_of_headers = false;

        // Strip a HTTP 100 Continue response if it exists
        if (preg_match("#^HTTP/1.1 100 Continue#", $lines[0])) {
            array_shift($lines);
            array_shift($lines);

            // After removing first two lines, re-look for headers and content
            $http_response = $this->content;
            $this->content = '';

            if (preg_match('#(.*?)\n\r?\n(.*)#s', $http_response, $matches)) {
                $lines = explode("\n", $matches[1]);
                $this->content = $matches[2];
            } else {
                $lines = explode("\n", $http_response);
            }
        }

        $i = 0;

        foreach ($lines as $line) {
            $line = trim($line);
            if ((!$end_of_headers) && $line) {
                if (preg_match("#^HTTP\/1\.[01] ([0-9]+) #", $line, $matches)) {
                    $this->http_code = $matches[1];
                } else {
                    if (strpos($line, ': ') !== false) {
                        list($name, $value) = explode(': ', $line, 2);
                        $this->headers[strtolower($name)] = $value;
                    } elseif (strpos($line, ':') !== false) {
                        // This never gets hit?!?!
                        list($name, $value) = explode(':', $line, 2);
                        $this->headers[strtolower($name)] = $value;
                    }
                }
            } else {
                if ($end_of_headers) {
                    $this->content .= "$line\n";
                }
            }
            if (!$line && $this->http_code) {
                $end_of_headers = true;
            }
            $i++;
        }
        if (isset($this->headers['content-type']) && $this->http_code == 200 && preg_match('#text/html#', $this->headers['content-type'])) {
            $this->bot->referer = $this->url;
        }

        // Uncompress gzipped content
        if (isset($this->headers['content-encoding']) && 'gzip' == $this->headers['content-encoding']) {
            $this->content = gzdecode($this->content);
        }

        // Adding redirect when a meta tag is found
        if (!(($this->http_code == 302 || $this->http_code == 301) && (!empty($this->headers['location'])))) {
            require_once dirname(__FILE__) . '/../../3rdparty/simple_html_dom.php';

            // html_dom class
            $html = str_get_html($http_response);
            foreach ($html->find('meta') as $meta) {
                if (!empty($meta->attr['http-equiv']) && strtolower($meta->attr['http-equiv']) == 'refresh' && !empty($meta->content)) {
                    $this->bot->addLog("Found meta refresh redirect to {$meta->content}");
                    $pos = strpos(strtolower($meta->content), 'url=');
                    if ($pos !== false) {
                        $redirect = substr($meta->content, $pos + 4, strlen($meta->content));
                        $this->bot->addLog("  redirecting to $redirect");
                        // Fake it into redirecting by setting the http_code and location header
                        $this->http_code = 302;
                        $this->headers['location'] = $redirect;
                        break;
                    }
                }
            }
            $html->clear();
            unset($html);
        }
    }

    // Parse out all of the parts of a URL that we might be intrested in
    function setUrlParts()
    {
        if ($parts = parse_url($this->url)) {
            $this->urlparts['scheme'] = isset($parts['scheme']) ? $parts['scheme'] : '';
            $this->urlparts['host'] = isset($parts['host']) ? $parts['host'] : '';
            $this->urlparts['path'] = isset($parts['path']) ? $parts['path'] : '';
            $this->urlparts['query'] = isset($parts['query']) ? $parts['query'] : '';
            //$this->urlparts['fragment'] = $parts['fragment'];

            // The directory contains the initial slash up to the slash before the file name
            /*if ($this->urlparts['path'] && preg_match('#^/([^/]+)/.*#', $this->urlparts['path'], $matches)) {
                $this->urlparts['directory'] = '/'.$matches[1].'/';
                $this->urlparts['file'] = substr($this->urlparts['path'], strlen($this->urlparts['directory']));*/
            if ($this->urlparts['path'] && preg_match_all('#(/([^/]+))#', $this->urlparts['path'], $matches)) {
                $matches = $matches[2];
                $this->urlparts['file'] = array_pop($matches);
                $this->urlparts['directory'] = '';
                foreach ($matches as $sub_directory) {
                    $this->urlparts['directory'] .= "/$sub_directory";
                }
                $this->urlparts['directory'] .= '/';
            } else {
                $this->urlparts['directory'] = '/';
                $this->urlparts['file'] = substr($this->urlparts['path'], 1);
            }

            $this->urlparts['parameters'] = array();
            if ($this->urlparts['query']) {
                $parts = explode('&', $this->urlparts['query']);
                foreach ($parts as $part) {
                    if (preg_match('#(.*)=(.*)#', $part, $matches)) {
                        $this->urlparts['parameters'][$matches[1]] = $matches[2];
                    } else {
                        $this->urlparts['parameters'][$part] = '';
                    }
                }
            }
        } else {
            $this->bot->addLog("Unable to parse url: {$this->url}");
        }

    }

    function emulateBrowser()
    {
        $this->fetchElements();
        // Sleep for a short period of time (as a user would do)
        $sleep = rand(2, 7);
        $this->bot->addLog("sleeping for $sleep seconds");
        sleep($sleep);
    }

    function pageElements()
    {
        $content = preg_replace('#<script.*?</script>#is', '', $this->content);
        preg_match_all("#src=[\"']+([^\"' ]+)#", $content, $matches);
        preg_match_all("#link href=[\"']?([^\"' ]+)#", $content, $link_matches);
        $elements = array_merge($link_matches[1], $matches[1]);

        $i = 0;
        while (isset($elements[$i])) {
            if (preg_match('#[\<\>\{\}\(\)]#', $elements[$i])) {
                unset($elements[$i]);
            } elseif (preg_match('#images-na.ssl-images-amazon.com#', $elements[$i])) {
                unset($elements[$i]);
            }
#            if (preg_match("/googlesyndication.com|google-analytics.com/", $elements[$i])) {
#                unset($elements[$i]);
#            } else {
#            }
            if (isset($elements[$i])) {
                $elements[$i] = $this->makeAbsolute(html_entity_decode($elements[$i]));
            }

            $i++;
        }
        return array_unique($elements);
    }

    function fetchElements()
    {
        $elements = $this->pageElements();
        foreach ($elements as $element) {
            if (preg_match('#pixel.fetchback.com#', $element)) {
                continue;
            }
            $this->bot->get($element, false);

        }
    }

    function links()
    {
        preg_match_all("#<a [^>]*href=[\"']?([^\"' ]+)#", $this->content, $matches);

        $links = array();
        foreach ($matches[1] as $location) {
            $links[] = $this->makeAbsolute($location);
        }
        return array_unique($links);
    }

    function makeAbsolute($location)
    {
        // A strange case like src="//www.shopping.com/merchant_logo.jpg   that browsers actually understand
        if (preg_match("#^//#", $location)) {
            $location = $this->urlparts['scheme'] . ":$location";
            // If it starts with a slash prepend the absolute path to this server
        } elseif (preg_match("#^/#", $location)) {
            $location = "{$this->urlparts['scheme']}://{$this->urlparts['host']}/" . substr($location, 1);;
            // Anything else, assume a relative path from the current page's directory
        } elseif (!preg_match("#^https?:#", $location)) {
            $location = "{$this->urlparts['scheme']}://{$this->urlparts['host']}{$this->urlparts['directory']}$location";
        }
        return $location;
    }

    // Return the full URL with for a given URI
    // This does the same thing as makeAbsolute()?
    function fullURL($element)
    {
        // $element already has a full path
        if (preg_match("#^https?:#i", $element)) {
            return $element;
        } else {
            // Build the full URL based on $this->urlparts
            $full = $this->urlparts['scheme'] . "://" . $this->urlparts['host'];
            // If it starts with a slash, then ignore the directory
            $full .= (preg_match('#^/#', $element)) ? '' : $this->urlparts['directory'];
            $full .= $element;
            return $full;
        }
    }

    public function snippet($start_str, $end_str)
    {
        $start = strpos($this->content, $start_str);
        $end = strpos($this->content, $end_str, $start);
        return substr($this->content, $start, ($end - $start));
    }

    function clickLink($anchor_text_regex, $bot)
    {
        // Todo: make sure delimiter is not contained in regex?
        if (preg_match("~<a[^>]*?href=[\"']([^\"']+)[\"'][^>]*?>[^<]*?{$anchor_text_regex}[^<]*?</a>~", $this->content, $matches)) {
            $link = $this->makeAbsolute(html_entity_decode($matches[1]));
            return $bot->get($link);
        } else {
            return null;
        }
    }

    function findByRegex($regex)
    {
        $hrefs = array();
        if (preg_match_all($regex, $this->content, $matches)) {
            for ($i = 0; $i < count($matches[1]); $i++)
                $hrefs[] = $matches[1][$i];
        }
        return $hrefs;
    }


    /*  poorly named - fix if you ever use it
        function relativePath()
        {
            return preg_replace("#/(.*)$#", '', $this->url);
        }

        function uriRoot()
        {
            preg_match("#(https?://[a-z0-9_\.]+/)(.*)$#i", $this->url, $matches);
            // Return everything up to and including the slash after the hostname
            return $matches[1];
        }
    */


    function parseForm($name)
    {
        $form = new HttpBotForm($this, $name);
        return $form;
    }
}
