<?php
namespace RoundSphere;

class Ezmws
{
    protected $merchant_id;
    protected $marketplace_id;
    protected $token;
    protected $service_url;
    protected $access_key;
    protected $secret_key;

    protected $other_marketplace_ids = array();

    protected $mws;
    protected $mws_sellers;
    protected $mws_inbound;
    protected $mws_products;
    protected $mws_orders;
    protected $mws_finances;
    protected $mws_merchantfulfillment;

    public function __construct($credentials)
    {
        $this->merchant_id      = $credentials['merchant_id'];
        $this->marketplace_id   = $credentials['marketplace_id'];
        $this->token            = isset($credentials['token']) ? $credentials['token'] : '';
        $this->service_url      = $credentials['service_url'];
        $this->access_key       = $credentials['access_key'];
        $this->secret_key       = $credentials['secret_key'];
    }

    public function __get($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        }
        return (isset($this->$name)) ? $this->$name : null;
    }

    public function setOtherMarketplaceIds($array)
    {
        $this->other_marketplace_ids = $array;
    }

    private function _config()
    {
        $config = array (
            'ServiceURL'      => $this->service_url,
            'ProxyHost'       => null,
            'ProxyPort'       => -1,
            'MaxErrorRetry'   => 3,
        );
        return $config;
    }

    public function looksValid()
    {
        if ($this->merchant_id && $this->marketplace_id && $this->access_key && $this->secret_key) {
            return true;
        } else {
            return false;
        }
    }

    public function testCredentials()
    {
        try {
            $result = $this->ListMarketplaceParticipations();
        } catch (exception $e) {
            return false;
        }

        if ($result) return true;
        return false;
    }

    private function mws()
    {
        if (!isset($this->mws)) {
            $config = $this->_config();
            $this->mws = new \AmazonMws\MarketplaceWebService\Client(
                $this->access_key,
                $this->secret_key,
                $config,
                APPLICATION_NAME,
                APPLICATION_VERSION);
        }
        return $this->mws;
    }

    private function mws_sellers()
    {
        if (!isset($this->mws_sellers)) {
            $sellers_config= $this->_config();
            $sellers_config['ServiceURL'] = $this->service_url.'/Sellers/2011-07-01';

            // Stupid - Sellers client has config at end?!?!
            $this->mws_sellers = new \AmazonMws\MarketplaceWebServiceSellers\Client(
                $this->access_key,
                $this->secret_key,
                APPLICATION_NAME,
                APPLICATION_VERSION,
                $sellers_config
            );
        }
        return $this->mws_sellers;
    }

    private function mws_inbound()
    {
        if (!isset($this->mws_inbound)) {
            $inbound_config = $this->_config();
            // $inbound_config['ServiceURL'] = preg_replace('#/$#', '', $this->service_url).':443/FulfillmentInboundShipment/2010-10-01/';
            $inbound_config['ServiceURL'] = $this->service_url.':443/FulfillmentInboundShipment/2010-10-01/';
            $this->mws_inbound = new FBAInboundServiceMWS_Client(
                $this->access_key,
                $this->secret_key,
                $inbound_config,
                APPLICATION_NAME,
                '2010-10-01'   //Stupid, version 0.01 doesn't work here!!
            );

        }
        return $this->mws_inbound;
    }

    private function mws_products()
    {
        if (!isset($this->mws_products)) {
            $products_config = $this->_config();
            $products_config['ServiceURL'] = $this->service_url.'/Products/2011-10-01';
            $this->mws_products = new \AmazonMws\MarketplaceWebServiceProducts\Client(
                $this->access_key,
                $this->secret_key,
                APPLICATION_NAME,
                APPLICATION_VERSION,
                $products_config
            );

        }
        return $this->mws_products;
    }

    private function mws_orders()
    {
        if (!isset($this->mws_orders)) {
            $orders_config = $this->_config();
            $orders_config['ServiceURL'] = $this->service_url.'/Orders/2011-10-01';
            $this->mws_orders = new \AmazonMws\MarketplaceWebServiceOrders\Client(
                $this->access_key,
                $this->secret_key,
                APPLICATION_NAME,
                APPLICATION_VERSION,
                $orders_config
            );
        }
        return $this->mws_orders;
    }

    private function mws_finances()
    {
        if (!isset($this->mws_finances)) {
            $finances_config = $this->_config();
            $finances_config['ServiceURL'] = $this->service_url.'/Finances/2015-05-01';
            $this->mws_finances = new \AmazonMws\MWSFinancesService\Client(
                $this->access_key,
                $this->secret_key,
                APPLICATION_NAME,
                APPLICATION_VERSION,
                $finances_config
            );
        }
        return $this->mws_finances;
    }

    private function mws_merchantfulfillment()
    {
        if (!isset($this->mws_merchantfulfillment)) {
            $merchantfulfillment_config = $this->_config();
            $merchantfulfillment_config['ServiceURL'] = $this->service_url.'/MerchantFulfillment/2015-06-01';
            $this->mws_merchantfulfillment = new \MWSMerchantFulfillmentService_Client(
                $this->access_key,
                $this->secret_key,
                APPLICATION_NAME,
                APPLICATION_VERSION,
                $merchantfulfillment_config
            );
        }
        return $this->mws_merchantfulfillment;
    }

    private function assignParams(&$request, $params)
    {
        // Optional Parameters
        foreach ($params as $name => $value) {
            $setter = "set{$name}";
            // See if a smple set{$varname} function is available
            if (method_exists($request, $setter)) {
                if (preg_match('#Date#', $name)) {
                    // $value = new DateTime($value);
                    $value = new \DateTime($value, new \DateTimeZone('UTC'));
                } elseif (preg_match('#set(.*)(Id|Type|Status)List#', $setter, $matches)) {
                    // These types of lists,  Have to have a special object passed
                    $type = $matches[2];
                    $list_type = "MarketplaceWebService_Model_{$type}List";
                    // require_once "Model/{$type}List.php";
                    $list = new $list_type;
                    $list_setter = "set{$type}";
                    $list->$list_setter($value);
                    $value = $list;
                }
                $request->$setter($value);
            } else {
                // echo "No method exists for {$setter}\n";
            }
        }
    }

    public function getAuthToken()
    {
        $request = new \AmazonMws\MarketplaceWebServiceSellers\Model\GetAuthTokenRequest();
        $request->setSellerId($this->merchant_id);
        $response = $this->mws_sellers()->getAuthToken($request);

        $result = $response->getGetAuthTokenResult();

        $mwsAuthToken = $result->getMWSAuthToken();

        return $mwsAuthToken;
    }

    public function ListMarketplaceParticipations($token = '')
    {
        $request = new \AmazonMws\MarketplaceWebServiceSellers\Model\ListMarketplaceParticipationsRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $response = $this->mws_sellers()->listMarketplaceParticipations($request);

        $result = $response->getListMarketplaceParticipationsResult();
        $marketplaces   = array();
        $participations = array();
        if ($result->isSetListMarketplaces()) {
            $listMarketplaces = $result->getListMarketplaces();
            $marketplaceList = $listMarketplaces->getMarketplace();
            $this_marketplace = array();
            foreach ($marketplaceList as $marketplace) {
                $this_marketplace['id'] = ($marketplace->isSetMarketplaceId()) ? $marketplace->getMarketplaceId() : '';
                $this_marketplace['name'] = ($marketplace->isSetName()) ? $marketplace->getName() : '';
                $this_marketplace['language_code'] = ($marketplace->isSetDefaultLanguageCode()) ? $marketplace->getDefaultLanguageCode() : '';
                $this_marketplace['country_code'] = ($marketplace->isSetDefaultCountryCode()) ? $marketplace->getDefaultCountryCode() : '';
                $this_marketplace['currency_code'] = ($marketplace->isSetDefaultCurrencyCode()) ? $marketplace->getDefaultCurrencyCode() : '';
                $this_marketplace['domain_name'] = ($marketplace->isSetDomainName()) ? $marketplace->getDomainName() : '';
                $marketplaces[$this_marketplace['id']] = $this_marketplace;
            }

            if ($result->isSetListParticipations()) {
                $listParticipations = $result->getListParticipations();
                $participationList = $listParticipations->getParticipation();
                foreach ($participationList as $participation) {
                    $thisone['marketplace_id'] = $participation->isSetMarketplaceId() ? $participation->getMarketplaceId() : '';
                    $thisone['seller_id'] = $participation->isSetSellerId() ? $participation->getSellerId() : '';
                    $thisone['suspended'] = $participation->isSetHasSellerSuspendedListings() ? $participation->getHasSellerSuspendedListings() : '';
                    if (isset($marketplaces[$thisone['marketplace_id']])) {
                        $thisone = array_merge($thisone, $marketplaces[$thisone['marketplace_id']]);
                    }
                    $participations[] = $thisone;
                }
            }
        }
        return $participations;
    }

    private function buildMarketplaceIdList()
    {
        $marketplaceIdList = array('Id' => array());
        foreach ($this->other_marketplace_ids as $marketplace_id) {
            if ($this->marketplace_id != $marketplace_id) {
                $marketplaceIdList['Id'][] = $marketplace_id;
            }
        }
        return $marketplaceIdList;
    }

    public function RequestReport($params = array())
    {
        if (!is_array($params)) {
            throw new Exception('ezmws::RequestReport() - array parameter required');
        }
        $request = new \AmazonMws\MarketplaceWebService\Model\RequestReportRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        $request->setMarketplaceIdList(array(
            'Id'    => $this->marketplace_id,
        ));
        $this->assignParams($request, $params);

        $response   = $this->mws()->RequestReport($request);
        return $response->getRequestReportResult()->getReportRequestInfo()->myArray();
    }

    public function CancelReportRequests($params = array())
    {
        if (!is_array($params)) {
            throw new Exception('ezmws::CancelReportRequests() - array parameter required');
        }
        $request = new \AmazonMws\MarketplaceWebService\Model\CancelReportRequestsRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        $this->assignParams($request, $params);

        $response   = $this->mws()->CancelReportRequests($request);
        return $response->getCancelReportRequestsResult();
    }

    public function GetReport($report_id)
    {
        $request = new \AmazonMws\MarketplaceWebService\Model\GetReportRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        $request->setReportId($report_id);
        $request->setReport(@fopen('php://memory', 'rw+'));
        $response = $this->mws()->getReport($request);
        $contents = stream_get_contents($request->getReport());
        return $contents;
    }

    public function GetReportHandle($report_id, $filename = null)
    {
        if (file_exists($filename) && filesize($filename)) {
            // Use existing file if it exists
            $fh = fopen($filename, 'r');
        } else {
            // Otherwise download the report
            $request = new \AmazonMws\MarketplaceWebService\Model\GetReportRequest();
            $request->setMerchant($this->merchant_id);
            if (!empty($this->token)) {
                $request->setMWSAuthToken($this->token);
            }
            $request->setMarketplace($this->marketplace_id);
            $request->setReportId($report_id);
            $pid = getmypid();
            $tmpFile = "{$filename}.tmp.{$pid}";

            if ($filename) {
                if (!file_exists($filename)) {
                    touch($filename);
                }
                $lockHandle = fopen($filename, 'r+');
                if (!flock($lockHandle, LOCK_EX | LOCK_NB)) {
                    // Obtain an exclusive lock on the final file handle
                    // so that we don't have two processes try to download the same file
                    return null;
                }
                // If $filename was specified, write to $filename.tmp.$pid
                $fh = @fopen($tmpFile, 'w+');
            } else {
                $fh = @fopen('php://memory', 'rw+');
            }
            $request->setReport($fh);

            try {
                $response = $this->mws()->getReport($request);
            } catch (\Exception $e) {
                if (preg_match('#Received Content-MD5#', $e->getMessage()) && $filename && filesize($tmpFile) > 100000) {
                    echo "Received a Content-MD5 Mismatch, but not throwing an exception because file was over 100k\n";
                    bclog("Received a Content-MD5 Mismatch, but not throwing an exception because file was over 100k");
                    // Don't throw an exception here so that we at least parse the part of the file that we have
                } else {
                    if ($filename) {
                        // Clean up temp file
                        fclose($fh);
                        unlink($tmpFile);
                    }
                    throw $e;
                }
                throw $e;
            }

            if ($filename) {
                // if $filename was specified, move it into place and re-open $fh
                fclose($fh);
                flock($lockHandle, LOCK_UN);
                fclose($lockHandle);
                rename($tmpFile, $filename);
                $fh = fopen($filename, 'r');
            }
        }
        return $fh;
    }

    public function GetReportByRequestId($request_id)
    {
        $response = $this->GetReportRequestList(array(
            'ReportRequestIdList'   => array($request_id)
        ));
        if (isset($response['reports'][0])) {
            switch ($response['reports'][0]['ReportProcessingStatus']) {
                case '_DONE_';
                    return $this->getReport($response['reports'][0]['GeneratedReportId']);

                default:
                case '_DONE_NO_DATA_':
                    return $response['reports'][0];
            }
        }
    }

    public function getReportList($params = array())
    {
        if (!is_array($params)) {
            throw new Exception('ezmws::GetReportRequestList() - array parameter required');
        }
        $request = new \AmazonMws\MarketplaceWebService\Model\GetReportListRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        $this->assignParams($request, $params);

        $response   = $this->mws()->getReportList($request);
        $reports    = array();
        foreach ($response->getGetReportListResult()->GetReportInfoList() as $report) {
            $reports[] = $report->myArray();
            //$reports[] = $report;
        }
        $next_token = $response->getGetReportListResult()->getNextToken();

        return array(
            'next_token'    => $next_token,
            'reports'       => $reports,
        );
    }

    public function GetReportRequestList($params = array())
    {
        if (!is_array($params)) {
            throw new Exception('ezmws::GetReportRequestList() - array parameter required');
        }
        $request = new \AmazonMws\MarketplaceWebService\Model\GetReportRequestListRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        $this->assignParams($request, $params);

        $response   = $this->mws()->getReportRequestList($request);
        $reports    = array();
        foreach ($response->getGetReportRequestListResult()->GetReportRequestInfoList() as $report) {
            $reports[] = $report->myArray();
/*
            $reports[] = array(
                'ReportRequestId'           => $report->ReportRequestId,
                'ReportType'                => $report->ReportType,
                'StartDate'                 => $report->StartDate->format('Y-m-d H:i:s'),
                'EndDate'                   => $report->EndDate->format('Y-m-d H:i:s'),
                'Scheduled'                 => $report->Scheduled,
                'SubmittedDate'             => $report->SubmittedDate->format('Y-m-d H:i:s'),
                'ReportProcessingStatus'    => $report->ReportProcessingStatus,
                'GeneratedReportId'         => $report->GeneratedReportId,
                'StartedProcessingDate'     => $report->StartedProcessingDate->format('Y-m-d H:i:s'),
                'CompletedDate'             => $report->CompletedDate->format('Y-m-d H:i:s'),
            );
*/
        }
        $next_token = $response->getGetReportRequestListResult()->getNextToken();

        return array(
            'next_token'    => $next_token,
            'reports'       => $reports,
        );
    }

    public function manageReportSchedule($params = array())
    {
        $request = new \MarketplaceWebService_Model_ManageReportScheduleRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        $this->assignParams($request, $params);

        $response   = $this->mws()->manageReportSchedule($request);
        $reports    = array();
        return $response;
        // foreach ($response->getGetReportRequestListResult()->GetReportRequestInfoList() as $report) {
    }

    public function GetReportScheduleList($reportTypes = array())
    {
        $request = new \AmazonMws\MarketplaceWebService\Model\GetReportScheduleListRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);
        if ($reportTypes) {
            // Do something better here\
        }

        $response   = $this->mws()->GetReportScheduleList($request);
        $reports    = array();

        $result = $response->getGetReportScheduleListResult();
        $list = $result->getReportScheduleList();

        foreach ($list as $reportSchedule) {
            $reportType = $reportSchedule->getReportType();
            $schedule = $reportSchedule->getSchedule();
            $scheduledDate = $reportSchedule->getScheduledDate();
            $reports[$reportType] = array(
                'ReportType' => $reportType,
                'Schedule'  => $schedule,
                'ScheduledDate' => $scheduledDate,
            );
        }
        return $reports;
    }

    public function productsServiceStatus()
    {
        $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetServiceStatusRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        // object or array of parameters
        $response = $this->mws_products()->getServiceStatus($request);
        $result = $response->getGetServiceStatusResult();
        return $result->getStatus();
    }

    public function getCompetitivePricingForASIN($asins)
    {
        // Need to clean up all of the junk in  here
        $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetCompetitivePricingForASINRequest();

        $asin_list = new \AmazonMws\MarketplaceWebServiceProducts\Model\ASINListType();
        $asin_list->setASIN($asins);

        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplaceId($this->marketplace_id);
        $request->setASINList($asin_list);
        // $this->assignParams($request, $params);

        $response = $this->mws_products()->getCompetitivePricingForASIN($request);


        $getCompetitivePricingForASINResultList = $response->getGetCompetitivePricingForASINResult();

        // Now put the data into a more useful $data array
        $data = array();

        foreach ($getCompetitivePricingForASINResultList as $getCompetitivePricingForASINResult) {
            $asin = false;
            if ($getCompetitivePricingForASINResult->isSetASIN()) {
                $asin = $getCompetitivePricingForASINResult->getASIN();
            } else {
                continue;
            }

            if ($getCompetitivePricingForASINResult->isSetStatus()) {
                $data[$asin]['status'] = $getCompetitivePricingForASINResult->getStatus();
            }
            if ($getCompetitivePricingForASINResult->isSetProduct()) {
                $product = $getCompetitivePricingForASINResult->getProduct();

                if ($product->isSetCompetitivePricing()) {
                    $competitivePricing = $product->getCompetitivePricing();
                    if ($competitivePricing->isSetCompetitivePrices()) {
                        $competitivePrices = $competitivePricing->getCompetitivePrices();
                        $competitivePriceList = $competitivePrices->getCompetitivePrice();
                        $data[$asin]['comps'] = array();

                        foreach ($competitivePriceList as $competitivePrice) {
                            $condition      = $competitivePrice->isSetCondition()       ? $competitivePrice->getCondition() : '';
                            $sub_condition  = $competitivePrice->isSetSubCondition()    ? $competitivePrice->getSubCondition() : '';
                            $price = $landed_price = $listed_price = $shipping_price = 0;

                            if ($competitivePrice->isSetPrice()) {
                                $price = $competitivePrice->getPrice();
                                $landed_price   = $price->isSetLandedPrice()    ? $price->getLandedPrice()->getAmount() : 0;
                                $listed_price   = $price->isSetListingPrice()   ? $price->getListingPrice()->getAmount() : 0;
                                $shipping_price = $price->isSetShipping()       ? $price->getShipping()->getAmount() : 0;
                            }

                            $data[$asin]['comps'][] = array(
                                'condition'     => $condition,
                                'sub_condition' => $sub_condition,
                                'landed_price'  => $landed_price,
                                'listed_price'  => $listed_price,
                                'shipping_price'=> $shipping_price,
                            );

                        }
                    }

                    if ($competitivePricing->isSetNumberOfOfferListings()) {

                        $numberOfOfferListings = $competitivePricing->getNumberOfOfferListings();
                        $offerListingCountList = $numberOfOfferListings->getOfferListingCount();

                        foreach ($offerListingCountList as $offerListingCount) {
                            $data[$asin]['number'][$offerListingCount->getCondition()] = $offerListingCount->getValue();
                        }

                    }
                    if ($competitivePricing->isSetTradeInValue()) {
                        $tradeInValue = $competitivePricing->getTradeInValue();
                        $data[$asin]['trade_in_price'] = $tradeInValue->isSetAmount() ? $tradeInValue->getAmount() : 0;
                    }
                }
                if ($product->isSetSalesRankings()) {
                    $data[$asin]['salesranks'] = array();
                    $salesRankings = $product->getSalesRankings();
                    $salesRankList = $salesRankings->getSalesRank();
                    foreach ($salesRankList as $salesRank) {
                        $data[$asin]['salesranks'][$salesRank->getProductCategoryId()] = $salesRank->getRank();

                    }
                }

            }

        }
        return $data;
    }

    private function arrayFromProductsResult($products)
    {
        $product_list = $products->getProduct();
        $return = [];
        foreach ($product_list as $product) {
            $productArray = $this->arrayFromProduct($product);
            $return[$product['asin']] = $productArray;
        }
        return $return;
    }

    private function arrayFromProduct($product)
    {
        if (!$product) {
            return [];
        }
        $identifiers = $product->isSetIdentifiers() ? $product->getIdentifiers() : array();
        $asin = '';
        if ($identifiers && $identifiers->isSetMarketplaceASIN()) {
            $marketplace_asin   = $identifiers->getMarketplaceASIN();
            $marketplace_id     = $marketplace_asin->isSetMarketplaceId()   ? $marketplace_asin->getMarketplaceId() : '';
            $asin               = $marketplace_asin->isSetASIN()            ? $marketplace_asin->getASIN()          : '';
            $return[$asin]['marketplace_id']   = $marketplace_id;
            $return[$asin]['asin']             = $asin;
        }
        if (!$asin) {
            return null;
        }

        $attribute_sets = $product->isSetAttributeSets() ? $product->getAttributeSets() : null;
        if ($attribute_sets && $attribute_sets->isSetAny()) {
            $nodeList = $attribute_sets->getAny();
            foreach ($nodeList as $domNode){
                $domDocument =  new \DOMDocument();
                $domDocument->preserveWhiteSpace = false;
                $domDocument->formatOutput = true;
                $nodeStr = $domDocument->saveXML($domDocument->importNode($domNode,true));
                $nodename = str_replace('ns2:', '', $domNode->nodeName);
                $return[$asin]['attribute_sets'][$nodename] = json_decode(json_encode((array) simplexml_load_string($nodeStr, 'SimpleXMLElement', 0, 'http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd', false)),1);
            }
        }

        if ($product->isSetSalesRankings()) {
            $rankings   = $product->getSalesRankings();
            $rank_list  = $rankings->getSalesRank();
            foreach ($rank_list as $rank) {
                $category   = $rank->isSetProductCategoryId()   ? $rank->getProductCategoryId() : '';
                $salesrank  = $rank->isSetRank()                ? $rank->getRank()              : '';
                if ($category && $salesrank) {
                    $return[$asin]['ranks'][$category] = $salesrank;
                }
            }
        }

        return $return;
    }

    public function ListMatchingProducts($query, $query_context_id)
    {
        $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\ListMatchingProductsRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplaceId($this->marketplace_id);
        $request->setQuery($query);
        $request->setQueryContextId($query_context_id);

        $response = $this->mws_products()->ListMatchingProducts($request);

        $result_list = $response->getListMatchingProductsResult();
        $return =  $result_list->isSetProducts() ? $this->arrayFromProductsResult($result_list->getProducts()) : '';
        return $return;
    }

    public function getProductCategoriesForASIN($asin)
    {
        $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetProductCategoriesForASINRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplaceId($this->marketplace_id);
        $request->setASIN($asin);

        $response = $this->mws_products()->GetProductCategoriesForASIN($request);

        $return = array();
        if ($response->isSetGetProductCategoriesForASINResult()) {
            $getProductCategoriesForASINResult = $response->getGetProductCategoriesForASINResult();
            $selfList = $getProductCategoriesForASINResult->getSelf();
            foreach ($selfList as $self) {

                $return[$asin][] = self::recurseCategories($self);
            }

        }
        return $return;
    }

    private function recurseCategories($cat)
    {
        $cat_id     = $cat->isSetProductCategoryId()   ? $cat->getProductCategoryId()     : '';
        $cat_name   = $cat->isSetProductCategoryName() ? $cat->getProductCategoryName()   : '';
        $return = array(
            'category_id'   => $cat_id,
            'category_name' => $cat_name,
        );
        if ($cat->isSetParent()) {
            $return['parent'] = self::recurseCategories($cat->getParent());
        }
        return $return;
    }

    public function getMatchingProduct($asins)
    {
        if (is_string($asins)) {
            $asins = array($asins);
        }
        $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetMatchingProductRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplaceId($this->marketplace_id);
        $asin_list = new \AmazonMws\MarketplaceWebServiceProducts\Model\ASINListType();
        $asin_list->setASIN($asins);
        $request->setASINList($asin_list);
        $response = $this->mws_products()->getMatchingProduct($request);
        $result = $response->getGetMatchingProductResult();
        $product = $result[0]->getProduct();

        return $this->arrayFromProduct($product);
    }


    public function GetMatchingProductForId($id, $type)
    {
        $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetMatchingProductForIdRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplaceId($this->marketplace_id);
        $request->setIdType($type);
        $idlist = new \AmazonMws\MarketplaceWebServiceProducts\Model\IdListType();
        $idlist->setId($id);
        $request->SetIdList($idlist);

        $response = $this->mws_products()->GetMatchingProductForId($request);

        $result_list = $response->getGetMatchingProductForIdResult();
        $return = array();
        foreach ($result_list as $result) {
            $id         = $result->isSetId()        ? $result->getId()      : '';
            $id_type    = $result->isSetIdType()    ? $result->getIdType()  : '';
            $status     = $result->isSetStatus()    ? $result->getStatus()  : '';
            $return[$id] = array(
                'id'        => $id,
                'type'      => $id_type,
                'status'    => $status,
                'products'  => $result->isSetProducts() ? $this->arrayFromProductsResult($result->getProducts()) : '',
            );
        }

        return $return;
    }


    public function getMatchingProductForASIN($asin)
    {
        // Not an actual MWS Call, but I should make this that returns a single ASIN
        $response = $this->getMatchingProductForID($asin, 'ASIN');

        if (isset($response[$asin]['products'][$asin])) {
            return $response[$asin]['products'][$asin];
        }
        return null;
    }

    public function GetLowestPricedOffersForASIN($asin, $condition = 'New', $marketplaceId = null, $raw = false)
    {
        try {
            $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetLowestPricedOffersForASINRequest();

            $request->setSellerId($this->merchant_id);
            if (!empty($this->token)) {
                $request->setMWSAuthToken($this->token);
            }
            if (is_null($marketplaceId)) {
                $request->setMarketplaceId($this->marketplace_id);
            } else {
                $request->setMarketplaceId($marketplaceId);
            }
            $request->setASIN($asin);
            $request->setItemCondition($condition);

            $response = $this->mws_products()->GetLowestPricedOffersForASIN($request);
            if ($raw) {
                return $response;
            }

            if ($response->isSetGetLowestPricedOffersForASINResult()) {
                $result = $response->getGetLowestPricedOffersForASINResult();
                return $result->asArray();
            }
            return $cleaned;
        } catch (Exception $e) {
            throw $e;
        }
    }


    public function getLowestOfferListingsForASIN($asins, $condition = 'New')
    {
        try {
            $request = new \AmazonMws\MarketplaceWebServiceProducts\Model\GetLowestOfferListingsForASINRequest();

            $asin_list = new \AmazonMws\MarketplaceWebServiceProducts\Model\ASINListType();
            $asin_list->setASIN($asins);

            $request->setSellerId($this->merchant_id);
            if (!empty($this->token)) {
                $request->setMWSAuthToken($this->token);
            }
            $request->setMarketplaceId($this->marketplace_id);
            $request->setItemCondition($condition);
            $request->setASINList($asin_list);
            // $this->assignParams($request, $params);

            $response = $this->mws_products()->getLowestOfferListingsForASIN($request);

            $getLowestOfferListingsForASINResultList = $response->getGetLowestOfferListingsForASINResult();

            // Now put the data into a more useful $data array
            $data = array();

            foreach ($getLowestOfferListingsForASINResultList as $getLowestOfferListingsForASINResult) {
                $error = $getLowestOfferListingsForASINResult->getError();
                if ($error)  {
                    continue;
                }
                $asin           = $getLowestOfferListingsForASINResult->getASIN();

                $status         = $getLowestOfferListingsForASINResult->getStatus();
                $all_considered = $getLowestOfferListingsForASINResult->getAllOfferListingsConsidered();
                $product        = $getLowestOfferListingsForASINResult->getProduct();

                $identifiers    = $product->getIdentifiers();
                $marketplaceASIN = $identifiers->getMarketplaceASIN();
                if (!empty($marketplaceASIN)) {
                    $marketplace_id = $marketplaceASIN->getMarketplaceId();
                    $asin           = $marketplaceASIN->getASIN();
                }

                $SKUIdentifier = $identifiers->getSKUIdentifier();
                if (!empty($SKUIdentifier)) {
                    $marketplace_id = $SKUIdentifier->getMarketplaceId();
                    $seller_id      = $SKUIdentifier->getSellerId();
                    $seller_sku     = $SKUIdentifier->getSellerSKU();
                }

                $prices = array();
                if ($product->isSetLowestOfferListings()) {
                    $lowestOfferListings = $product->getLowestOfferListings();
                    $lowestOfferListingList = $lowestOfferListings->getLowestOfferListing();
                    foreach ($lowestOfferListingList as $lowestOfferListing) {
                        if ($lowestOfferListing->isSetQualifiers()) {
                            $qualifiers     = $lowestOfferListing->getQualifiers();
                            $cond           = $qualifiers->getItemCondition();
                            $subcond        = $qualifiers->getItemSubcondition();
                            $fc             = $qualifiers->getFulfillmentChannel();
                            $shipdom        = $qualifiers->getShipsDomestically();
                            $shippingTime   = $qualifiers->getShippingTime();
                            $shipmax        = $shippingTime->getMax();
                            $rating         = $qualifiers->getSellerPositiveFeedbackRating();
                            $number         = $lowestOfferListing->getNumberOfOfferListingsConsidered() ;
                            $fbcount        = $lowestOfferListing->getSellerFeedbackCount();

                            $price1 = $lowestOfferListing->getPrice();
                            if ($price1->getLandedPrice()) {
                                $price_landed = $price1->getLandedPrice()->getAmount();
                            } else {
                                $price_landed = '';
                            }
                            if ($price1->getListingPrice()) {
                                $price  = $price1->getListingPrice()->getAmount();
                            } else {
                                $price = '';
                            }

                            if ($price1->getShipping()) {
                                $price_shipping  = $price1->getShipping()->getAmount();
                            } else {
                                $price_shipping = '';
                            }

                            $multi      = $lowestOfferListing->getMultipleOffersAtLowestPrice() ;
                            $prices[] = array(
                                'cond'      => $cond,
                                'subcond'   => $subcond,
                                'fc'        => $fc,
                                'shipdom'   => $shipdom,
                                'shipmax'   => $shipmax,
                                'rating'    => $rating,
                                'number'    => $number,
                                'fbcount'   => $fbcount,
                                'multi'     => $multi,
                                'price'     => $price,
                                'price_shipping'    => $price_shipping,
                                'price_landed'      => $price_landed,
                            );
                        }
                    }
                }
                $data[$asin] = array(
                    'status'            => $status,
                    'all_considered'    => $all_considered,
                    'prices'            => $prices,
                );
            }
            return $data;
        } catch (Exception $e) {
        }
    }

    public function submitFeed($report_type, $data)
    {
        $fh = tmpfile();
        fwrite($fh, $data);
        rewind($fh);

        $request = new \MarketplaceWebService_Model_SubmitFeedRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplaceIdList(array('Id' => array($this->marketplace_id)));
        $request->setFeedType($report_type);
        $request->setContentMd5(base64_encode(md5(stream_get_contents($fh), true)));
        rewind($fh);
        $request->setPurgeAndReplace(false);
        $request->setFeedContent($fh);
        rewind($fh);


        $response = $this->mws()->submitFeed($request);
        $result = $response->isSetSubmitFeedResult() ? $response->getSubmitFeedResult() : null;
        $info   = $result->isSetFeedSubmissionInfo() ? $result->getFeedSubmissionInfo() : null;

        if (!$info) return null;
        return $this->feedSubmissionInfo($info);
    }

    protected function feedSubmissionInfo(MarketplaceWebService_Model_FeedSubmissionInfo $info)
    {
        $return = array(
            'id'        => $info->isSetFeedSubmissionId()           ? $info->getFeedSubmissionId()                                 : '',
            'type'      => $info->isSetFeedType()                   ? $info->getFeedType()                                         : '',
            'submitted' => $info->isSetSubmittedDate()              ? $info->getSubmittedDate()->format('Y-m-d H:i:s')             : '',
            'status'    => $info->isSetFeedProcessingStatus()       ? $info->getFeedProcessingStatus()                             : '',
            'started'   => $info->isSetStartedProcessingDate()      ? $info->getStartedProcessingDate()->format('Y-m-d H:i:s')     : '',
            'completed' => $info->isSetCompletedProcessingDate()    ? $info->getCompletedProcessingDate()->format('Y-m-d H:i:s')   : '',
        );
        return $return;
    }

    public function getFeedSubmissionList($params)
    {
        $request = new \MarketplaceWebService_Model_GetFeedSubmissionListRequest($params);
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setMarketplace($this->marketplace_id);

        // May need to do special stuff for some parameters here
        if (isset($params['FeedSubmissionIdList'])) {
            if (is_array($params['FeedSubmissionIdList'])) {
                $idlist = new \MarketplaceWebService_Model_IdList();
                // I don't think this works
                $idlist->setId($params['FeedSubmissionIdList']);
            }
        }

        $response = $this->mws()->getFeedSubmissionList($request);

        $return = array();
        $result = $response->getGetFeedSubmissionListResult();
        if ($result->isSetNextToken()) {
            $return['NextToken'] = $result->getNextToken();
        }
        if ($result->isSetHasNext()) {
            $return['HasNext'] = $result->getHasNext();
        }
        $info_list = $result->getFeedSubmissionInfoList();
        foreach ($info_list as $info) {
            $parsed = $this->FeedSubmissionInfo($info);
            $return['feeds'][$parsed['id']] = $parsed;
        }

        return $return;
    }


    public function getFeedSubmissionResult($feed_id)
    {
        $request = new \MarketplaceWebService_Model_GetFeedSubmissionResultRequest();
        $request->setMerchant($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setFeedSubmissionId($feed_id);
        $fh = @fopen('php://memory', 'rw+');
        $request->setFeedSubmissionResult($fh);

        $response = $this->mws()->getFeedSubmissionResult($request);
        $result = $response->getGetFeedSubmissionResultResult();
        $expected_md5 = $result->isSetContentMd5() ? $result->getContentMd5(): '';

        $contents = '';
        while ($bytes = fread($fh, 8192)) {
            $contents .= $bytes;
        }
        $md5 = Util::md5_base64($contents);
        if ($md5 != $expected_md5) {
            throw new Exception("MD5 of contents ($md5) does not match expected md5 of {$expected_md5}");
        }
        return $contents;
    }

    public function listOrderItems($order_id)
    {
        $request = new \AmazonMws\MarketplaceWebServiceOrders\Model\ListOrderItemsRequest();
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $request->setAmazonOrderId($order_id);
        try {
            $response = $this->mws_orders()->ListOrderItems($request);
            $items = array();
            $result = $response->getListOrderItemsResult();
        } catch (Exception $e) {
            return false;
        }
        $orderItems = $result->getOrderItems();
        // $orderItemsList = $orderItems->getOrderItem();
        foreach ($orderItems as $item) {
            $my_item = array(
                'asin'                      => $item->isSetASIN()               ? $item->getASIN()                  : '',
                'msku'                      => $item->isSetSellerSKU()          ? $item->getSellerSKU()             : '',
                'order_item_id'             => $item->isSetOrderItemId()        ? $item->getOrderItemId()           : '',
                'title'                     => $item->isSetTitle()              ? $item->getTitle()                 : '',
                'qty_ordered'               => $item->isSetQuantityOrdered()    ? $item->getQuantityOrdered()       : '',
                'qty_shipped'               => $item->isSetQuantityShipped()    ? $item->getQuantityShipped()       : '',
                'promotion_ids'             => $item->isSetPromotionIds()       ? $item->getPromotionIds()          : '',
                'condition_note'            => $item->isSetConditionNote()      ? $item->getConditionNote()         : '',
                'condition_id'              => $item->isSetConditionId()        ? $item->getConditionId()           : '',
                'condition_subtype_id'      => $item->isSetConditionSubtypeId() ? $item->getConditionSubtypeId()    : '',
                'price_item'                => 0,
                'price_shipping'            => 0,
                'price_giftwrap'            => 0,
                'price_tax'                 => 0,
                'price_shipping_tax'        => 0,
                'price_giftwrap_tax'        => 0,
                'price_shipping_discount'   => 0,
                'price_promotion_discount'  => 0,
            );
            if ($item->isSetItemPrice()) {
                $price = $item->getItemPrice();
                $my_item['price_item']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetShippingPrice()) {
                $price = $item->getShippingPrice();
                $my_item['price_shipping']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetGiftWrapPrice()) {
                $price = $item->getGiftWrapPrice();
                $my_item['price_giftwrap']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetItemTax()) {
                $price = $item->getItemTax();
                $my_item['price_tax']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetShippingTax()) {
                $price = $item->getShippingTax();
                $my_item['price_shipping_tax']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetGiftWrapTax()) {
                $price = $item->getGiftWrapTax();
                $my_item['price_giftwrap_tax']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetShippingDiscount()) {
                $price = $item->getShippingDiscount();
                $my_item['price_shipping_discount']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }
            if ($item->isSetPromotionDiscount()) {
                $price = $item->getPromotionDiscount();
                $my_item['price_promotion_discount']   = $price->isSetAmount()     ? $price->getAmount() : '';
            }

            $items[] = $my_item;
        }
        // Restore rate of one every two seconds
        if ($this->marketplace_id == 'A2EUQ1WTGCTBG2') {
            // Canada seems to be pickier
            sleep(4);
        } else {
            sleep(2);
        }
        return $items;
    }

    // Note that each order calls listOrderItems() which takes at least two seconds each
    public function listOrdersByNextToken($token, $order_detail = true)
    {
        $token = str_replace(' ', '+', $token);
        $request = new \AmazonMws\MarketplaceWebServiceOrders\Model\ListOrdersByNextTokenRequest();
        $request->setNextToken($token);
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }
        $response   = $this->mws_orders()->listOrdersByNextToken($request);
        $result     = $response->getListOrdersByNextTokenResult();
        return $this->processListOrdersResult($result, $order_detail);
    }

    // Note that each order calls listOrderItems() which takes at least two seconds each
    public function listOrders($params, $order_detail = true)
    {
       if (!is_array($params)) {
            throw new Exception('ezmws::listOrders() - array parameter required');
        }

        foreach (array('LastUpdatedAfter', 'LastUpdatedBefore') as $timestampField) {
            if (isset($params[$timestampField]) && is_object($params[$timestampField]) && 'DateTime' == get_class($params[$timestampField])) {
                // Latest MWS Orders client library needs a string instead of DateTime object
                $params[$timestampField] = $params[$timestampField]->format('Y-m-d').'T'.$params[$timestampField]->format('H:i:s').'Z';
            }
        }
        $request = new \AmazonMws\MarketplaceWebServiceOrders\Model\ListOrdersRequest($params);
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }

        $request->setMarketplaceId(array(
            $this->marketplace_id,
        ));

        $response   = $this->mws_orders()->listOrders($request);
        $result     = $response->getListOrdersResult();
        return $this->processListOrdersResult($result, $order_detail);
    }

    // Note that each order calls listOrderItems() which takes at least two seconds each
    public function getOrder($params, $order_detail = true)
    {
        if (is_string($params)) {
            $order_id = trim($params);

            $params = array(
                'AmazonOrderId' => array(
                    $order_id,
                ),
            );
        }

        if (!is_array($params)) {
            throw new Exception('ezmws::getOrder() - array parameter required');
        }

        $request = new \AmazonMws\MarketplaceWebServiceOrders\Model\GetOrderRequest($params);
        $request->setSellerId($this->merchant_id);
        if (!empty($this->token)) {
            $request->setMWSAuthToken($this->token);
        }

        $response   = $this->mws_orders()->getOrder($request);
        $result     = $response->getGetOrderResult();
        return $this->processListOrdersResult($result, $order_detail);
    }

    private function processListOrdersResult($result, $order_detail = true)
    {
        if (method_exists($result, 'isSetNextToken')) {
            $next_token             = $result->isSetNextToken()         ? $result->getNextToken()           : '';
            $created_before         = $result->isSetCreatedBefore()     ? $result->getCreatedBefore()       : '';
            $last_updated_before    = $result->isSetLastUpdatedBefore() ? $result->getLastUpdatedBefore()   : '';
        } else {
            $next_token             = '';
            $created_before         = '';
            $last_updated_before    = '';
        }
        $myorders = array();

        $myorders = array();
        if ($result->isSetOrders()) {
            $orders = $result->getOrders();
            // $orderList = $orders->getOrder();
            foreach ($orders as $order) {
                $address = $order->isSetShippingAddress() ? $order->getShippingAddress() : '';

                $isBusiness = ($order->isSetIsBusinessOrder() && $order->getIsBusinessOrder() == 'true');
                $isPrime    = ($order->isSetIsPrime() && $order->getIsPrime() == 'true');

                $this_order = array(
                    'id'                    => $order->isSetAmazonOrderId()                 ? $order->getAmazonOrderId()        : '',
                    'merchant_order_id'     => $order->isSetSellerOrderId()                 ? $order-> getSellerOrderId()       : '',
                    'purchase_date'         => $order->isSetPurchaseDate()                  ? $order->getPurchaseDate()         : '',
                    'last_updated_date'     => $order->isSetLastUpdateDate()                ? $order->getLastUpdateDate()       : '',
                    'status'                => $order->isSetOrderStatus()                   ? $order->getOrderStatus()          : '',
                    'fulfillment_channel'   => $order->isSetFulfillmentChannel()            ? $order->getFulfillmentChannel()   : '',
                    'sales_channel'         => $order->isSetSalesChannel()                  ? $order->getSalesChannel()         : '',
                    'ship_service_level'    => $order->isSetShipServiceLevel()              ? $order->getShipServiceLevel()     : '',
                    'recipient'             => ($address && $address->isSetName())          ? $address->getName()               : '',
                    'ship_address1'         => ($address && $address->isSetAddressLine1())  ? $address->getAddressLine1()       : '',
                    'ship_address2'         => ($address && $address->isSetAddressLine2())  ? $address->getAddressLine2()       : '',
                    'ship_city'             => ($address && $address->isSetCity())          ? $address->getCity()               : '',
                    'ship_state'            => ($address && $address->isSetStateOrRegion()) ? $address->getStateOrRegion()      : '',
                    'ship_zip'              => ($address && $address->isSetPostalCode())    ? $address->getPostalCode()         : '',
                    'ship_country'          => ($address && $address->isSetCountryCode())   ? $address->getCountryCode()        : '',
                    'buyer_email'           => $order->isSetBuyerEmail()                    ? $order->getBuyerEmail()           : '',
                    'buyer_name'            => $order->isSetBuyerName()                     ? $order->getBuyerName()            : '',
                    'type'                  => $isBusiness                                  ? 'business'                        : '',
                    'prime'                 => $isPrime                                     ? 'y'                               : 'n',
                );
                if ($order_detail) {
                    $this_order['items'] = $this->listOrderItems($this_order['id']);
                }
                $myorders[] = $this_order;
            }
        }

        return array(
            'next_token'            => $next_token,
            'orders'                => $myorders,
            'created_before'        => $created_before,
            'last_updated_before'   => $last_updated_before,
        );
    }

    public function GetServiceStatusFinances()
    {
        $request = new \AmazonMws\MWSFinancesService\Model\GetServiceStatusRequest();
        $request->setSellerId($this->merchant_id);
        $request->setMWSAuthToken($this->token);

        $response = $this->mws_finances()->GetServiceStatus($request);
        $result = $response->getGetServiceStatusResult();
        return $result->asArray();
    }

    public function ListFinancialEventGroups($params)
    {
        $request = new \AmazonMws\MWSFinancesService\Model\ListFinancialEventGroupsRequest();
        $request->setSellerId($this->merchant_id);
        $request->setMWSAuthToken($this->token);

        $this->assignParams($request, $params);

        $response = $this->mws_finances()->ListFinancialEventGroups($request);

        if ($response->isSetListFinancialEventGroupsResult()) {
            $result = $response->getListFinancialEventGroupsResult();
            return $result->asArray();
        }
        return null;
    }

    public function ListFinancialEventGroupsByNextToken($token)
    {
        $request = new \AmazonMws\MWSFinancesService\Model\ListFinancialEventGroupsByNextTokenRequest();
        $request->setSellerId($this->merchant_id);
        $request->setMWSAuthToken($this->token);

        $request->setNextToken($token);

        $response = $this->mws_finances()->ListFinancialEventGroupsByNextToken($request);

        if ($response->isSetListFinancialEventGroupsByNextTokenResult()) {
            $result = $response->getListFinancialEventGroupsByNextTokenResult();
            return $result->asArray();
        }
        return null;
    }

    public function ListFinancialEvents($params)
    {
        $request = new \AmazonMws\MWSFinancesService\Model\ListFinancialEventsRequest();
        $request->setSellerId($this->merchant_id);
        $request->setMWSAuthToken($this->token);

        $this->assignParams($request, $params);

        $response = $this->mws_finances()->ListFinancialEvents($request);

        if ($response->isSetListFinancialEventsResult()) {
            $result = $response->getListFinancialEventsResult();
            return $result->asArray();
        }
        return null;
    }

    public function ListFinancialEventsByNextToken($token)
    {
        $request = new \AmazonMws\MWSFinancesService\Model\ListFinancialEventsByNextTokenRequest();
        $request->setSellerId($this->merchant_id);
        $request->setMWSAuthToken($this->token);

        $request->setNextToken($token);

        $response = $this->mws_finances()->ListFinancialEventsByNextToken($request);

        if ($response->isSetListFinancialEventsByNextTokenResult()) {
            $result = $response->getListFinancialEventsByNextTokenResult();
            return $result->asArray();
        }
        return null;
    }

    public function createInboundShipmentPlan($params)
    {
        $params['SellerId']     = $this->merchant_id;
        $params['Marketplace']  = $this->marketplace_id;

        // Call the actual API here
            $response = $this->mws_inbound()->createInboundShipmentPlan($params);

        $plans = $response-> getCreateInboundShipmentPlanResult()->getInboundShipmentPlans()->getmember();

        $simple = array();
        foreach ($plans as $plan) {
            $shipment_id = $plan->getShipmentId();
            $shipto = $plan->getShipToAddress();
            $addr = array(
                'name'      => $shipto->getName(),
                'addr1'     => $shipto->getAddressLine1(),
                'addr2'     => $shipto->isSetAddressLine2()     ? $shipto->getAddressLine2() : '',
                'county'    => $shipto->isSetDistrictOrCounty() ? $shipto->getDistrictOrCounty()    : '',
                'city'      => $shipto->getCity(),
                'state'     => $shipto->getStateOrProvinceCode(),
                'country'   => $shipto->getCountryCode(),
                'zip'       => $shipto->getPostalCode(),
            );
            $fbaitems = $plan->getItems()->getmember();
            foreach ($fbaitems as $fbaitem) {
                $items[$fbaitem->getSellerSKU()] = array(
                    'sku'   => $fbaitem->getSellerSKU(),
                    'fnsku' => $fbaitem->getFulfillmentNetworkSKU(),
                    'qty'   => $fbaitem->getQuantity(),
                );
            }
            $simple[$shipment_id] = array(
                'shipment_id'           => $shipment_id,
                'label_prep_type'       => $plan->getLabelPrepType(),
                'ship_to_addr'          => $addr,
                'fulfillment_center_id' => $plan->getDestinationFulfillmentCenterId(),
                'items'                 => $items,
            );
        }
        return $simple;
    }

    public function updateInboundShipment($params = array())
    {
        // Add our required parameters
        $params['SellerId']     = $this->merchant_id;
        $params['Marketplace']  = $this->marketplace_id;
        $response = $this->mws_inbound()->updateInboundShipment($params);
        if ($response->isSetUpdateInboundShipmentResult()) {
            $result = $response->getUpdateInboundShipmentResult();
            if ($result->isSetShipmentId()) {
                // Just return the shipment ID on success
                return $result->getShipmentId();
            }
        }
        return $response;
    }

    public function createInboundShipment($params = array())
    {
        // Add our required parameters
        $params['SellerId']     = $this->merchant_id;
        $params['Marketplace']  = $this->marketplace_id;

        $response = $this->mws_inbound()->createInboundShipment($params);

        if ($response->isSetCreateInboundShipmentResult()) {
            $result = $response->getCreateInboundShipmentResult();
            if ($result->isSetShipmentId()) {
                // Just return the shipment ID on success
                return $result->getShipmentId();
            }
        }
        return false;
    }

    public function listInboundShipments($params = array())
    {
        // Add our required parameters
        $params['SellerId']     = $this->merchant_id;
        $params['Marketplace']  = $this->marketplace_id;

        $response = $this->mws_inbound()->listInboundShipments($params);
        $shipments = array();
        if ($response->isSetListInboundShipmentsResult()) {
            $result = $response->getListInboundShipmentsResult();
            if ($result->isSetShipmentData()) {
                $shipment_data = $result->getShipmentData();
                $member_list = $shipment_data->getMember();
                foreach ($member_list as $member) {
                    $shipment_id    = $member->isSetShipmentId()                        ? $member->getShipmentId()                      : '';
                    $shipments[$shipment_id] = array(
                        'shipment_id'   => $shipment_id,
                        'name'          => $member->isSetShipmentName()                      ? $member->getShipmentName()                    : '',
                        'fc_id'         => $member->isSetDestinationFulfillmentCenterId()    ? $member->getDestinationFulfillmentCenterId()  : '',
                        'status'        => $member->isSetShipmentStatus()                    ? $member->getShipmentStatus()                  : '',
                        'prep_type'     => $member->isSetLabelPrepType()                     ? $member->getLabelPrepType()                   : '',
                        'ship_from'     => 'unavailable',
                    );
                    if ($member->isSetShipFromAddress()) {
                        $from = $member->getShipFromAddress();
                        $shipments[$shipment_id]['ship_from'] = array(
                            'name'      => $from->isSetName()                   ? $from->getName()                  : '',
                            'addr1'     => $from->isSetAddressLine1()           ? $from->getAddressLine1()          : '',
                            'addr2'     => $from->isSetAddressLine2()           ? $from->getAddressLine2()          : '',
                            'county'    => $from->isSetDistrictOrCounty()       ? $from->getDistrictOrCounty()      : '',
                            'city'      => $from->isSetCity()                   ? $from->getCity()                  : '',
                            'state'     => $from->isSetStateOrProvinceCode()    ? $from->getStateOrProvinceCode()   : '',
                            'zip'       => $from->isSetPostalCode()             ? $from->getPostalCode()            : '',
                        );
                    }
                }
            }
        }
        return $shipments;
    }

    public function listInboundShipmentItems($params = array())
    {
        // Add our required parameters
        $params['SellerId']     = $this->merchant_id;
        $params['Marketplace']  = $this->marketplace_id;

        if (isset($params['NextToken'])) {
            $response = $this->mws_inbound()->listInboundShipmentItemsByNextToken($params);
        } else {
            $response = $this->mws_inbound()->listInboundShipmentItems($params);
        }

        $items      = array();
        $next_token = '';

        if ($response) {
            if (isset($params['NextToken'])) {
                $listInboundShipmentItemsResult = $response->getListInboundShipmentItemsByNextTokenResult();
            } else {
                $listInboundShipmentItemsResult = $response->getListInboundShipmentItemsResult();
            }
            if ($listInboundShipmentItemsResult->isSetItemData()) {
                $itemData = $listInboundShipmentItemsResult->getItemData();
                $memberList = $itemData->getmember();
                foreach ($memberList as $member) {
                    $msku       = $member->isSetSellerSKU()             ? $member->getSellerSKU()               : '';
                    $fnsku      = $member->isSetFulfillmentNetworkSKU() ? $member->getFulfillmentNetworkSKU()   : '';
                    $shipped    = $member->isSetQuantityShipped()       ? $member->getQuantityShipped()         : '';
                    $received   = $member->isSetQuantityReceived()      ? $member->getQuantityReceived()        : '';
                    if ($msku) {
                        $items[$msku] = array(
                            'msku'      => $msku,
                            'fnsku'     => $fnsku,
                            'shipped'   => $shipped,
                            'received'  => $received,
                        );
                    }
                }
                $next_token = ($listInboundShipmentItemsResult->isSetNextToken()) ? $listInboundShipmentItemsResult->getNextToken() : '';
            }
        }
        return array(
            'items'         => $items,
            'next_token'    => $next_token
        );

    }

    private function prettyPrint($nodeList)
    {
        foreach ($nodeList as $domNode){
            $domDocument =  new \DOMDocument();
            $domDocument->preserveWhiteSpace = false;
            $domDocument->formatOutput = true;
            $nodeStr = $domDocument->saveXML($domDocument->importNode($domNode,true));
            echo($nodeStr."\n");
        }
    }

}
