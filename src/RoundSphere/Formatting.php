<?php

namespace RoundSphere;

// Formatting function
class Formatting
{
    static public function money($number)
    {
        echo '$' . number_format($number, 2, '.', ',');
    }

    static public function number($number)
    {
        echo number_format($number, 2, '.', ',');
    }

    static public function integer($number)
    {
        echo number_format($number, 0, '.', ',');
    }

    static public function percentage($number, $decimals = 0)
    {
        printf("%.{$decimals}f%%", $number * 100);
    }

    static public function truncate_string($string, $max_length)
    {
        if (strlen($string) > $max_length) {
            $string = substr($string, 0, $max_length);
            $string .= '&hellip;';
        }
        return $string;
    }


    static public function ago($tstamp)
    {
        //$tstamp = strtotime($tstamp);
        $seconds = time() - $tstamp;

        $minutes = intval($seconds / 60);
        $seconds = $seconds % 60;

        $hours = intval($minutes / 60);
        $minutes = $minutes % 60;

        $days = intval($hours / 24);
        $hours = $hours % 24;

        $weeks = intval($days / 7);
        $days = $days % 7;

        $months = intval($weeks / 4);
        $weeks = $weeks % 4;

        $years = intval($months / 12);
        $months = $months % 12;

        if ($diff = formatting::since($years, 'year', $months, 'month')) {
        } elseif ($diff = formatting::since($months, 'month', $days, 'day')) {
        } elseif ($diff = formatting::since($weeks, 'week', $days, 'day')) {
        } elseif ($diff = formatting::since($days, 'day', $hours, 'hour')) {
        } elseif ($diff = formatting::since($hours, 'hour', $minutes, 'minute')) {
        } elseif ($diff = formatting::since($minutes, 'minute', $seconds, 'second')) {
        } else {
            $diff = "seconds";
        }
        return $diff . " ago";
    }

    static public function since($sum1, $desc1, $sum2, $desc2)
    {
        if ($sum1 == 1 && $sum2 == 0) {
            $diff = "1 $desc1";
        } elseif ($sum1 == 1 && $sum2 == 1) {
            $diff = "1 $desc1";
        } elseif ($sum1 == 1 && $sum2 > 1) {
            $diff = "1 $desc1";
        } elseif ($sum1 > 1 && $sum2 == 1) {
            $diff = "$sum1 {$desc1}s";
        } elseif ($sum1 > 1 && $sum2 > 1) {
            $diff = "$sum1 {$desc1}s";
        } else {
            return false;
        }
        return $diff;
    }


    static public function number_suffix($number)
    {
        // Also, increasing the range above the condition statements
        // increases efficiency. That's almost 20% of the numbers
        // between 0 and 100 that get to end early.

        // Validate and translate our input
        if (is_numeric($number)) {

            // Get the last two digits (only once)
            $n = $number % 100;

        } else {
            // If the last two characters are numbers
            if (preg_match('/[0-9]?[0-9]$/', $number, $matches)) {

                // Return the last one or two digits
                $n = array_pop($matches);
            } else {

                // Return the string, we can add a suffix to it
                return $number;
            }
        }

        // Skip the switch for as many numbers as possible.
        if ($n > 3 && $n < 21)
            return $number . 'th';

        // Determine the suffix for numbers ending in 1, 2 or 3, otherwise add a 'th'
        switch ($n % 10) {
            case '1':
                return $number . 'st';
            case '2':
                return $number . 'nd';
            case '3':
                return $number . 'rd';
            default:
                return $number . 'th';
        }
    }

    static public function hideExtra($string, $limit, $threshold = 10, $opening = '', $divider = '', $closing = '')
    {
        if (strlen($string) <= $limit) {
            return "{$opening}{$string}{$closing}";
        }

        $opening = ($opening) ? $opening : '<span>';
        $divider = ($divider) ? $divider : '</span> <span class="more">';
        $closing = ($closing) ? $closing : '</span>';

        /* Here we know that we need to shorten the string to $limit.
        * We subtract 2 from the length as we later add the " ..." */
        $limit -= 2;
        $Work = substr($string, 0, $limit);

        // (2) try to find a word boundary at the end to make it look nicer
        $split_pos = strrpos($Work, ' ');
        if ($split_pos >= ($limit - $threshold))
            $Work = substr($Work, 0, $split_pos + 1);

        $Work .= $divider;
        $Work .= substr($string, $split_pos, strlen($string));
        return $opening . $Work . $closing;
    }
}

