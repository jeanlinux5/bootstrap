<?php

namespace RoundSphere;

/**
 * Class Paginator
 *
 * @package RoundSphere
 */
class Paginator
{
    protected $count;
    protected $offset_name;
    protected $limit_name;
    protected $base;

    protected $params = array();

    public function __construct($count = 0, $base_url = null, $offset_name = 'offset', $limit_name = 'limit', $params = null)
    {
        $this->count        = $count;
        $this->offset_name  = $offset_name;
        $this->limit_name   = $limit_name;

        if (!empty($base_url)) {
            $this->base = $base_url;
        } else {
            $scheme = isset($_SERVER['HTTPS'])          ? 'https'                   : 'http';
            $host   = isset($_SERVER['HTTP_HOST'])      ? $_SERVER['HTTP_HOST']     : '';
            $uri    = isset($_SERVER['REDIRECT_URL'])   ? $_SERVER['REDIRECT_URL']  : '/';
            // Get rid of initial slash on URI (we addit below)
            $uri    = preg_replace('#^/#', '', $uri);
            $this->base = "{$scheme}://{$host}/{$uri}";
        }

        if ($params === null) {
            $this->params = $_REQUEST;
        } else {
            $this->params = $params;
        }

        if (!isset($this->params[$offset_name])) {
            $this->params[$offset_name] = 0;
        }
        if (!isset($this->params[$limit_name])) {
            $this->params[$limit_name] = 50;
        }

        $this->offset = $this->params[$offset_name];
        $this->limit  = $this->params[$limit_name];
    }

    protected function buildUrl($params)
    {
        $return = $this->base;
        if ($params) {
            $return .= '?' . http_build_query($params);
        }
        return $return;
    }


    protected function currentOffset()
    {
        return (isset($this->params[$this->offset_name])) ? $this->params[$this->offset_name] : 0;
    }

    protected function currentLimit()
    {
        return (isset($this->params[$this->limit_name])) ? $this->params[$this->limit_name] : 50;
    }

    public function next()
    {
        $params = $this->params;
        if ($this->currentOffset() + $this->currentLimit() < $this->count) {
            $params[$this->offset_name] = $this->currentOffset() + $this->currentLimit();
            return $this->buildUrl($params);
        } else {
            return '';
        }
    }

    public function prev()
    {
        $params = $this->params;

        $current_offset = $params[$this->offset_name];
        if ($current_offset > 0) {
            $prev_offset = $this->currentOffset() - $this->currentLimit();
            if ($prev_offset < 0) {
                $prev_offset = 0;
            }
            $params[$this->offset_name] = $prev_offset;
        } else {
            return '';
        }
        return $this->buildUrl($params);
    }

    public function first()
    {
        $params = $this->params;
        $params[$this->offset_name] = 0;
        return $this->buildUrl($params);
    }

    public function last()
    {
        $last_page = floor(($this->count-1) / $this->currentLimit());
        $last_offset = $this->currentLimit() * $last_page;

        $params = $this->params;
        $params[$this->offset_name] = $last_offset;
        return $this->buildUrl($params);
    }

    public function pages()
    {
        return ceil($this->count / $this->currentLimit());
    }
}
