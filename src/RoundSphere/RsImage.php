<?php

namespace RoundSphere;

use Exception;

/*
    Image manipulations class
    Basicaly an object-orientented wrapper for the PHP GD Image functions
    with a few useful things
*/
class RsImage
{
    protected $source_file;
    protected $image;

    // Class constructor
    public function __construct($file = '')
    {
        // An element from $_FILES was passed in
        if(is_array($file)) {
            $this->source_file = $file['tmp_name'];
            $ext = $this->getExtention($file['name']);

        // A filename was passed in
        } elseif(is_string($file)) {
            $ext = $this->getExtention($file);
            $this->source_file = $file;
        }
        if (!$ext) {
            $ext = 'unknown';
        }
        // bclog("source file is ".$this->source_file);

        switch($ext) {
            case "jpg":
            case "jpeg":
                $this->image = ImageCreateFromJpeg($this->source_file);
                break;
            case "gif":
                $this->image = ImageCreateFromGif($this->source_file);
                break;
            case "png":
                $this->image = ImageCreateFromPng($this->source_file);
                break;
            default:
                try {
                    $this->image = ImageCreateFromString(file_get_contents($this->source_file));
                } catch (\Exception $e) {
                    // bclog("imageCreateFromString() didn't work");
                }
                break;
        }
        return ($this->image) ? true : false;
    }

    public function scaleToWidth($newSx)
    {
        $currSx = imagesx($this->image);
        $currSy = imagesy($this->image);
        if(! ($currSx && $currSy)) {
            // bclog("Unable to get width/height.   Got currSx = $currSx / currSy = $currSy");
            return false;
        }
        $ratio = $currSx / $currSy;
        $newSy = (int) ($newSx / $ratio);
        // bclog("resizing from $currSx/$currSy to $newSx/$newSy");
        return $this->scaleTo($newSx, $newSy);
    }

    public function scaleToHeight($newSy)
    {
        $currSx = imagesx($this->image);
        $currSy = imagesy($this->image);
        if(! ($currSx && $currSy)) {
            // bclog("Unable to get width/height.   Got currSx = $currSx / currSy = $currSy");
            return false;
        }
        $ratio = $currSx / $currSy;
        $newSx = (int) ($newSy * $ratio);
        // bclog("resizing from $currSx/$currSy to $newSx/$newSy");
        return $this->scaleTo($newSx, $newSy);
    }

    public function scaleMax($maxwidth, $maxheight)
    {
        $currSx = imagesx($this->image);
        $currSy = imagesy($this->image);
        // TODO : This doesn't work properly in all cases (ie: if
        // specifying kindof wierd parameters like 100x1000
        //$ratio = ($currSx / $currSy);
        //$maxratio = ($maxwidth / $maxheight);
        //bclog("ratio = $ratio / maxratio = $maxratio");
        if($currSx > $currSy) {
            $this->scaleToWidth($maxwidth);
        } else {
            $this->scaleToHeight($maxheight);
        }
    }

    public function scaleDownMax($maxwidth, $maxheight)
    {
        if ( (imagesx($this->image) > $maxwidth) || imagesy($this->image) > $maxheight) {
            $this->scaleMax($maxwidth, $maxheight);
        }
    }

    public function scaleTo($newSx, $newSy)
    {
        // bclog("resizing to $newSx, $newSy");
        $original = $this->image;
        $this->image = imagecreatetruecolor($newSx, $newSy);
        return ImageCopyResampled($this->image, $original,
            0, 0, 0, 0, $newSx, $newSy, imagesx($original), imagesy($original) );
    }

    public function height()
    {
        return imagesy($this->image);
    }

    public function width()
    {
        return imagesx($this->image);
    }

    public function fileSize()
    {
        return filesize($this->source_file);
    }

    public function save()
    {
        return $this->saveAs($this->source_file);
    }

    public function saveAs($dest_file)
    {
        $ext = $this->getExtention($dest_file);
        switch($ext) {
            case "jpg":
            case "jpeg":
                $rc = imagejpeg($this->image, $dest_file);
                break;
            case "gif":
                $rc = imagegif($this->image, $dest_file);
                break;
            case "png":
                $rc = imagepng($this->image, $dest_file);
                break;
            case "bmp":
                $rc = imagewbmp($this->image, $dest_file);
                break;
            default:
                // bclog("Unknown extention for ".$this->source_file);
                $rc = false;
                break;
        }
        return $rc;
    }

    public function outputAs($type)
    {
        // TODO: don't save to temp file
        $tmpfile = "/tmp/".randomString(40).".$type";
        $this->saveAs($tmpfile);
        switch($type) {
            case 'jpg':
            case 'jpeg':
                $content_type = 'image/jpeg';           break;
            case 'gif':
                $content_type = 'image/gif';            break;
            case 'png':
                $content_type = 'image/x-png';          break;
            case 'bmp':
                $content_type = 'image/image/x-ms-bmp'; break;
        }
        header("Content-type:$content_type");
        readfile($tmpfile);
        unlink($tmpfile);
    }


    public function getExtention($filename)
    {
        $parts = explode('.', $filename);
        return strtolower($parts[count($parts)-1]);
    }

    public function cleanFilename($filename)
    {
        // Windows uploads contains the full path
        if(preg_match("/\\\\/",$filename)) {
            $parts = preg_split('\\\\',$filename);
            $filename = $parts[sizeof($parts)-1];
        }
        $filename = preg_replace("/%20/", "", $filename);
        $fileneme = strtolower($filename);
        $clean_filename = "";
        for($i=0; $i < strlen($filename); $i++) {
            $letter = substr($filename, $i, 1);
            if(preg_match("/[a-zA-Z0-9\.]/",$letter)) {
                $clean_filename .= $letter;
            }
        }
        $clean_filename = stripExtention($clean_filename);
        $clean_filename .= ".jpg";
        return $clean_filename;
    }
}
