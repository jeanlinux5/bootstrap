<?php

namespace RoundSphere;

class Errors
{
    var $errors = array();
    var $errorFormat = '{$ERROR}';

    function add($field, $errmsg = "Please correct this field")
    {
        $this->errors[$field] = $errmsg;
    }


    function ValidEmail($email)
    {
        return preg_match('/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/', $email);
    }


    function requireNonBlank($fields, $errmsg = 'This field is required')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            $field = trim($field);
            if (preg_match("/^(.*)\[(.*)\]$/", $field, $matches)) {
                if (empty($_REQUEST[$matches[1]][$matches[2]])) {
                    $this->errors[$field] = $errmsg;
                }
            } else {
                if (empty($_REQUEST[$field])) {
                    $this->errors[$field] = $errmsg;
                }
            }
        }
    }

    function requireBlank($fields, $errmsg = 'This field must be blank')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if (!empty($_REQUEST[$field])) {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireValidEmail($fields, $errmsg = 'This field requires a valid E-Mail address')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if ((!isset($_REQUEST[$field])) || !$this->ValidEmail($_REQUEST[$field])) {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireValidPhone($fields, $errmsg = 'This field requires a valid phone number')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if (isset($_REQUEST[$field])) {
                $testPhone = preg_replace('/[-\/() \.]/', '', $_REQUEST[$field]);
                if (!(is_numeric($testPhone) && strlen($testPhone) >=10) ) {
                    $this->errors[$field] = $errmsg;
                }
            } else {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireValidZip($fields, $errmsg = 'Please enter a valid zip code')
    {
        $this->requireRegexMatch('/^[0-9\-]{5,9}$/', $fields, $errmsg);
    }

    function requireNumeric($fields, $errmsg = 'This field must be numeric')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if (!(isset($_REQUEST[$field]) && is_numeric($_REQUEST[$field]))) {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireInt($fields, $errmsg = 'This field must be an integer')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if (!(isset($_REQUEST[$field]) && is_int($_REQUEST[$field]))) {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireValidMonth($field, $errmsg = 'Please select a month', $text = false)
    {
        $valid_numeric_values = array(1, 2, 3, 4, 5, 6, 7, 8, 9,
            '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
        $valid_text_values = array(
            'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
        );
        if ($text) {
            $valid_values = array_merge($valid_numeric_values, $valid_text_values);
        } else {
            $valid_values = $valid_numeric_values;
        }
        $value = strtolower(requestValue($field));
        if (!in_array($value, $valid_values)) {
            $this->errors[$field] = $errmsg;
        }
    }


    function requireAlphaNumeric($fields, $errmsg = 'This field must be alphanumeric')
    {
        $this->requireRegexMatch("/^[a-zA-Z0-9]+$/", $fields, $errmsg);
    }

    function requireRegexMatch($regex, $fields, $errmsg = 'Please provide proper data')
    {
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if (!(isset($_REQUEST[$field]) && preg_match($regex, $_REQUEST[$field]))) {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireMatching($field1, $field2, $errormsg = 'These fields must be the same')
    {
        if (isset($_REQUEST[$field1]) && isset($_REQUEST[$field2]) && $_REQUEST[$field1] != $_REQUEST[$field2]) {
            $this->errors[$field2] = $errormsg;
        }
    }

    function requireValidDate($field, $errormsg = 'This field requires a valid date')
    {
        $value = isset($_REQUEST[$field]) ? $_REQUEST[$field] : "";
        if (!preg_match('/^[0-9]{4}-[0-9]{2}\-[0-9]{2}$/', $value)) {
            $this->errors[$field] = $errormsg;
        }
    }

    function requireValidTimestamp($field, $errormsg = 'This field requires a valid timestamp')
    {
        $value = isset($_REQUEST[$field]) ? $_REQUEST[$field] : '';
        if (!preg_match("/^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}[0-9:]+/", $value)) {
            $this->errors[$field] = $errormsg;
        }
    }

    function requireValidIP($fields, $errmsg = 'This field requires a valid IP Address')
    {
        // $regex = "/^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$/";
        $regex = "/\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/";
        if (!is_array($fields)) {
            $fields = explode(',', $fields);
        }
        foreach ($fields as $field) {
            if (!(isset($_REQUEST[$field]) && preg_match($regex, $_REQUEST[$field]))) {
                $this->errors[$field] = $errmsg;
            }
        }
    }

    function requireValidDomain($fields, $errmsg = 'Please enter a valid domain name')
    {
        //return $this->requireRegexMatch("/^([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/", $fields, $errmsg);
        //This didn't work: return $this->requireRegexMatch("/^([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}$/", $fields, $errmsg);
        return $this->requireRegexMatch("/^([a-z0-9\.-])+\.([a-z]{2,6})$/", $fields, $errmsg);
    }

    function requireValidURL($fields, $errmsg = 'Please enter a valid URL')
    {
        return $this->requireRegexMatch("#^https?://[a-z0-9\.-]+\.[a-z]{2,6}/?.*$#i", $fields, $errmsg);
    }

    function requireValidRdns($fields, $errmsg = 'Please enter a valid Reverse DNS')
    {
        return $this->requireRegexMatch("/^[0-9a-z\.\-_]+$/i", $fields, $errmsg);
    }


    function requireValidCC($field, $type, $errmsg = 'Please enter a valid Credit Card Number')
    {
        $ccnum = requestValue($field);
        if (!$this->validCC($type, $ccnum)) {
            $this->errors[$field] = $errmsg;
        }
    }

    function requireWithinMaxLength($field, $maxLength, $errmsg = null)
    {
        $value = requestValue($field);
        if (strlen($value) > $maxLength) {
            if (!$errmsg) {
                $errmsg = "Max length: $maxLength exceeded";
            }
            $this->add($field, $errmsg);
        }
    }

    function requireIntWithinRange($field, $min, $max, $errmsg = null)
    {
        $value = requestValue($field, null);
        if ($value !== null) {
            // is_numeric() allows floats, and is_int() doesn't allow strings
            $valid = is_numeric($value) && ((string)(int)$value === (string)$value);

            if ($valid) {
                $valInt = (int)$value;
                $valid = $valInt <= $max && $valInt >= $min;
            }

            if (!$valid) {
                if (!$errmsg) {
                    $errmsg = "Value not an int within range $min to $max";
                }

                $this->add($field, $errmsg);
            }
        }
    }

    function requireInArray($field, $validValues, $errmsg = null)
    {
        $value = requestValue($field, null);
        if ($value !== null) {
            // in_array non-strict behavior is returning the wrong result
            // possibly related: http://bugs.php.net/bug.php?id=8015%C2%A0
            // workaround is to use strict flag.  $validValues should always be string values
            $valueStr = (string)$value;
            $valid = in_array($valueStr, $validValues, true);

            if (!$valid) {
                if (!$errmsg) {
                    $errmsg = "Invalid value.  Valid values are: " . implode(',', $validValues);
                }
                $this->add($field, $errmsg);
            }
        }
    }


    function ValidCC($type, $ccnum)
    {
        $valid = true;
        switch (strtolower($type)) {
            case 'visa':
            case 'vi':
                if (!preg_match('/^4[0-9]{15}$/', $ccnum)) {
                    $valid = false;
                }
                break;
            case 'mc':
            case 'mastercard';
                if (!preg_match('/^5[1-5][0-9]{14}$/', $ccnum)) {
                    $valid = false;
                }
                break;
            case 'disc':
            case 'discover':
                if (!preg_match('/^6011[0-9]{12}$/', $ccnum)) {
                    $valid = false;
                }
                break;
            case 'amex':
                if (!preg_match('/^3[47][0-9]{13}$/', $ccnum)) {
                    $valid = false;
                }
                break;
            default:
                $valid = false;
        }
        $valid = $valid ? $this->mod10check($ccnum) : false;
        return $valid;

    }

    // Perform a mod10 check (see http://www.beachnet.com/~hstiles/cardtype.html)
    // return true if valid, false if invalid
    function mod10check($number)
    {
        $sum_number = '';
        $loop = 0;
        for ($i = strlen($number) - 1; $i >= 0; $i--) {
            $thisdigit = substr($number, $i, 1);
            $sum_number .= ($loop % 2 == 0) ? $thisdigit : $this->sumdigits($thisdigit * 2);
            $loop++;
        }
        return $this->sumdigits($sum_number) % 10 == 0 ? true : false;
    }

    function sumdigits($number)
    {
        $sum = 0;

        for ($i = 0; $i <= strlen($number) - 1; $i++) {
            $sum += intval(substr($number, $i, 1));
        }

        return $sum;
    }

    function fetch($field)
    {
        if (isset($this->errors[$field])) {
            return str_replace('{$ERROR}', $this->errors[$field], $this->errorFormat);
        }
    }

    function show($field)
    {
        echo $this->fetch($field);
    }

}
