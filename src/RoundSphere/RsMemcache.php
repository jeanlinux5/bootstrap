<?php

namespace RoundSphere;

class RsMemcache extends \Memcache
{
    private $persistant  = true;
    public $expires     = 86400;    // 1 Day
    public $serialize   = false;
    public $name        = '';
    private $compress   = MEMCACHE_COMPRESSED;

    function __construct($params = array())
    {
        $servers = isset($params['servers']) ? $params['servers'] : $GLOBALS['CONFIG']['memcache'];

        foreach ($servers as $server) {
            list($host, $port) = explode(':', $server);
            $this->addServer($host, $port, $this->persistant);
        }

        if (is_string($params)) {
            $params = array('name' => $params);
        }
        $this->setParams($params);
        return true;
    }

    function setParams($params)
    {
        foreach ($params as $name => $value) {
            if (isset($this->$name)) {
                $this->$name = $value;
            }
        }
        return true;
    }

    function setName($name)
    {
        $this->name = $name;
    }

    function setExpires($expires)
    {
        $this->expires = $expires;
    }

    function realkey($key)
    {
        return $this->name.$key;
    }

    function get($key, $recency = '')
    {
        $data = parent::get($this->realkey($key), $this->compress);
        if ($recency) {
            if ($data['created'] > time() - $recency) {
                return $data['data'];
            }  else {
                return null;
            }
        } else {
            return $data['data'];
        }
    }

    function delete($key)
    {
        return parent::delete($this->realkey($key));
    }

    function set($key, $value, $expires = 'BLAHBLAHBLAH')
    {
        $realkey = $this->realkey($key);
        $expires_to_use = ($expires == 'BLAHBLAHBLAH') ? $this->expires : $expires;
        $data = array(
            'created'   => time(),
            'data'      => $value
        );
        return parent::set($realkey, $data, $this->compress, $expires_to_use);
    }
}

