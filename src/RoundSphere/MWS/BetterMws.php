<?php

namespace RoundSphere\MWS;

use Exception;

/*
    Sample usage:

    Looking up an ASIN is very simple:

    $bettermws = new bettermws('your_merchant_id', 'your_marketplace_id', 'your_secret_key');
    $pricing = $bettermws->itemlookup($asin);

*/
class BetterMws
{
    private $merchant_id;
    private $marketplace_id;
    private $secret;

    private $ua = 'BetterMWS-PHP-Class-v0.2';

    private $endpoint = 'http://api.bettermws.com/v1';

    public function __construct($merchant_id, $marketplace_id = null, $secret = null)
    {
        if (is_array($merchant_id)) {
            // If an array of parameters was passed as the first argument
            $this->merchant_id      = $merchant_id['merchant_id'];
            $this->marketplace_id   = $merchant_id['marketplace_id'];
            $this->secret           = $merchant_id['secret'];
        } else {
            $this->merchant_id      = $merchant_id;
            $this->marketplace_id   = $marketplace_id;
            $this->secret           = $secret;
        }
    }

    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function itemLookup($asin, $persist = null, $fresh = null)
    {
        $persist    = (int)$persist;

        $url        = $this->endpoint."/lookup?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}&asin={$asin}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $url       .= $persist ? "&persist={$persist}" : '';
        $url       .= $fresh   ? "fresh"               : '';
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }


    public function exceptions($feed_id)
    {
        $url        = $this->endpoint."/feeds?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}&feed_id={$feed_id}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function feeds()
    {
        $url        = $this->endpoint."/feeds?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function bulkpersist($asins, $persist = null)
    {
        if (!is_array($asins)) {
            throw new Exception('bettermws::bulkpersist() requires first argument as an array');
        }
        $persist = (int)$persist;
        if ($persist < 0 || $persist > 168) {
            $persist = 2;
        }

        $url = $this->endpoint.'/bulkpersist';

        $params = array(
            'merchant_id'       => $this->merchant_id,
            'marketplace_id'    => $this->marketplace_id,
            'persist'           => $persist,
            'ts'                => time(),
            'asins'             => implode(',', $asins),
            'format'            => 'sphp',
        );

        $post_data = http_build_query($params);
        $string_to_sign = $this->secret . $url . '?'. $post_data;
        $signature = md5($string_to_sign);
        $post_data .= "&sig={$signature}";

        $response = $this->sendRequest($url, $post_data);
        return unserialize($response);

    }


    private function sendRequest($url, $post_data = '')
    {
        $ch = curl_init();
        if ($post_data) {
            curl_setopt($ch, CURLOPT_POST,              true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,        $post_data);
        }
        curl_setopt($ch, CURLOPT_URL,               $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,    true);
        // curl_setopt($ch, CURLOPT_HEADER,            true);
        // curl_setopt($ch, CURLINFO_HEADER_OUT,       true);      // for use with debugging
        // curl_setopt($ch, CURLOPT_VERBOSE,           true);      // for use with debugging
        curl_setopt($ch, CURLOPT_TIMEOUT,           30);
        curl_setopt($ch, CURLOPT_USERAGENT,         $this->ua);

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function search($id, $id_type)
    {
        $id_type = strtolower($id_type);
        $timestamp  = strtotime('2016-12-31');

        $signature = md5("{$timestamp}{$this->merchant_id}{$this->secret}");

        $url = "http://research.api.sellerlabs.com/v1/search?{$id_type}={$id}";
        $url .= "&key={$timestamp}|{$this->merchant_id}|{$signature}";

        if (isset($_GET['debug'])) echo "URL: {$url}\n<br />\n";
        $response =  $this->sendRequest($url);
        if (isset($_GET['debug'])) echo "Response: {$response}\n<br />\n";
        return json_decode($response, true);
    }

    public function getMatchingProductForId($id, $id_type)
    {
        $url        = $this->endpoint."/GetMatchingProductForId?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}&id={$id}&type={$id_type}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function getMatchingProduct($asin)
    {
        $url        = $this->endpoint."/GetMatchingProduct?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}&asin={$asin}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function listMatchingProducts($query, $query_context_id)
    {
        $url        = $this->endpoint."/ListMatchingProducts?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}&query={$query}&query_context_id={$query_context_id}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function itemSearch($params)
    {
        $url        = $this->endpoint."/ItemSearch?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}";
        foreach ($params as $name => $value) {
            $url .= "&{$name}=".urlencode($value);
        }
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function product($asin)
    {
        $url        = $this->endpoint."/item?merchant_id={$this->merchant_id}&marketplace_id={$this->marketplace_id}&asin={$asin}";
        $url       .= '&format=sphp';
        $url       .= "&ts=".time();
        $signature  = md5($this->secret . $url);
        $url       .= "&sig={$signature}";
        $response =  $this->sendRequest($url);
        return unserialize($response);
    }

    public function item($asin)
    {
        return $this->product($asin);
    }
}
