<?php

namespace RoundSphere\MWS;

use RoundSphere\Base;

class Product extends Base
{
    protected $data;

    public function __construct($data)
    {
        if (is_string($data)) {
            // Load from BetterMWS??
        } else {
            $this->data = $data;
        }
    }

    public function __get($name)
    {
        return (isset($this->data[$name])) ? $this->data[$name] : parent::__get($name);
    }

    public function asin()
    {
        return isset($this->product['asin']) ? $this->product['asin'] : '';
    }


    public function title()
    {
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['Title'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['Title'];
        }

        return '';
    }

    public function rank()
    {
        foreach ($this->mws_product['ranks'] as $category => $rank) {
            if (!is_numeric($category)) return $rank;
        }

        return isset($this->product['salesrank']) ? $this->product['salesrank'] : '';
    }

    public function image($width = '')
    {
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['SmallImage']['URL'])) {
            $url    = $this->mws_product['attribute_sets']['ItemAttributes']['SmallImage']['URL'];
            if ($width && is_numeric($width)) {
                // $pixels = (int)$width;
                return preg_replace('#_SL[0-9]+#', "SL{$width}", $url);
            }
            return $url;
        }

        return null;
    }

    public function length()
    {
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Length'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Length'];
        }
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Length'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Length'];
        }

        return null;
    }

    public function width()
    {
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Width'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Width'];
        }
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Width'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Width'];
        }

        return null;
    }

    public function height()
    {
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Height'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Height'];
        }
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Height'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Height'];
        }

        return null;
    }

    public function weight()
    {
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Weight'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['PackageDimensions']['Weight'];
        }
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Weight'])) {
             return $this->mws_product['attribute_sets']['ItemAttributes']['ItemDimensions']['Weight'];
        }

        return null;
    }


    public function isOversize()
    {
        // TODO - This no longer makes sense to do at the product level..
        //        It is determeined as the fees_* level instead

        // "Standard-Size includes any packaged Unit that is 20 lbs or less
        // with its longest side 18" or less,
        // its shortest side 8" or less,
        // and its median side 14" or less.
        // A Unit exceeding any of these dimensions is Oversize."
        $length = $this->length();
        $width  = $this->width();
        $height = $this->height();
        $weight = $this->weight();
        if ($weight > 20) return true;
        if ($length > 18) return true;
        if ($width  > 18) return true;
        if ($height > 18) return true;
        if ($length > 8 && $width > 8 && $height >8) return true;
        if (($length + $width + $height) / 3 > 14) return true;
        return false;
    }



    public function recurseCategories($cat = array())
    {
        if ($cat === array()) {
            $cat = $this->data['categories'][0];
        }
        if (isset($cat['parent']) && $cat['parent']['category_name']) {
            return $this->recurseCategories($cat['parent']);
        } else {
            return array($cat['category_id'], $cat['category_name']);
        }
    }

    public function primaryCategory()
    {
        // One of the ProductGroup sections is the most preferrable (ie: "Kitchen"   or "Jewelry")
        if (isset($this->attributes['ProductGroup'])) {
            return $this->attributes['ProductGroup'];
        }
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['ProductGroup'])) {
            return $this->mws_product['attribute_sets']['ItemAttributes']['ProductGroup'];
        }
        // Next preference is ProductTypeName (ie: CAR_AUDIO_OR_THEATER, or HANDBAG
        if (isset($this->attributes['ProductTypeName'])) {
            return $this->attributes['ProductTypeName'];
        }
        if (isset($this->mws_product['attribute_sets']['ItemAttributes']['ProductTypeName'])) {
            return $this->mws_product['attribute_sets']['ItemAttributes']['ProductTypeName'];
        }
        // Last resort, pull from PAAPI category, which should be the same as ProductTypeName
        return (isset($this->product['type'])) ? $this->product['type'] : '';
    }

    public function primaryCategoryId()
    {
        list($category_id, $category_name) =  $this->recurseCategories();
        return $category_id;
    }

    public function primaryCategoryName()
    {
        list($category_id, $category_name) =  $this->recurseCategories();
        return $category_name;
    }

    public function isMedia()
    {
        switch (strtoupper($this->primaryCategory())) {
            case 'BOOK':
            case 'DVD':
            case 'MUSIC':

            // PAAPI Product Names:
            case 'ABIS_BOOK':
            case 'ABIS_DVD':
            case 'ABIS_VIDEO':
            case 'ABIS_MUSIC':
            case 'BOOKS_1973_AND_LATER':
                return true;

            default:
                return false;
        }
    }

    public function cubicFeet()
    {
        return $this->cubicInches() / (12*12*12);
    }

    public function cubicInches()
    {
        return $this->width() * $this->length() * $this->height();
    }

    public function standardShipping()
    {
        switch ($this->primaryCategory()) {
            case 'book':               return 3.99;
            case 'dvd':                return 3.99;
            case 'music':              return 3.98;
            case 'video_games':        return 3.98;
            // Probably more specific categories here.
            // See http://www.amazon.com/gp/help/customer/display.html/ref=hp_lnav_dyn?ie=UTF8&nodeId=1161252
            default:
                return sprintf('%.2f', 4.99 + 0.50 * ceil($item['weight']));
        }
    }

    public function productDescription()
    {
        $html = file_get_contents("http://www.amazon.com/dp/{$this->asin}");
        if (preg_match('#<div class="productDescriptionWrapper" >(.*?)<#is', $html, $matches)) {
            return trim($matches[1]);
        } else {
            return false;
        }
    }
}

