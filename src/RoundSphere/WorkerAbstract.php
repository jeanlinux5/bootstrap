<?php

namespace RoundSphere;
use Monolog;

abstract class WorkerAbstract
{

    public function __construct()
    {
        $this->logger = new Monolog\Logger(get_class($this));
        $logfile = Properties::getProperty('logdir').'/'.get_class($this).".log";
        $this->logger->pushHandler(new Monolog\Handler\StreamHandler($logfile));
    }

    public function logToStdout()
    {
        $this->logger->pushHandler(new Monolog\Handler\StreamHandler('php://stdout'));
    }

    protected function log($level, $message, $context = array())
    {
        // Prefix a PID# to for sorting since multiple processes can write to the same file
        $message = 'P'.str_pad(getmypid(), 5)." {$this->logPrefix} {$message}";
        $this->logger->log($level, $message);
    }

    protected function logDebug($message, $context = array())
    {
        $this->log('DEBUG', $message, $context);
    }
    protected function logInfo($message, $context = array())
    {
        $this->log('INFO', $message, $context);
    }
    protected function logNotice($message, $context = array())
    {
        $this->log('NOTICE', $message, $context);
    }
    protected function logWarning($message, $context = array())
    {
        $this->log('WARNING', $message, $context);
    }
    protected function logError($message, $context = array())
    {
        $this->log('ERROR', $message, $context);
    }
    protected function logCritical($message, $context = array())
    {
        $this->log('CRITICAL', $message, $context);
    }
    protected function logAlert($message, $context = array())
    {
        $this->log('ALERT', $message, $context);
    }
    protected function logEmergency($message, $context = array())
    {
        $this->log('EMERGENCY', $message, $context);
    }

    // Nothing special here yet
    abstract function doWork($message);
}

