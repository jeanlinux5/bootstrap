<?php

namespace RoundSphere;

use Exception;

class Base
{
    protected $_dirty = false;

    protected $_created_auto = true;
    protected $_modified_auto = true;
    protected $_blind_replace = false;

    private $dbh;

    public function __construct($data = null, $dbh = null)
    {
        if ($dbh) {
            $this->dbh =& $dbh;
        }
        if (is_array($data)) {
            $load = false;
            if (isset($this->_db_key_col) && is_array($this->_db_key_col) && count($this->_db_key_col) == count($data)) {
                $needed = count($this->_db_key_col);
                foreach ($this->_db_key_col as $keyi) {
                    if (isset($data[$keyi])) {
                        $needed--;
                    }
                }
                if ($needed == 0) {
                    $load = true;
                }
            }

            if ($load) {
                $this->load($data);
            } else {
                $this->setAll($data);
                // If constructed with an array, then we are always dirty
                $this->_dirty = true;
            }
        } elseif ($this->_db_table && $this->_db_key_col) {
            if ($data) {
                $this->load($data);
            }
        }
    }

    public function load($id)
    {
        $key_col = $this->_db_key_col;
        $connection_name = isset($this->_db_connection_name) ? $this->_db_connection_name : 'app';

        $dbh = ($this->dbh) ? $this->dbh : $dbh = \db::$connection_name();

        if (is_array($key_col)) {
            if ((!is_array($id)) || (count($id) != count($key_col))) {
                throw new Exception('rsbase :: Invalid key specified');
            }
            $sql = "
                SELECT  *
                FROM    `{$this->_db_table}`
                WHERE
            ";
            $params = array();
            foreach ($key_col as $i => $key) {
                if ($i > 0) {
                    $sql .= " AND ";
                }
                $sql .= " `{$key}` = ? ";
                $params[] = isset($id[$key]) ? $id[$key] : '';
            }
            $row = $dbh->getRow($sql, $params);
            if (!$row) {
                $row = $id;
            }
        } else {
            $row = $dbh->getRow("
                SELECT  *
                FROM    `{$this->_db_table}`
                WHERE   `{$key_col}` = ?
            ", array($id));
        }

        if ($row) {
            $this->setAll($row);
        }
        $this->_dirty = false;
    }

    public function loadWithData($data)
    {
        $this->setAll($data);
    }

    public function setAll($data)
    {
        foreach ($data as $name => $value) {
            if (property_exists($this, $name)) {
                // $this->$name = $value;
                $this->__set($name, $value);
            }
        }
    }

    public function __get($name)
    {
        if (method_exists($this, $name)) {
            return $this->$name();
        }
        return (isset($this->$name)) ? $this->$name : null;
    }

    public function __set($name, $value)
    {
        // Allow overriding of __set functionality for specific attributes
        $funcname = "set_{$name}";
        if (method_exists($this, $funcname)) {
            return $this->$funcname($value);
        }
        if (property_exists($this, $name)) {
            if ($this->$name !== $value) {
                if ($this->$name != $value) {
                    $this->_dirty = true;
                } else {
                }
                if (is_string($value)) {
                    $this->$name = trim($value);
                } else {
                    $this->$name = $value;
                }
            } else {
            }
            return true;
        }
        return false;
    }

    public function __isset($name)
    {
        return property_exists($this, $name);
    }

    public function asArray()
    {
        $arr = array();
        $skip_fields = array('_dirty', '_db_key_col', '_db_table', '_db_connection_name');
        foreach ($this as $name => $value) {
            if (in_array($name, $skip_fields)) continue;
            $arr[$name] = $this->__get($name);
        }
        return $arr;
    }

    public function save($dbh = null)
    {
        // Check $_dirty prior to saving (Important for performance)
        if (!$this->_dirty) {
            return true;
        }
        if (!($this->_db_table && $this->_db_key_col)) return false;
        if ($dbh === null) {
            if ($this->dbh) {
                $dbh = $this->dbh;
            } else {
                $connection_name = isset($this->_db_connection_name) ? $this->_db_connection_name : 'app';
                $dbh = \db::$connection_name();
            }
        }
        try {
            $key_col = $this->_db_key_col;

            if (is_array($key_col)) {
                // Complex when $key_col is on more than one column
                $cols = $vals = array();
                $lookup = true;
                foreach ($key_col as $col) {
                    if (empty($this->$col) || $this->_blind_replace) $lookup = false;
                    $cols[] = $col;
                    $vals[] = $this->$col;
                }

                if (!$lookup) {
                    // If any of the $key_cols was empty, then it must not exist in the db
                    $exists = null;
                } else {
                    $sql = " SELECT  * FROM    `{$this->_db_table}` WHERE   ";
                    foreach ($cols as $i => $col) {
                        if ($i != 0) {
                            $sql .= " AND ";
                        }
                        $sql .= "`{$col}` = ? ";
                    }
                    $exists = $dbh->getRow($sql, $vals);
                }

            } else {

                // Simple single-column $key_col
                if (empty($this->$key_col) || $this->_blind_replace) {
                    $exists = null;
                } else {
                    $exists = $dbh->getRow("
                        SELECT  *
                        FROM    `{$this->_db_table}`
                        WHERE   `{$key_col}` = ?
                    ", array($this->$key_col));
                }
            }

            if (!$exists) {
                if (isset($this->describe)) {
                    $rows = $this->describe;
                } else {
                    $rows = $dbh->getAll("
                        DESCRIBE `{$this->_db_table}`
                    ", array(), 'Field');
                }

                $sql = "REPLACE INTO `{$this->_db_table}` SET ";
                $values = array();
                foreach ($rows as $row) {
                    $col = $row['Field'];
                    if ($col == 'modified' && (!empty($this->_modified_auto))) continue;
                    if ($col == 'created' && (!empty($this->_created_auto))) continue;
                    // if (isset($this->$col)) {
                    if ('auto_increment' == $row['Extra'] && null == $this->$col) {
                        // Skip the column to have it auto increment
                    } elseif ('NO' == $row['Null'] || !empty($this->$col)) {
                        $sql .= " `{$col}` = ?,";
                        $values[] = (string)$this->$col;
                    }
                }
                $sql = preg_replace('#,$#', '', $sql);
                $dbh->query($sql, $values);

                // If the $key_col is not an array, is empty, and is auto_increment, then populate
                // it with the lastInsertId()
                if ((!is_array($key_col)) && empty($this->$key_col) && ($key_col == 'id' || $rows[$key_col]['Extra'] == 'auto_increment')) {
                    $this->$key_col = $dbh->conn()->lastInsertId();
                }

            } else {
                $sql = "UPDATE `{$this->_db_table}` SET ";
                foreach ($exists as $col => $old_value) {
                    if ($col == 'modified' && (!empty($this->_modified_auto))) continue;

                    // if (isset($this->$col)) {
                    if (isset($this->$col) || $this->col != $old_value) {
                        if (is_array($key_col)) {
                            $is_key_col = in_array($col, $key_col);
                        } else {
                            $is_key_col = ($col == $key_col);
                        }

                        if (!$is_key_col) {
                            $sql .= " `{$col}` = ?,";
                            $values[] = $this->$col;
                        }
                    }
                }
                $sql = preg_replace('#,$#', '', $sql);

                if (is_array($key_col)) {
                    $sql .= " WHERE ";
                    foreach ($key_col as $i => $col) {
                        if ($i != 0) {
                            $sql .= " AND ";
                        }
                        $sql .= " `{$col}` = ? ";
                        $values[] = $this->$col;
                    }

                } else {
                    $sql .= " WHERE `{$this->_db_key_col}` = ? ";
                    $values[] = $this->$key_col;
                }
                $dbh->query($sql, $values);
            }
            return true;

        } catch (Exception $e) {
            throw $e;
            // echo "oops. Something went wrong!";
            // echo $e->getMessage()."\n";
        }

    }

    public function delete($dbh = null)
    {
        if (!($this->_db_table && $this->_db_key_col)) return false;
        if ($dbh === null) {
            if ($this->dbh) {
                $dbh = $this->dbh;
            } else {
                $connection_name = isset($this->_db_connection_name) ? $this->_db_connection_name : 'app';
                $dbh = \db::$connection_name();
            }
        }
        try {
            $key_col = $this->_db_key_col;

            if (is_array($key_col)) {

                $cols = $vals = [];
                foreach ($key_col as $col) {
                    $cols[] = $col;
                    $vals[] = $this->$col;
                }

                $sql = " DELETE  * FROM `{$this->_db_table}` WHERE   ";
                foreach ($cols as $i => $col) {
                    if ($i != 0) {
                        $sql .= " AND ";
                    }
                    $sql .= "`{$col}` = ? ";
                }
                $sql .= ' LIMIT 1';
                $exists = $dbh->query($sql, $vals);
            } else {
                $exists = $dbh->query("
                    DELETE
                    FROM    `{$this->_db_table}`
                    WHERE   `{$key_col}` = ?
                    LIMIT 1
                ", [$this->$key_col]);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return $this;
    }
}
