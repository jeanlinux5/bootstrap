<?php

require_once dirname(__FILE__).'/3rdparty/class.phpmailer.php';

class mailer extends PHPMailer {

    function __construct()
    {
        global $CONFIG;
        $this->Host     = isset($CONFIG['smtp_host']) ? $CONFIG['smtp_host'] : 'localhost';
        if(isset($CONFIG['mail_reeturn_path'])) {
            $this->Sender   = $CONFIG['mail_return_path'];
        }
        if(isset($CONFIG['smtp_user']) && $CONFIG['smtp_user']) {
            $this->SMTPAuth = true;
            $this->Username = $CONFIG['smtp_user'];
            $this->Password = $CONFIG['smtp_pass'];
        }
    }

    function error_handler($msg)
    {
        bclog("An error occured with the mailer");
        bclog($msg);
    }

    function bcSend($params)
    {
        foreach($params as $name => $value) {
            $$name = $value;
            if(is_string($name) && isset($this->$name)) {
                $this->$name = $value;
            }
        }
        global $CONFIG;

        if(is_array($from)) {
            $from_name  = isset($from['name'])  ? $from['name'] :  $from[0];
            $from_email = isset($from['email']) ? $from['email'] : $from[1];
        } else {
            $from_name  = '';
            $from_email = $from;
        }

        if(is_array($to)) {
            $to_name  = isset($to['name'])  ? $to['name'] :  $to[0];
            $to_email = isset($to['email']) ? $to['email'] : $to[1];
        } else {
            $to_name  = '';
            $to_email = $to;
        }

        if(!empty($params['ccto'])) {
            $this->AddCC($params['ccto']);
        }

        if(!empty($params['bccto'])) {
            $this->AddBCC($params['bccto']);
        }

        $this->Subject  = $subject;

        $this->From     = $from_email;
        $this->FromName = $from_name;
        $this->AddReplyTo($from_email, $from_name);

        $this->Mailer   = 'smtp';
        $this->Timeout  = '30';
        $this->SMTPKeepAlive = false;
        $mailer_custom_bounce_header = 'roundsphere-bounce-id';

        $html_message = isset($html_message) ? $html_message : nl2br($text_message);
        $text_message = isset($text_message) ? $text_message : strip_tags($html_message);

        $this->ClearAddresses();
        $this->ClearAttachments();
        $this->ClearCustomHeaders();
        $this->Body    = $html_message;
        $this->AltBody = $text_message;
        $rand_id = util::randomString('40');
        $this->AddCustomHeader("$mailer_custom_bounce_header:$rand_id");
        $this->AddAddress($to_email, $to_name);

        if($this->Send()) {
            bclog("MailID $rand_id sent successfully to $to_email");
            $status = 'SENT';
        } else {
            bclog("MailID $rand_id failed sending to $to_email");
            bclog("errorInfo = ".$mail->ErrorInfo);
            $status = 'FAILED';
        }
        $this->saveOutgoing($to_email, $rand_id, $status);
    }


    // Insert into the outgoing_emails table if that table exists
    // This is useful for identifying what user an email bounce belongs to
    function saveOutgoing($to_email, $rand_id, $status) {
        return true;    // Disabled for now
        global $db;
        $exists = $db->getOne(" SHOW TABLES LIKE 'outgoing_emails' ");
        if($exists) {
            $result = $db->query("
                INSERT INTO outgoing_emails
                SET     om_id           = 'INSERTID',
                        om_rand_id      = ?,
                        om_to_address   = ?,
                        om_timestamp    = NOW(),
                        om_status       = ?
            ", array($rand_id, $to_email, $status));
        } else {
            bclog("Not saving to database because outgoing_emails table doesn't exist");
        }
    }


}

