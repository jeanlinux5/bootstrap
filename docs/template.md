# RoundBase Templating System

RoundBase provides a primitive templating system for server-side HTML rendering.  This was developed following
some patterns around [Smarty](https://www.smarty.net/), but without a separate templating syntax.

HTML template files are in the `templates/` directory for a project and can be rendered with the
`wrap()` or `display()` methods.  The HTML template files can use PHP extensively for quite a lot of flexibility.


# Typical Usage
Generally, `www/include.php` contains a line like this which instantiates the `$t` template object:

```
$t = new \RoundSphere\Template(\RoundSphere\Properties::loadProperties());
```

Your script will generally "require" the `include.php` and then can make use of the `$t` template object.
You will add variables to the scope of the template, using the `assign()` method. Usage usually looks like:

```
$t->assign([
  'users' => $users,
  'userCount' => count($users),
]);
```

Finally, you can call `wrap()` or `display()` to render a specified template file for the user.


```
$t->wrap('/admin/users/list.html');
```

`templates/admin/users/list.html` would look something like this:

```
<div>
    <h1>List of Users</h1>
    <table class="table">
         <tr>
             <td>
                 Email Address
             </td>
             <td>
                 Last Login
             </td>
         </tr>
         <?php foreach ($users as $user) { ?>
         <tr>
             <td>
                 <?=$user['email'];?>
             </td>
             <td>
                 <?=$user['lastLogin'];?>
             </td>
         </tr>
    </table>
</div>
````
            

                 

# `display()` versus `wrap()`
The method `display()` will render the single template file specified (which could include other template files).
When calling the `wrap()` method, it actually renders the file in `template/layout.html`, which should render a
header, the content file specified, then the footer.

`templates/layout.html` typically looks like this:

```
<?php $t->display('header.html');?>

<?php $t->display($content_file); ?>

<?php $t->display('footer.html');?>
```

This is very useful to provide a common header and footer for most pages on the site.

# Additional Template Functions

## Redirects
The `redirect($destination)` method can be used for sending an HTTP redirect to another page.
If a fully-qualified URL (beginning with http) is provided in `$destination`, then it redirects to that full URL
Any other `$destination` will redirect relative to the `website` variable in `project.properties`.

This is commonly used when submitting forms or performing many actions so that you have clean, meaningful
URLs.

## User-Facing messages
User-Facing messages can be sent to the user using the `$t->addMessage()` and `$t->addError()` methods.
This temporarly stores the messages in a PHP session, and renders them on the next page load.

Common usage may look like this:
```
$newThing = new thing([
  'name' => requestValue('name'),
  'description' => requestValue('description'),
]);
$t->addMessage('Your new thing was created');
  
$newThing->save();
$t->redirect('/things');
```

This needs to be implimented in the HTML templates. We often use the 
[Inspinia theme](https://wrapbootstrap.com/theme/inspinia-responsive-admin-template-WB0R5L90S)
so often, footer.html will contain this:

```
<?php if (!empty($global_messages) || !empty($global_errors)) { // {{{?>
<link href="/theme/css/plugins/toastr.min.css" rel="stylesheet">
<?=$t->display('elements/notifications.phtml');?>
<?php } // }}}?>
```

and `templates/elements/notifications.phtml` contains:
```
<link href="/theme/css/plugins/toastr.min.css" rel="stylesheet">
<script src="/theme/js/plugins/toastr/toastr.min.js"></script>
<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "15000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    <?php if (!empty($global_messages)) {
        foreach ($global_messages as $message) {
            echo 'toastr.info("' . addslashes($message) . '");';
        }
    }

    if (!empty($global_errors)) {
        foreach ($global_errors as $message) {
            echo 'toastr.error("' . addslashes($message) . '");';
        }
    }
    ?>
</script>
```


# HTML Template Helpers Functions
The Template system also contains a number of "helper" functions to assist in rendering HTML forms.  These helper functions
make it easy to generate a form with existing input (ie: when an error occurred), and display errors for specific fields.

## Common Parameters
Many of these functions contain common parameters

| parameter           | Description
|---------------------|----------------------------------
| `name`              | The HTML 'name' of the element. This is the name of the value submitted.
| `id`                | The HTML `id` of the element.  Defaults to the `name` if not provided. Useful if using CSS Selectors on this element
| `class`             | The HTML `class` of the element. 
| `title`             | A "title" to display on the field (ie: "Address Line 1")
| `layout`            | One of `plain`, `2cols`, or `row`. Defaults to `plain`.  When `2cols` or `row` is provided, it wraps the element as if it were elements in a table, with `<td>` and `<tr>` tags.
| `previous`          | If provided, the exact value to use in the existing element.  If not provided, it looks for a `$previous` array in the template's scope, or else in the global `$_REQUEST` array.

## Example, common usage

Controller in `www/theThing.php`

```
require_once __DIR__.'/include.php';

$errors = new \RoundSphere\Errors()
$errors->requireNonBlank(['email']);

if ($errors) {
    $t->addError("Please fix the errors below");
    $t->assign([
        'errors' => $errors->errors,
        'previous' => $_REQUEST,
    ]);
    $t->wrap('theThing/new.html');
    exit;
}
$t->wrap('theThing/new/html');
```

Template file in `templates/theThing/new.html`:

```
<table>
<?=$t->input([
    'name'  => 'email',
    'title' => 'Email Address:',
    'layout' => 'row',
]);
</table>
```

## `$t->input()`
Display an input text box.  Additional parameters include `size` for the width

## `$t->password()`
Same as `input()`, but sets type to "password" so that input is hidden

## `$t->select()`
Used to generate a `<select>` HTML element to select a specific value from a list of provided values. Sample usage:
```
$values = [
   'one'    => '1111111',
   'two'    => '2222222',
   'threes' => '3333333',
];
$t->select([
    'title'       => 'Select a Thing:',
    'name'        => 'myValue'
    'values'      => $values
    'display_col' => '_value',
    'value_col'   => '_key',
]);
```

If an array of arrays is used for `values` (as in the case of a bunch of rows from a database)
`display_col` and `value_col` can also contain names of fields in the database.

For example:

```
$values = \RoundSphere\Database::getAll("
    SELECT id, name
    FROM   theThings
    ORDER BY id ASC
");

$t->select([
    'title'       => 'Select a Thing:',
    'name'        => 'myValue'
    'values'      => $values
    'display_col' => 'name',
    'value_col'   => 'id',
]);
```


  
## `$t->selectmultiple()`
Same as `select()`, but allows the user to select multiple things

## `$t->radio()`
Mostly the same as `select()`, but presented as a series of items with radio buttons next to each

## `$t->textarea()`
Similar to `input()` but rendered as a `<textarea>`.  Additional parameters include `rows` and `cols` for the dimentions

## `$t->checkbox()`
Renders a checkbox with the specified name.

## `$t->hidden()`
Technically, same as `input()` with `type` set to 'hidden' so that it is not displayed on the page, but the value is submitted in the form.


# Some other Useful methods

## `$t->sameUrl(array $overrides)`
Generates a URL the same as the current URL, but overriden by parameters in $overides.
Example from `https://myapp.com/page?search=foobar&sort=desc`

## `$t->pagination(int $currenetPage, int $pageCount, string $method= 'GET')`
Renders a "Page x of Y" with appropriate "Previous" and "Next" links

```
$t->sameUrl([
  'sort' => 'asc'
]);
```

Would return `https://myapp.com/page?search=foobar&sort=asc`. This has been useful within pagination and some filtering scenarios.

  
## `$t->sortUrl()`
Somehow useful in providing 'sort' headers in HTML column headers. Needs more documentation.

# Less Used. Needs more Documentation:
## `$t->showValue()`
## `$t->showOnly()`
## `$t->date()`
## `$t->cycle()`
