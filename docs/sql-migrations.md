# RoundBase Database Migrations

RoundBase has some functionality included to perform Database Migrations.

See the example code in [./examples/migrate.php](./examples/migrate.php) for a working
example of a migration script for per-user databases.  That example should be adaptable
for most projects by deleting the un-necessary code.

# Template Files
Notice that the [Directory Structure](./directory-structure.md) contains an `sql/` directory
for files associated with the project. It typically contains a customized version of
[./examples/migrate.php](./examples/migrate.php), as well as a directory named `master/`
or `template/` which contain a series of `.sql` files for the project.

The `.sql` files should contain a numeric prefix (usually three digits) starting with `000_` 
that ensures that the SQL files are executed in the desired order. The migrate.php script
will execute these SQL files in the intended order. This can be especially useful when there are separate
development and production environments, so that dev can be updated first, the files committed,
then run on production.  A new developer should be able to run migrate.php on a brand new database
server in their local environment and generate a database that is identical to production.

# Rules
Enforcement of some rules will help ensure that multiple databases or environments will not
become out of sync.
 - Test SQL files first on a test table or database to ensure that the syntax is correct, that
   the changes have the desired effect, etc.
 - When adding columns, you often want to specify the order in which the columns occur. Use the
   `ALTER TABLE..ADD...AFTER` syntax to place the new column in the desired location instead of always as the last column in the table.
 - Only one SQL statement should be in each file. This ensures that files are not partially executed, which can cause a mess.
 - Take care to ensure that the ***correct, intended*** `.sql` file is committed to the repository.
 - Once committed and pushed, you should generally NOT change a `.sql` file, because it could have been run in a different environment.
   You should only do this if you can assure that it has been run on no other databases/environments.
 - Especially large tables (with >100M rows) may take a long time to update, so you will want to
   perform DROP, ADD COLUMN, and Change Indexes in a single statement so that the table is only rewritten once.
 
 # When to break the rules
  - Especially large table often have complications that warrant breaking some of the above rules
  - You may add more than one statement to a `.sql` file if you alter a default value, then subsequently UPDATE existing rows to that default value.
  - Foreign Key contstaints can cause some problems that require direct changes outside of this SQL migration functionality.
  
# Ask Brandon
 If you have any questions, feel free to ask Brandon. He's worked with many instances of this can can help point out some best practices.
