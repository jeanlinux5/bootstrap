# Startup script for {PROJECT} on Ubuntu 20.04

# This script utilizes the permissions from the `{PROJECT}-role` role, assumed from the `{PROJECT}-policy` security policy

## Note the script is pulled from S3 when the instance boots. Changes made here need to be uploaded to S3 with the command
# aws s3 cp /root/start.sh s3://{CONFIGS_BUCKET}/{PROJECT}-production/start.sh

echo "EC2 Metadata:"
/usr/bin/ec2metadata

INSTANCEID=`/usr/bin/ec2metadata --instance-id`

## Run sshd on Port 9922, instead of default port 22
echo Port 9922 >> /etc/ssh/sshd_config
/etc/init.d/ssh restart

# Install git and ntp
apt-get -y install git ntp awscli

/etc/init.d/ntp start

# Add Github to global known_hosts
cat <<EOF >> /etc/ssh/ssh_known_hosts
|1|bP3TpkGbV0a+cL3dYQ3mwoVwQGc=|3j0SXn4jKovLE16bOXr1YDHZQhc= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
|1|7DfdQCtOd4yLz07XhgwqhWy4yYI=|Pz5lIQUQ9VVBLb/ZxWZvwnCb+m8= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
|1|TPkONSxO6ZP9SHLxg/0VMGJ5ZFM=|NnxoE9Pk6Gbv0C4837Nokz5NQhc= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
|1|ISkzszjeYOycZD3b8GLE0YFU3y4=|5dxmj59HX4n5hC3r5a3VkaJ7FSk= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
EOF


## kamasgroup will own all application files. This also create the kamasgroup group
useradd -d /dev/null -s /bin/false kamasgroup




echo "Creating Jean's home directory"
## Make jean's home directory
mkdir /home/jeanlinux/
useradd -g kamasgroup -p '$apr1$PyaP.pfl$op3If2LQL38.NXatbi7Yy0' -d /home/jeanlinux -s /bin/bash jeanlinux

cat << EOF > /home/jeanlinux/.bash_aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
EOF

mkdir -p /home/jeanlinux/.ssh
cat <<EOF > /home/jeanlinux/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqOWyPT56+9ySC3Y90YanymdEDTicFPYoXcGM0eG/8RnJ9bZNqxWC2pl6HA9w5inTWzquaEc/5u0eB4KVSiFXDU/hlUqGl5sPp8L6vzebOWNlLfJdhLM9pl6nh7UjA6qJQESYgX9oiI0zX5PJNeobuERFS5YAcqI/TQnG2TbqqQQ0d7ewZHx7JpU/IbIXEeS2hQu3WK2kAIU+ZIfozBjV0EtbLFeM99Vwrd1r9gUC95X6Xtn6sjvZP2jfWowaV1DFVNLwsFCsTvB4Cg92q5tdst+xeWz+ww19Y1dlU1m7jbCUK4/4f1hfGJtAjd6Mgj4rPAzKALZZrP7VdzsYsu/Xb jeanlinux@Jeanlinux-MacBook-Pro.local
EOF

cat <<EOF > /home/jeanlinux/.ssh/known_hosts
EOF

cat <<EOF > /home/jeanlinux/.profile
echo
echo " INITIAL CONNECTION TO THIS INSTANCE"
echo "Pulling down your home directory from git@bitbucket.org:jeanlinux5/homesick.git";
echo
echo
git clone git@bitbucket.org:jeanlinux5/homesick.git /home/jeanlinux/homedir/
git clone git@bitbucket.org:jeanlinux5/workflows.git /websites/workflows/
rsync -taz /home/jeanlinux/homedir/ /home/jeanlinux/
rm -Rf /home/jeanlinux/homedir/
source ~/.bashrc

EOF

chown -R jeanlinux:kamasgroup /home/jeanlinux/

## Add brandon to sudoers
echo "jeanlinux    ALL=(ALL:ALL) ALL" >> /etc/sudoers



export COMPOSER_HOME=/root

## Create some necessary directories
mkdir -p /websites
mkdir -p /websites/conf

# Install some system dependencies
apt-get -y install php7.4 git php-json rsyslog-gnutls ntp php-cli php-zip apache2 awscli

## Create keys needed to 'git pull'
mkdir /root/.ssh/

cat <<EOF > /root/.ssh/id_rsa
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAqjlsj0+evvckgt2PdGGp8pnRA04nBT2KF3BjNHhv/EZyfW2T
asVgtqZehwPcOYp01s6rmhHP+btHgeClUohVw1P4ZVKhpebD6fC+r83mzljZS3yX
YSzPaZep4e1IwOqiUBEmIF/aIiNM1+TyTXqG7hERUuWAHKiP00Jxtk26qkENHe3s
GR8eyaVPyGyFxHktoULt1itpACFPmSH6MwY1dBLWyxXjPfVcK3da/YFAveV+l7Z+
rI72T9o31qMGldQxVTS8LBQrE7weAoPdqubXbLfsXls/sMNfWNXZVNZu42wlCuP+
H9YXxibQI3ejII+KzwMygC2Waz+1Xc7GLLv12wIDAQABAoIBAD7JUYaGuqn7ZKc0
a85km5fVLmgGspPXQBEnLiBTcBLgNSiYezB9vw9/zAwG23sJ29DDiD4a5dhaQKTi
JHiNuvyCA3P0B5W2yXqHhv43dzOTSAr02K1KJvLuswWXTSeDI/1eZe65DC9dT+Gu
G8WCNTSobPhjmF55uhsVPfYdM4XKQLlfdSV2ARUxjHDkHpJMeB4MaSA8jRvdWoes
i8vtPYOlrS6kkJpR/9nSeSy7OaFW6jLVelVh2SzmUB71y8cMMdhIXD/lVk8kugYo
0nVjmbz1slFo1xppwWYy/+30dOkXN5OaA/9niNO9WsRrftDbseVFnQFiIrDs3x3Z
iVTOCTkCgYEA0hUbnarVUAE+sAjH3CXAL8Bi6i3jUD9l4LAcJnotkOu19PrxAH3I
+Jis2LiTJSVZ0iE2gp36qVKJQ1UFNzvGs9684q9G1UaYXFwu7fuCPLgFYeO4+GT8
jfhmv3iAcGczIUUqQ94xVMi6VpD2B6Q9TqQM9pYJSDroUFh58ALhTdUCgYEAz24b
AOOoyWExAlGX2p31rjOvkoDg4TPCaHiVnhZuvs73bbU55PRmBQvIOKzGTjYv/M3E
hf9ZNh34vMmnGbf9PC3ORYb+jozABoqAhP71HlvfKmcHjxiCpmFku1JBUAZktA+W
NjuiHJTKE9Kjdk9RSNWOWb9jHanOdhL/IIvdHO8CgYAofUb4SU+ZftPtK5QRevki
eurVhYiVE9L3zhlu1QdpMfS8xYyjcprERvCxSEmb1CoOlLYj0HzaG303C/DYZ13x
ltFCw1NckaGV03J8DYhZWYI4DqK8xQHj/ucWqgvfHhS9qc3ujKlnVNSNEJ1Kdb4X
JQkWkW4yjJ/vtE20E2oHDQKBgQCgp0BW0fv57EKO7y7TbR/IydMjfpmtEe9yBmKq
y+j980IDQCcUwoz5P4TVEEuOa72C6MNanMg1d8UcQnofGzcQktF9W+5v7WcUKdUc
LHMgDjurlZxvIaaewjPIOQ7Isgrwwlmuq0UFKd/QtoVPrmLgLDZSID7dzJRRMqlS
RcD9FQKBgQC2Vp9SEwgLJl7FUUv+WilvjTOTHCkenfD8Bf/G1+dNeVdeFylQbfMb
lmzXiH2KvaW2tCKfjS4J1OcylTtQYUkZNnsbxqoyAlTxEt7zoiWzB4cTRJx4MTEG
BKjPwPUKxMYKexO2OvH19ZA8T86NT0A+h5ZXq2O0BS6V+00a2Mwruw==
-----END RSA PRIVATE KEY-----
EOF
chmod 400 /root/.ssh/id_rsa

cat <<EOF > /root/.ssh/id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqOWyPT56+9ySC3Y90YanymdEDTicFPYoXcGM0eG/8RnJ9bZNqxWC2pl6HA9w5inTWzquaEc/5u0eB4KVSiFXDU/hlUqGl5sPp8L6vzebOWNlLfJdhLM9pl6nh7UjA6qJQESYgX9oiI0zX5PJNeobuERFS5YAcqI/TQnG2TbqqQQ0d7ewZHx7JpU/IbIXEeS2hQu3WK2kAIU+ZIfozBjV0EtbLFeM99Vwrd1r9gUC95X6Xtn6sjvZP2jfWowaV1DFVNLwsFCsTvB4Cg92q5tdst+xeWz+ww19Y1dlU1m7jbCUK4/4f1hfGJtAjd6Mgj4rPAzKALZZrP7VdzsYsu/Xb jeanlinux@Jeanlinux-MacBook-Pro.local
EOF

cat <<EOF > /root/.ssh/id_rsa.kamasgroup
-----BEGIN RSA PRIVATE KEY-----
EOF
chmod 400 /root/.ssh/id_rsa.kamasgroup

cat <<EOF > /root/.ssh/id_rsa.kamasgroup.pub
EOF



## Add github to known_hosts to avoid some errors
cat <<EOF > /root/.ssh/known_hosts
|1|bP3TpkGbV0a+cL3dYQ3mwoVwQGc=|3j0SXn4jKovLE16bOXr1YDHZQhc= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
|1|7DfdQCtOd4yLz07XhgwqhWy4yYI=|Pz5lIQUQ9VVBLb/ZxWZvwnCb+m8= ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAubiN81eDcafrgMeLzaFPsw2kNvEcqTKl/VqLat/MaB33pZy0y3rJZtnqwR2qOOvbwKZYKiEO1O6VqNEBxKvJJelCq0dTXWT5pbO2gDXC6h6QDXCaHo6pOHGPUy+YBaGQRGuSusMEASYiWunYN0vCAI8QaXnWMXNMdFP3jHAJH0eDsoiGnLPBlBp4TNm6rYI74nMzgz3B9IikW4WVK+dc8KZJZWYjAuORU3jc1c/NPskD2ASinf8v3xnfXeukU0sJ5N6m5E8VLjObPEO+mN2t/FZTMZLiFqPWc/ALSqnMnnhwrNi2rbfg/rd/IpL8Le3pSBne8+seeFVBoGqzHM9yXw==
EOF

## On EC2, we use a separate EBS Block device for logs, so need to mount it
mkfs -t ext4 /dev/xvdb
mount /dev/xvdb /mnt/

## On EC2, we put our logs on /mnt which has plenty of space and goes away when the instance terminates
mkdir /mnt/tmp
chown www-data:users /mnt/tmp
chmod 755 /mnt/tmp
ln -s /mnt/tmp /websites/tmp

mkdir /mnt/applogs
chown www-data:users /mnt/applogs
chmod 755 /mnt/applogs
ln -s /mnt/applogs /websites/applogs

mkdir /mnt/apachelogs
ln -s /mnt/apachelogs /websites/logs
chown www-data:users /mnt/apachelogs
chmod 755 /mnt/apachelogs

## Clone our repository
git clone git@bitbucket.org:jeanlinux5/bootstrap.git /websites/bootstrap/

cd /websites/workflows/

## This should install php and other significant things
apt-get install -y $(cat deploy/etc/depend.ubuntu)

## Install composer
export HOME=/root
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

## Download our config file

#aws s3 cp s3://{CONFIGS_BUCKET}/{PROJECT}-production/project.properties /websites/{ROOT_DIR}/conf/project.properties

## Now that we've checked out the task.run code, we have to swap the ssh key
## to the 'roundbase' key so that composer can install roundbase
## (This is due to a GitHub limitiation which allows a deploy key to be used
##   on only one repository)
#
#mv /root/.ssh/id_rsa /root/.ssh/id_rsa.workflows
#mv /root/.ssh/id_rsa.pub /root/.ssh/id_rsa.workflows.pub
#
#cp /root/.ssh/id_rsa.kamasgroup /root/.ssh/id_rsa
#cp /root/.ssh/id_rsa.kamasgroup.pub /root/.ssh/id_rsa.pub

## Finally run the applications composer install

cd /websites/workflows/workflows

## Install project's composer dependencies
COMPOSER_HOME="/root/" composer --no-plugins --no-scripts -n install

## chown everything to brandon:users
find /websites -user root -exec chown jeanlinux:users {} \;
