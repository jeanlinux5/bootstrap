<?php
// Example of a common `app/common.php` file that initializes the RoundBase framework.

session_name('{PROJECT}-phpsessid');
ini_set('session.gc_maxlifetime', 86400 * 7);

date_default_timezone_set('UTC');

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');

function DebugVar($var)
{
    echo "<pre>\n";
    print_r($var);
    echo "\n</pre>\n";
}

function requestValue($name, $default = '')
{
    // Correctly handle where $name = 'foo[bar]'
    if (preg_match('#(.*)\[(.*)\]#', $name, $matches)) {
        return isset($_REQUEST[$matches[1]][$matches[2]]) ? $_REQUEST[$matches[1]][$matches[2]] : $default;
    }
    return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
}

set_include_path(
    get_include_path().PATH_SEPARATOR.
    __DIR__.'/classes/'.PATH_SEPARATOR
);

// For some reason, this machine has '.' in the current path
set_include_path (preg_replace('#^\.:#', '', get_include_path()));

spl_autoload_register('rs_autoload');

function rs_autoload($className)
{
    $filePath = str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    $includePaths = explode(PATH_SEPARATOR, get_include_path());
    foreach ($includePaths as $includePath){
        if (file_exists($includePath . DIRECTORY_SEPARATOR . $filePath)){
            require_once $filePath;
            return;
        }

        $classSyntaxPath = "{$className}.class.php";
        if (file_exists($includePath . DIRECTORY_SEPARATOR . $classSyntaxPath)) {
            require_once $classSyntaxPath;
            return;
        }
    }
}

// Load up Composer autoload file
if (file_exists(__DIR__.'/../vendor/autoload.php')) {
    require_once __DIR__.'/../vendor/autoload.php';
}

\RoundSphere\Properties::setConfigFile(__DIR__.'/project.properties');

$bugsnag = null;
$bugsnagConfig = \RoundSphere\Properties::getProperty('bugsnag');
if ($bugsnagConfig) {
    $bugsnag = Bugsnag\Client::make($bugsnagConfig['apiKey']);
    if (isset($_SERVER['WINDOW'])) {
        echo "Not registering BugSnag handler since running in window {$_SERVER['WINDOW']}\n";
    } else {
        Bugsnag\Handler::registerWithPrevious($bugsnag);
    }
}

$redisConfig = \RoundSphere\Properties::getProperty('redis');
if ($redisConfig) {
    ini_set('session.save_handler', 'redis');
    ini_set('session.save_path', "tcp://{$redisConfig['host']}:{$redisConfig['port']}");
}


function bclog($msg)
{
    $logfile = \RoundSphere\Properties::getProperty('logfile');
    $fh = @fopen($logfile, 'a');
    if ($fh) {
        fwrite($fh, date('Y-m-d H:i:s')." {$msg}\n");
        fclose($fh);
    }
}
