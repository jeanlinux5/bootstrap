<?php
require_once __DIR__.'/../app/common.php';

/*
USAGE:
  To run migrations for a single account:
   # php migrate.php <accountId>

  To run migrations for all accounts on a specified database server
    (useful for running migrate.php simultaneously, one for each
        db_host so as not to overload a single server)
   # php migrate.php --host=<db_host>

  You can use the command-line argument --assume-yes as the final argument
    to bypass the interactive prompt (use with care!)
*/

echo "\n\n\n";
echo "================================================================\n";
echo "  THIS IS REALLY POWERFUL. MAKE SURE YOU KNOW WHAT YOU ARE DOING\n";
echo "================================================================\n";
echo "\n\n\n";

$options = getopt('', [ 'account::', 'host::', 'template', 'skip-template', 'master', 'assume-yes' ]);

$yes_to_all = false;
if (isset($options['assume-yes'])) {
    $yes_to_all = true;
}

echo "This will run database migrations against the various customer databases\n\n";
echo "USAGE: {$argv[0]} [--account=<accountId>]\n";
echo "                 [--host=<db_host>] [--template] [--master] [--assume-yes]\n\n";
echo "  --account         If specified, migration will only be attempted for the specified account\n";
echo "                    datbase. Useful for testing\n\n";
echo "  --host            Runs migrations for all account databases on the specified host.  May be\n";
echo "                    useful for running multiple migrations simultaneously. One execution for\n";
echo "                    each database server\n\n";
echo "  --master          Migrations will be run only on the is the master database\n";
echo "                    where all account/user information is stored\n\n";
echo "  --template        Runs migrations on template database only\n";
echo "                    This database is copied for new accounts\n";
echo "  --skip-template   Skip migrating the template database\n\n";
echo "  --assume-yes      Bypasses asking the user to enter YES to confirm each individual database\n\n";

$environment = 'master_db';

$accountId = '';
if (!empty($options['account']) && is_numeric($options['account'])) {
    $accountId = $options['account'];
}

$migrateHost = '';
if (!empty($options['host'])) {
    $migrateHost = $options['host'];
}

$errors = [];

// Look for any migrations on the master database
if (empty($accountId) || isset($options['master'])) {
    try {
        $master_config = \RoundSphere\Properties::getProperty($environment);

        $migrate = new \RoundSphere\DBMigrate();
        $migrate->properties = [
            'db_name'       => $master_config['name'],
            'db_user'       => $master_config['user'],
            'db_pass'       => $master_config['pass'],
            'db_host'       => $master_config['host'],
            'db_port'       => $master_config['port'],
        ];

        $migrate->sql_file_dir = __DIR__.'/master/';
        echo "\n\n\n";
        echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo "  Master database\n";
        echo "\n\n";

        $preview = $migrate->migrationsToRun();
        if (!$preview) {
            echo "\n";
            echo "  No DB Migrations to run on master database\n";
            echo "----------------------------------------------------------------\n";
            echo "\n\n\n";
        } else {
            echo "\n\nThe Following SQL files will be run:\n";
            foreach ($preview as $file) {
                echo "    {$file}\n";
            }

            if ($yes_to_all) {
                $yes = 'YES';
            } else {
                echo "\n\nEnter YES to continue: ";
                $handle = fopen ("php://stdin","r");
                $yes = trim(fgets($handle));
            }
            if ($yes == 'YES TO ALL') {
                $yes_to_all = true;
                $yes = 'YES';
            }
            if ($yes != 'YES') {
                echo "EXITING.  MIGRATIONS WERE NOT RUN!\n";
                exit;
            }

            echo "\n";

            // set command line to use when running sql scripts.  Use {variable} notation to substitute in properties
            $migrate->setDbCommandLine("mysql --user='{db_user}' --password='{db_pass}' --host='{db_host}' --port='{db_port}' {db_name}");
            // directory containing your SQL files.  files should be named in numeric order.  e.g.:
            //    001_create_schema.sql
            //    002_add_foo_table.sql
            //    003_add_column_to_table.sql
            //
            // run the migration.  this will create the "db_migrate" table if not found, and will then
            // run all migration files that have not yet been run against this environment
            $migrate->migrate();
            echo "  All Done\n";
            echo "----------------------------------------------------------------\n\n\n";
        }
    } catch (Exception $e) {
        echo "Master database EXCEPTION: ".$e->getMessage()."\n";
        $errors["{$master_config['db_name']}@{$master_config['db_host']}"] = $e->getMessage();
    }
}

if ((!$accountId || isset($options['template'])) && (!isset($options['skip-template']))) {
    // Now look for any migrations to run on the template database
    $config = \RoundSphere\Properties::getProperty($environment);
    $masterDb = new \RoundSphere\Database($config);

    $template = $config;
    $template['name'] =  'template';

    try {
        echo "migrating template {$template['host']}\n";

        $migrate = new \RoundSphere\DBMigrate();
        $migrate->properties = [
            'db_name'       => $template['name'],
            'db_user'       => $template['user'],
            'db_pass'       => $template['pass'],
            'db_host'       => $template['host'],
            'db_port'       => $template['port'],
        ];

        $migrate->sql_file_dir = __DIR__.'/template/';

        echo "\n\n\n";
        echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
        echo "  'template' database on {$template['host']}\n";
        echo "\n\n";

        $preview = $migrate->migrationsToRun();
        if (!$preview) {
            echo "\n\n";
            echo "  No DB Migrations to run on 'template' database\n";
            echo "----------------------------------------------------------------\n\n\n";
            echo "\n\n\n";
        } else {
            echo "\n\nThe Following SQL files will be run:\n";
            foreach ($preview as $file) {
            echo "    {$file}\n";
            }

            if ($yes_to_all) {
                $yes = 'YES';
            } else {
                echo "\n\nEnter YES to continue: ";
                $handle = fopen ("php://stdin","r");
                $yes = trim(fgets($handle));
            }
            if ($yes == 'YES TO ALL') {
                $yes_to_all = true;
                $yes = 'YES';
            }
            if ($yes != 'YES') {
                echo "EXITING.  MIGRATIONS WERE NOT RUN!\n";
                exit;
            }
            echo "\n";

            // set command line to use when running sql scripts.  Use {variable} notation to substitute in properties
            $migrate->setDbCommandLine("mysql --user='{db_user}' --password='{db_pass}' --host='{db_host}' --port='{db_port}' {db_name}");
            // directory containing your SQL files.  files should be named in numeric order.  e.g.:
            //    001_create_schema.sql
            //    002_add_foo_table.sql
            //    003_add_column_to_table.sql
            //
            // run the migration.  this will create the "db_migrate" table if not found, and will then
            // run all migration files that have not yet been run against this environment
            $migrate->migrate();
            echo "  All done with migrations\n";
            echo "----------------------------------------------------------------\n\n\n";
        }
    } catch (Exception $e) {
        echo "EXCEPTION: ".$e->getMessage()."\n";
        $errors["{$template['db_name']}@{$template['db_host']}"] = $e->getMessage();
    }
}


if (isset($options['master']) || isset($options['template'])) {
    // Don't do user databases if --master or --template was specified
} else {
    // Now look through any account databases

    $sqlWhere = '';
    $sqlParams = [];
    $sqlWhere = '';
    $sqlParams = [];

    if (!empty($accountId)) {
        $sqlWhere .= ' AND accountId = :account ';
        $sqlParams['id'] = $accountId;
    }

    if (!empty($migrateHost)) {
        $sqlWhere .= ' AND db_host = :db_host ';
        $sqlParams['db_host'] = $migrateHost;
    }

    $config = \RoundSphere\Properties::getProperty($environment);
    $masterDb = new \RoundSphere\Database($config);

    $accounts = $masterDb->getAll("
        SELECT  *
        FROM    accounts
        WHERE   1
        {$sqlWhere}
        ORDER BY RAND()
    ", $sqlParams);

    $total = count($accounts);
    $i = 1;

    foreach ($accounts as $account) {

        // Now look for any migrations to run on the template database
        try {

            $migrate = new \RoundSphere\DBMigrate();
            $migrate->properties = [
                'db_name'       => $account['db_name'],
                'db_user'       => $account['db_user'],
                'db_pass'       => $account['db_pass'],
                'db_host'       => $account['db_host'],
                'db_port'       => $account['db_port'],
            ];

            $migrate->sql_file_dir = __DIR__.'/template/';

            echo "\n\n\n";
            echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
            echo "[{$i}/{$total}] Account Database for accountId {$account['id']} ({$account['db_name']}) on {$account['db_host']}\n";
            echo "\n\n";
            $i++;

            $preview = $migrate->migrationsToRun();
            if (!$preview) {
                echo "\n\n";
                echo "  No DB Migrations to run on '{$account['db_name']}' database\n";
                echo "----------------------------------------------------------------\n\n\n";
                echo "\n\n\n";
            } else {
                $size = $migrate->databaseSize();
                $mb = round(($size / 1024 / 1024), 1);
                echo "\n\nThe Following SQL files will be run (database size=".number_format($mb)."Mb):\n";
                foreach ($preview as $file) {
                    echo "    {$file}\n";
                }
                if ($yes_to_all) {
                    $yes = 'YES';
                } else {
                    echo "\n\nEnter YES to continue: ";
                    $handle = fopen ("php://stdin","r");
                    $yes = trim(fgets($handle));
                }
                if ($yes == 'YES TO ALL') {
                    $yes_to_all = true;
                    $yes = 'YES';
                }
                if ($yes != 'YES') {
                    echo "EXITING.  MIGRATIONS WERE NOT RUN!\n";
                    exit;
                }
                echo "\n";

                // set command line to use when running sql scripts.  Use {variable} notation to substitute in properties
                $migrate->setDbCommandLine("mysql --user='{db_user}' --password='{db_pass}' --host='{db_host}' --port='{db_port}' {db_name}");

                $migrate->migrate();
                echo "  All done with migrations\n";
                echo "----------------------------------------------------------------\n\n\n";
            }
        } catch (Exception $e) {
            echo "EXCEPTION: ".$e->getMessage()."\n";
            $errors["{$account['db_name']}@{$account['db_host']}"] = $e->getMessage();
        }
    }
}

echo "All Done\n\n";
if ($errors) {
    echo count($errors) . " ERRORS FOUND\n";
    foreach ($errors as $db => $errorMsg) {
        echo "{$db}  => {$errorMsg}\n";
    }
    echo "\n\n";
}
