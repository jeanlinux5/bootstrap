# RoundBase Workers

Workers are, quite simply, programs that do "work" in the background. They don't directly have a user-interface
and are generally triggered by retrieving a job from a queuing system like SQS, Step Functions, or Redis. 
Jobs may be put into those queues via a cron task, a user clicking something in a UI, or from some other process.

Workers will often gather data from external resources and put data into a local database. They may summarize
information and send emails. They could perform daily operations like checking billing and refreshing tokens.

Not all projects have any worker process.  Some projects are all-back end and run exclusively via the worker system.

# Relevant Files and Directories
The `bin/` directory contains several notable files.

| File                  | Purpose
|-----------------------|-----------------
| `bin/daemon.php`      | Long-running daemon process that starts and runs many worker jobs simultaneously
| `bin/runManual.php`   | Script to run a single job from the command-line.  Run without parameters to see usage instructions
| `bin/runonce.php`     | Run a single job of the specified type from the job queue
| `bin/runonly.php`     | Run jobs continuously from the job queue of a single type, in the foreground. `runonly.php` can also be used to run the jobs inside a container.
| `bin/workers/`        | This directory contains all of the running "worker" job definitions.
| `etc/worker.ini`      | Configuration file for the daemon process


# `bin/daemon.php`
This process is responsible for running multiple PHP process simultaneously (which is not a trivial task), and for retrieving a
job from the queueing system and assigning it to a worker that will perform the work.

Since the queueing service is different on each project, only an example is provided in this repo in
[./examples/daemon-step-functions.php](./examples/daemon-step-functions.php).  This example uses AWS Step Functions as the
queuing system and can be adapted for SQS or other systems.  This file should be copied to `bin/daemon.php` in the project
and customized as needed for the desired queuing system and directory.

# Worker Code
Workers will typically extend the `\RoundSphere\WorkerAbstract` class which provides logging and some very basic functionality.
You must override the `doWork()` function for any extended classes that will receive a message from the queuing system
and perform the relevant work.

A simple worker that does nothing useful would look like this:

```
<?php

class test extends \RoundSphere\WorkerAbstract
{
    protected $activityArn = 'arn:aws:states:us-east-1:722537357562:activity:test';

    public function doWork($input)
    {
        $this->logInfo("test starting with {$input['executionName']}");
        $this->logError("This is an error");
    }
}
```

Workers will often validate the provided `$input` to confirm that the necessary inputs are provided. They will then
perform whatever work is needed.  The `doWork()` function should return true if the job is complete
(either successfully or failed) and should be removed from the queue.  It can return false to indicate that the
work is not complete and should be retried by the queuing system.

## Extension of WorkerAbstract
Many of our projects extend the `WorkerAbstract` class to provide their own initialization or other objects available to
the worker.  Some of these extensions should probably make their way back into the RoundBase class to be useful to other
projects.

# Logs
Workers will log an output file for each type of worker in `logdir` from the configuration file.   Logs contain
a Process ID (PID) to be able to filter out a specific process.
