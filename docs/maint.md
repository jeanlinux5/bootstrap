# RoundBase Maintenance Files

Most projects include a `maint/` directory that is intended to be for scripts that are used
by technical staff to perform maintenence tasks.

Maintenance tasks may be something like synchronizing a list of email addresses to an external
email provider. Or running a monthly process to calculate overall usage.

If a script is inteneded only for one-time use, it generally **should not** be committed
to the repository. An exception may be made if it contains code that would be hard to reproduce
and may be useful in the future.
