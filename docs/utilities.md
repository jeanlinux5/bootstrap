# RoundBase Utilities

Several functions come up again and again and are just put into the `\RoundSphere\Util` class to make them
available to many projects

In no particular order:


# validEmail($email) 
Performs a standard regex match on `$email` to see if it 'looks' like a valid email address. Returns true/false

# randomString($size, $charset)
Returns a random string of `$size` length. The default alpha-numeric character set can be override with a string
containing all available characters.

# remoteIP()
Returns the IP Address of a remote user, taking into account proxy servers and many things.

# timed_file($filename, $timeout, $lines)
A version of PHP's native `file()` function,w ith a timeout.

# shortId($int)` and `fromShortId($shortId)
Converts integers (which could contain many digits) into shorter string ID's and back.

# removeOutliers($array, $width = 1)
Remove values outside the specified stddev from an array

# lowestInArray($array)
Returns the lowest number in the array

# array_combine_expand($a, $b, $pad = TRUE)
Similar to PHP's native [array_combine](https://www.php.net/array_combine) function,
but gracefully handles arrays of mixed lengths

# function expma($in)
Exponentional Moving Average 

# avgTopFromArray($array, $number)
Average Top from Array

# roundToQuarter($amount)
Rounds an amount to the nearest quarter

# addMonth($date_str, $months = 1)`
Adds a month to a provided dat

# mwsTimestamp($timestamp)
Returns timestamp in MWS'ss preferred Date format

# timezoneList($usOnly = false)
Returns a list of US Time Zones

# strtotimeUtc($string)
Returns time in UTC

# md5_base64($data)
md5 of base64 encoded data

# ago($tstamp)
Provides a sensible string describing how long ago something occurred ("seconds ago", or "3 months ago"), depending on the age

# formatSince($sum1, $desc1, $sum2, $desc2)
Umm, dunno.

# array_to_xml($data, &$xmlobj)
Convert a PHP Array to an XML object

# mb_unserialize($string)
Unserialize a multi-byte string.  I guess?

# uniqueId()
Returns an string suitable for use as a Unique ID (current timestamp + 4 random characters)

# obfuscatedEmailAddress($email)
Obfusecate an email address with HTML entities

# obfuscatedEmailLink($email, $params = array())
Provides a `mailto:` email link that is obfusecated

# mandrillMail($recipient, $subject, $message, $from = null)
Sends a message via Mandrill. Requires configured `mandrillApiKey` in `project.properties`


# blankPng()
Returns raw bytes for a blank `.png` file

# sendBlankPng()
Sends a blank `.png` file, including `Content-type` header

# blankGif()
Returns raw bytes for a blank `.gif` file

# sendBlankGif()
Send a blank `.gif` file, including `Content-Type` header


# A bunch of sorting/hashing/array funnctions:

 - `static function hashByField($things, $field)Iterate through `$things`. Return a new array of things, with an index by `$field``
 - `static function sortObjectByField(&$array, $field, $direction = SORT_ASC, $second_field = '', $second_direction = SORT_ASC)`
 - `static function sortHashByField(&$hash, $field, $direction = SORT_ASC, $second_field = '', $second_direction = SORT_ASC)`
 - `static function orderBy(&$data, $field, $sortdir = 'ASC')`
 - `static function orderByArray(&$data, $field, $sortdir = 'ASC')`
 - `static function in_iarray($str, $a)`
 - `static function array_iunique($a)`
 - `static function array_index($arr, $needle)`
 - `static function sortHashOfObjectsByField(&$hash, $field, $direction = SORT_ASC, $second_field = '', $second_direction = SORT_ASC)`
