# RoundBase Object Relational Model (ORM)

RoundBase's Object Relational Model was created around the time that other PHP ORMs were being created.
So the term "Object Relational Model" was not well understood.  It is referred to internally as `Base.php` or `\RoundSphere\Base`

This ORM may be a bit archaic and primitive by modern PHP standards. But it usually works pretty well, is simpler and more
performant than many other big projects, so we continue to use it until there seems to be a good reason to change it.

# Overview
In general, the `\RoundSphere\Base` class allows a convenient mapping to an underlying SQL database table.
The columns are defined in the ORM model file, usually saved inside `app/classes`. Common functionality like `$obj->save()`
will figure out out the propert SQL `INSERT`/`UPDATE` commands to save an object to the database.

# Basic Usage

The example below is a minimum class to map to a typical `users` database table:

```
<?php
/*
     User class
*/

class user extends \RoundSphere\Base
{
    protected $_db_table    = 'users';
    protected $_db_key_col  = 'id';

    protected $id;
    protected $accountId;
    protected $email;
    protected $created;
    protected $lastLogin;
}
```

You can then do things like  `$user = new user(1);` to create a new `$user` object,
then perhaps update `$user->lastLogin` to the current timestamp, and finally call `$user->save()` to persist the changes to the database.

Often, we'll build out much more functionality inside the `user` class to do other things as well. The "Base" is intended to be just a
starting point with reasonable defaults, upon which more funcationality can be added.

For example, with a User class, we may want to create a `setPassword()` method that properly hashes and updates the password in a database,
but since $password is not defined in the model definition, it is not accessible by other objects.

# Getters and Setters and Magic Methods
Although the individual class properties are declared `protected`, what would usually mean that you can get or set them directly.
This Base class uses the [magic PHP methods](https://www.php.net/manual/en/language.oop5.magic.php) `__get` and `__set` to make that work as
you would expect.

You can override the basic behaviour of `__get` and `__set` for specific columns, by creating a method named `set_{columnName}($value)` with the desired behavior.

# Specific Properties

## `$_db_table`
The `$_db_table` property refers to the underlying database table where these objects will be stored

## `$_db_key_col`
The protected property `$_db_key_col` refers to the column or columns that uniquely identify a row in the table.
Care is taken to support multi-key uniqueness, so this could be an array of columns, like `['accountId', 'userId']`

## `$_db_connection_name`
If `$_db_connection_name` is not defined, id defaults to `app`.  This calls `db::app()` when trying to read or write to the database.
If a different database connection is needed, you can set `$_db_connection_name` to a different value to have it read/write to
that connection instead of `db::app()`

# `asArray()` method
Sometimes you want to have a representation of the object as an array.  Call the `asArray()` method on an object to return the object as an array, without any of the special fields used internally by the class.

# Per-User Databases
In a per-data user project, you can pass the database handle of the desired database as an optional second argument to the constructor.
For example `$report = new report($someStuff, $account->db())`.
`$report` can then be `$report->save()`d to save the row to the proper user's database.

# Optimizations
The class attempts avoid hitting the database when `save()` has been called, but nothing has changed to the values. It does this by setting `$_dirty` to true if a value called via the `__set()` method has been changed. This has a big performance improvement, especially when iterating through many rows.  This will sometimes leads to unintended consequences though when setting properties in a derived class. If this ever comes up, trying calling `$this->__set($column, $value)` instead of `$this->column = $value` inside derived classes.
