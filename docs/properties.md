# RoundBase Configuration Directives (Properties)

# Overview
The RoundBase library provides a flexible way to access configuration parameters such as database server credientials,
access keys, and other application-level configuration values. This works well with the [AWS Provisioning Pattern](./aws-provisioning.md)
as the `project.properties` file can be retrieved during server deployment from S3, and values can remain a secret.

# project.properties configuration files
Configuration files follow the format below

```
## Comments beginning with hash marks are ignored
db.host                = somehostname.somerandomchars.us-east-1.rds.amazonaws.com
db.port                = 3306
db.user                = theproject
db.pass                = xLxdGrdGbcDb6bvVvWGJHXu9
db.name                = theproject

encrypt.encryption_key = MMfUVSFQ4dEDMMhXZeJzXLFWMMfUVSFQ4dEDMMhXZeJzXLFW

mandrillApiKey         = xvNNpfDptaKCRfXmvbysEGsM

someService.clientId   = S9JQq3fXSPArrXtaK8ShqWfp
someService.secret     = 3cw2XmQJAGsa97xh43DD6xKt
```

# Initialization
Typically, `app/common.php` intitializes the configuration file location with the line below.
This points to the "committed" config file, and it figures out the location of the "uncommitted" version relative to that path.

```
\RoundSphere\Properties::setConfigFile(__DIR__.'/project.properties');
```



# Usage
After initialization, you can retrieve values from the config file with a call like:

```
$environment = \RoundSphere\Properties::getProperty('environment');
```

Nested values, like `someService` in the example above are converted to an array. So you might use
```
$config = \RoundSphere\Properties::getProperty('someService);
$someService = new \Some\Service($config['clientId'], $config['secret']);
```

Or, you can fetch a sub-value from the config file using a dot notation like this:
```
$someServiceClientId = \RoundSphere\Properties::getProperty('someService.clientId);
```

# Committed versus un-committed project.properties
**TWO** `project.properties` files may exist for a project.  One inside the `app/` directory for the committed application code.
This version committed to the repository is suitable for things like relative file/directory locations, or defaults that you
want in a local or test environment for new developers.

The other project.properties file is located outside the application code in a directory relative to the main application directory 
at `../conf/project.properties`. This location is intentionally outside the application code and therefore impossible to commit to the repository.
This file is intended for production config secrets like database credentials, and secrets needed to access external resources.
Configuration values in the second location (outside the repo) override values inside the committed file.

In practical terms, the two relevant config files are in:
 - `/websites/theproject.com/theproject/app/project.properties`  (the "committed" file)
 - `/websites/theproject.com/conf/project.properites` (the "uncommitted" file with secrets)
