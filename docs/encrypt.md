# RoundBase Encryption Functionality

RoundBase provide a couple of handy encryption-related functions using [GNUPG](https://en.wikipedia.org/wiki/GNU_Privacy_Guard#)
and [MySQL's aes_encode](https://dev.mysql.com/doc/refman/8.0/en/encryption-functions.html)

# MySQLs aes_encode / aes_decode

MySQL provide an easy-to-use implimentation that we will often use for encoding parameters in a URL or short-term value
This is often used, for example, in a "Reset password" link to encode information about the userId and timestamp
that the link is being created.  This is convenient in that it doesn't require saving any values to a database.

The only required [configuration parameters](./properties.md) is the `encryption_key` parameter. This is a symetrical key
used for both encrypting and decrypting.  The encrypted binary values are encoded/decoded in base64, suitable for use as strings.

(Note that some base64 values are not suitable for URLs, so characters should be changed if used in a URL)

## Usage
This is a very simple example of how to use the provided `aes_encode` and `aes_decode` methods:

```
<?php
require_once __DIR__.'/../app/common.php';

$someSecretValue = 'foo';

echo "Original String: {$someSecretValue}\n";
$encrypted = \RoundSphere\Encrypt::aes_encode($someSecretValue);

echo "Encrypted string: {$encrypted}\n";

$decrypted = \RoundSphere\Encrypt::aes_decode($encrypted);
echo "Decrypted String: {$decrypted}\n";
```




# GNUPG Configuration
It may be desireable to encrypt some content on the server using a GNUPG public key that can then be transmitted via email
or web page that can be decrypted on a different system.

When using GNUPG, you need to create a directory on disk with the gnupg files and public key.
The following [configuration parameters](./properties.md) must be present in `project.properties`:

*(TODO: This hasn't been used in a while. Additional documentation for setting up gnupg should be written when it is used next)*

| Property            | Purpose
|---------------------|-----------
| `gnupg_home`        | Directory on disk where gnupg can create temporary files. This directory should be protected.
| `gnupg_fingerprint` | Fingerprint for the public key to use to encrypt the values.


