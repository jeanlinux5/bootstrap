# RoundBase Database Connections

RoundBase provides some convenient classes for working with MySQL compatible databases.
 - [Configuration](#configuration)
 - [Example](#example)
 - [Common Parameters](#common-parameters)
 
 Methods:
 - [getOne()](#getOne)
 - [getRow()](#getRow)
 - [getCol()](#getCol)
 - [getAll()](#getAll)
 - [query()](#query)
 - [insertIdFromQuery()](#insertIdFromQuery)
 - [conn()](#conn)

Database Migrations are addressed in [sql-migrations.md](./sql-migrations.md)

Be sure to follow the [SQL Best Practices (including Coding Standards)](./sql-best-practices.md) when using the Database functionality.

# Configuration

Your application configuration file in [`project.properties`](./properties.md) should contain these values:
```
db.port = 3306
db.user = theProject
db.pass = aSuperSecurePasswordShouldGoHere
db.name = theproject
db.host = theproject.randomchars.us-east-1.rds.amazonaws.com
```

# Example
A Common Example may look like this:
```php
use \RoundSphere\Database AS db;

$phoneNumbers = db::getAll("
    SELECT  id, phoneNumber, created
    FROM    phoneNumbers
    WHERE   accountId   = :accountId
        AND country     = :country
        AND created     > DATE_SUB( NOW(), INTERVAL 30 DAY)
", [
    'accountId' => $account->id,
]);
````

This returns an array of phoneNumbers with the columns selected in the query.
 
# Common Parameters
  `$sql` is the first argument in any of the `get()` methods.  It is the actual SQL to execute, including placeholders
  `$args` is the second parameter in any of the `get()` methods.  It includes any arguments to pass to the query, taking the place of the placeholders
  
# getOne()
 The `getOne()` function can be used to retrieve a single value from a single row in a database. For example, to fetch a single setting.

```php
$setting = \RoundSphere\Database::getOne("
   SELECT  value
   FROM     settings
   WHERE    name = 'foo'
 ");
 ```
 
 Or to grab a sum from a column
 ```php
 $total = \RoundSphere\Database::getOne("
     SELECT   SUM(amount) AS amount
     FROM     transactions
     WHERE    YEAR(timestamp) = :year
", [
     'year' => 2020,
]);
```

# getRow()
The `getRow()` function can be used to retrieve a single row (record) from a database. For example, to fetch any record

```php
$userRow = \RoundSphere\Database::getRow("
    SELECT  *
    FROM    users
    WHERE   id = :id
", [
    'id' => $user->id,
]);
```

`$userRow` now contains an array that represents all columns in the table for the one record.

# getCol()
The `getCol()` function can be used to retrieve a single column of values from a database.

Function Definition:
```php
    public function getCol($sql, $args = array(), $column = '', $recurse = true)
```

Example Usage:
```php
$emails = \RoundSphere\Database::getCol("
    SELECT  email
    FROM    users
], [], 'email');
```

`$emails` now contains an array that represents all columns in the table for the one record.

# getAll()
By far, the most frequently used method, the `getAll()` function can be used to retrieve multiple columns from 
multiple rows of a database into a multidimensional array

Function Definition:
```php
    public function getAll($sql, $args = array(), $index_col = '', $recurse = true)
```

Example Usage:
```php
$users = \RoundSphere\Database::getAll("
    SELECT  id, email, lastLogin
    FROM    users
    WHERE   accountId = :accountId
], [
    'accountId' => $accountId,
], 'id');
```

That will create an array.  If the third argument is null or absent, it will be a regular, numerically indexed array.
If the third `$index_col` is non-null, the array indices will be values from the specified column

```php
foreach ($user as $userId => $user) {
    echo "userId = {$userId} (also present as {$user['id']}\n";
}
```

# query()
Used for `UPDATE`s and `INSERT`s, the `query()` function can be used for any other SQL query
The method does not return a value.

Function Definition:
```php
    public function query($sql, $args = array(), $recurse = true)
```

Example Usage:
```php
$users = \RoundSphere\Database::query("
    UPDATE  users
    SET     lastLogin = NOW()
    WHERE   id = :id
], [
    'id' => $user->id,
])
```

# insertIdFromQuery()
Used for `INSERT`s into an auto_incrementing table, the `insertIdFromQuery()` function can be used to insert
and it will return the last insertId from the query.


Function Definition:
```php
    public function insertIdFromQuery($sql, $args = array(), $recurse = true )
```

Example Usage:
```php
$newUserId = \RoundSphere\Database::insertIdFromQuery("
    INSERT INTO users
    SET     id      = 'INSERTID',
            email   = :email
            created = NOW()
], [
    'email' => $email,
])
```

`$newUserId` contains the new user's ID and can be used as expected.

# conn()
The `conn()` method can be used to get the raw [PDO Database object](https://www.php.net/manual/en/book.pdo.php).
You can then use the PDO Object to do anything that the RoundBase library doesn't provide.
This is most often used on large datasets so that you can iterate through the the results in your
application code instead of using RoundBase, which can be memory intensive since the [`getAll()`](#getAll) method
fetches the entire result set into memory.

 
# `db` shortcut
You can `use` the \RoundSphere\Database class to make the syntax much shorter:

```php
use \RoundSphere\Database AS db;

$user = db::getRow("
    SELECT  *
    FROM    users
    WHERE   id = :id
", [
    'id' => $user->id,
]);
```

Also, since many of our projects include multiple databases, we often use a `db` class to simplify access to multiple datbases.

This is usually accomplished with a file in `app/classes/db.class.php` that looks like this:
```php
<?php

class db
{
    static private $app;
    static private $master;
    
    static public function app()
    {
        if (!isset(self::$app)) {
            $config = \RoundSphere\Properties::getProperty('db');
            self::$app = new db($config);
        }
        return self::$app;
    }
    
    static public function master()
    {
        if (!isset(self::$master)) {
            $config = \RoundSphere\Properties::getProperty('master_db');
            self::$master = new db($config);
        }
        return self::$master;
    }
}
````

That allows less short usage of `db::app()` like:
```php
$value = db::app()->getOne("
    SELECT  value
    FROM    settings
    WHERE   name = 'foo'
");
```

And connections to another database like:
```php
$value = db::master()->getOne("
    SELECT  value
    FROM    settings
    WHERE   name = 'foo'
");
```

 
 
# Paginated Results
We used to use the [`SQL_COUNT_FOUND_ROWS`](https://dev.mysql.com/doc/refman/8.0/en/information-functions.html#function_found-rows)
function to determine the number of results that *would* have been fetched without a `LIMIT` clause.
MySQL has deprecated `SQL_COUNT_FOUND_ROWS` and the associated `FOUND_ROWS()` function as of MySQL 8.0.17, so the best practice is now
to make two calls. One with the `LIMIT` clause, and a second using `COUNT(*)` without the `LIMIT` clause.

