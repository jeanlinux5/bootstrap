# SQL Best Practices

 - Always use placeholders! Never put variables directly into SQL statements so that you avoid [SQL Injection](https://bobby-tables.com/)
 - Always use the `utf8mb4` character set and `utf8mb4_unicode_ci` colation. Never, ever use `utf8`. [Background](https://medium.com/@adamhooper/in-mysql-never-use-utf8-use-utf8mb4-11761243e434)
   - If you think a different character set is appropriate, you are probably wrong!
   - `latin` may seem appropriate in some cases due to its simplicity, actually adds un-necessary complexity when everything else is `utf8mb4`.
 - Follow the Formatting guidelines
 
# SQL Coding Standards

For readability and consistency, please:
 - Place SQL Commands are in all uppercase
 - Use Tabs are used liberally to line up parameters
 - Usually use just one "statement" per line.
 
When considering code style, one of the primary goals is to make it easy for future modification.  For example, by
having each column in the SELECT clause on a separate line, it makes it easy to add, remove, or change columns and make
a very clean `git diff` that clearly indicates what was changed.

Continue to use [PSR-12](https://www.php-fig.org/psr/psr-12/) in the arrays and other parts of the statements.

## Example

```
$phoneNumbers = db::getAll("
    SELECT  id, phoneNumber, created
    FROM    phoneNumbers
    WHERE   accountId   = :accountId
        AND country     = :country
        AND created     > DATE_SUB( NOW(), INTERVAL 30 DAY)
", [
    'accountId' => $account->id,
]);
```
