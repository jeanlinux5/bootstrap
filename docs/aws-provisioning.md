# RoundBase AWS Provisioning

Most RoundBase-based projects are provisioned through AWS Auto-Scaling groups.

The auto-scaling group has specific User-Data to load the project. This updates the operating system,
loads the required libraries and then runs a bootstrap script from S3.  The necessary permissions to read
from the S3 bucket need to be manually created using an IAM role, also assigned to the Auto-Scaling Group.

```
#cloud-config
repo_update: true
repo_upgrade: all

packages:
 - awscli
 - git

runcmd:
 - aws s3 cp s3://{secrets-bucket}/{projectName}-production/start.sh /root/start.sh
 - sh /root/start.sh
 ```
 
An example `start.sh` script is provided at [`examples/start.sh`](./examples/start.sh)
